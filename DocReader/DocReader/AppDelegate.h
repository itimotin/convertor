//
//  AppDelegate.h
//  DocReader
//
//  Created by Admin on 19.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic,retain) UIWindow *window;

@property (nonatomic,retain) ViewController *viewController;

@end

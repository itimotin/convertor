//
//  DownloadView.m
//  DocReader
//
//  Created by Admin on 21.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "DownloadView.h"
#import "DownloadHelper.h"

@implementation DownloadView
@synthesize webView,URLaddress,extension,file;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [indicator removeFromSuperview];
    fileDetector = [[FileDetector alloc] init];
    [fileDetector setDelegate:self];
    
    [loadShadow removeFromSuperview];
    [askView removeFromSuperview];
    [progressView removeFromSuperview];
    [confirmDownload removeFromSuperview];
    
    pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scale:)];
    [scrollView addGestureRecognizer:pinchGesture];
    
    
    /////////////custom search display tuning
    /////////////setting addres field to accept long search requests
    [addressField setContentSize:CGSizeMake(1024, 20)];
    
    /////////////
    [shadowImage removeFromSuperview];
    [previousSearchKeywords removeFromSuperview];
    /////////////
    searchHistory = [[NSMutableArray alloc] initWithCapacity:1];
    NSFileManager *filemgr;
    NSArray *dirPaths;
    NSString *docsDir;
    NSString *historyDir;
    
    filemgr = [NSFileManager defaultManager];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    historyDir = [docsDir stringByAppendingPathComponent:@"data"];
        
    NSString *historyPath = [historyDir stringByAppendingPathComponent:@"history.txt"];
    BOOL isDir=NO;
    if ([filemgr fileExistsAtPath:historyPath isDirectory:&isDir]&&!isDir) {
        NSLog(@"searchHistory.txt exist");
        NSString *string = [[NSString alloc] initWithContentsOfFile:historyPath encoding:NSUTF8StringEncoding error:nil];
    
        [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByLines usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) 
        {
          
            if (substringRange.location!=NSNotFound) {
                [searchHistory addObject:substring];
            }  
        } ];
    }else{
        NSString *str = @"";
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        [filemgr createFileAtPath:historyPath contents:data attributes:nil];
        [searchHistory addObject:str];
        NSLog(@"searchHistory.txt created");
    }
    count = [searchHistory count];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [pinchGesture release],pinchGesture = nil;
    [fileDetector release],fileDetector = nil;
    if (downloadHelper) {
        [downloadHelper release],downloadHelper = nil;
    }
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //i'm lazy to do normal resizing
    BOOL should = [[NSUserDefaults standardUserDefaults] boolForKey:@"autorotation"];
    if (should) {
        if (![shadowImage isDescendantOfView:self.view]) {
            [self.view addSubview:shadowImage];
            shadowImage.hidden = YES;
            [self.view addSubview:previousSearchKeywords];
            previousSearchKeywords.hidden = YES;
        }
        if ([shadowImage isDescendantOfView:self.view]&&
            (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight))
        {
            CGRect frame = previousSearchKeywords.frame;
            frame.size.height = 44;
            previousSearchKeywords.frame = frame;
            should = YES;
            return should;
        }else{
            CGRect frame = previousSearchKeywords.frame;
            frame.size.height = 163;
            previousSearchKeywords.frame = frame;
            should = YES;
            return should;
        }
    }
    if (should==NO)should=UIInterfaceOrientationPortrait;
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    if ([shadowImage isDescendantOfView:self.view]&&shadowImage.hidden) {
        [shadowImage removeFromSuperview];
        [previousSearchKeywords removeFromSuperview];
        shadowImage.hidden = NO;
        previousSearchKeywords.hidden = NO;
    }
}

- (IBAction)dismisBtnPressed:(UIButton *)sender
{
    [self dismissModalViewControllerAnimated:YES];
}




#pragma mark - WebView delegate methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    if (urlRequest!=nil)
        [urlRequest release],urlRequest = nil;
    if (urlConnection!=nil)
        [urlConnection release],urlConnection = nil;
    if (_openLink) {
        _openLink = NO;
        return YES;
    }
    if (_wvRequest!=nil) {
        [_wvRequest release],_wvRequest = nil;
    }
    [fileDetector isFile:request.URL];
    _wvRequest = [request retain];
    
    return YES;
}



- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
}

#pragma mark - TableView Delegate Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [searchHistory count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    
    cell.textLabel.text = [searchHistory objectAtIndex:indexPath.row];
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
        NSString *str = @"http://";
    if([[tableView cellForRowAtIndexPath:indexPath].textLabel.text rangeOfString:str].location==NSNotFound){
        URLaddress = [str stringByAppendingString:[tableView cellForRowAtIndexPath:indexPath].textLabel.text];
    }else{
        URLaddress = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
    }
               
            
            
        
        NSLog(@"%@",URLaddress);
        if(URLaddress.length>7)requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:URLaddress]];
        [webView loadRequest:requestObj];
        [shadowImage removeFromSuperview];
        [previousSearchKeywords removeFromSuperview];
        [addressField resignFirstResponder];
}
#pragma mark - TextView Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (shadowImage.hidden) {
        shadowImage.hidden = NO;
        previousSearchKeywords.hidden = NO;
    }
    NSFileManager *filemgr;
    NSArray *dirPaths;
    NSString *docsDir;
    NSString *historyDir;
    
    filemgr = [NSFileManager defaultManager];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    historyDir = [docsDir stringByAppendingPathComponent:@"data"];
    
    
    NSString *historyPath = [historyDir stringByAppendingPathComponent:@"history.txt"];
    BOOL isDir=NO;
    if ([filemgr fileExistsAtPath:historyPath isDirectory:&isDir]&&!isDir) {
        NSLog(@"searchHistory.txt exist");
        NSString *string = [[NSString alloc] initWithContentsOfFile:historyPath encoding:NSUTF8StringEncoding error:nil];
        NSString *newString = @"";
        
        for (NSString *str in searchHistory) {
            str = [str stringByAppendingString:@"\n"];
            newString = [newString stringByAppendingString:str];
            //NSLog(@"%@",str);
            
        }
        NSLog(@"%@",newString);
        if([string isEqualToString:newString]){
            NSLog(@"PRAISE THE LORD");
        }else{
            [searchHistory removeAllObjects];
            [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByLines usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) 
             {
                 
                 if (substringRange.location!=NSNotFound) {
                     [searchHistory addObject:substring];
                 }  
             } ];
        
        }
    }else{
        NSString *str = @"";
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        [filemgr createFileAtPath:historyPath contents:data attributes:nil];
        [searchHistory addObject:str];
        NSLog(@"searchHistory.txt created");
    }
    [previousSearchKeywords reloadData];
    [self.view addSubview:shadowImage];
    [self.view addSubview:previousSearchKeywords];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    BOOL shouldChange = YES;
    if ([text isEqualToString:@"\n"]) {
        NSString *str = @"http://";
        if([text rangeOfString:str].location==NSNotFound){
            URLaddress = [str stringByAppendingString:textView.text];   
        }else{
            str = textView.text;
        }
        NSLog(@"%@",URLaddress);
        if(URLaddress.length>7)requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:URLaddress]];
        [webView loadRequest:requestObj];
        [shadowImage removeFromSuperview];
        [previousSearchKeywords removeFromSuperview];
        [textView resignFirstResponder];
        
        shouldChange = NO;
        
        NSFileManager *filemgr;
        NSArray *dirPaths;
        NSString *docsDir;
        NSString *historyDir;
        
        filemgr = [NSFileManager defaultManager];
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        historyDir = [docsDir stringByAppendingPathComponent:@"data"];
        
        
        NSString *historyPath = [historyDir stringByAppendingPathComponent:@"history.txt"];
        BOOL isDir=NO;
        
        if ([filemgr fileExistsAtPath:historyPath isDirectory:&isDir]&&!isDir) {
            NSLog(@"searchHistory.txt exist");
            NSString *string = [[NSString alloc] initWithContentsOfFile:historyPath encoding:NSUTF8StringEncoding error:nil];
            
            NSString *searchRequest = [textView.text stringByAppendingString:@"\n"];
            
            if ([string rangeOfString:searchRequest].location==NSNotFound) {
                NSLog(@"search request = %@",searchRequest);
                NSString *str = [string stringByAppendingString:searchRequest];
                NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                if(![filemgr removeItemAtPath:historyPath error:nil])NSLog(@"Error removing file");
                if(![filemgr createFileAtPath:historyPath contents:data attributes:nil])NSLog(@"Error creating file");
            }else{
                NSLog(@"request exist");
            }
            
            NSString *test = [[NSString alloc] initWithContentsOfFile:historyPath encoding:NSUTF8StringEncoding error:nil];
            NSLog(@"test is \n%@",test);
            [string release];
            [test release];
            
        }else{
            NSString *str;
            if(textView.text.length>0){str = text;}else{str = @"";}
            NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
            [filemgr createFileAtPath:historyPath contents:data attributes:nil];
            [searchHistory addObject:str];
            NSLog(@"searchHistory.txt created");
        }
        [filemgr release];
    }
    return shouldChange;
}

#pragma mark - Browser actions
- (IBAction)downloadFile:(UIButton*)sender
{
    _openLink = NO;
    if ([askView isDescendantOfView:self.view]) {
        [askView removeFromSuperview];
    }
    [self.view addSubview:confirmDownload];
}

- (void)openLink
{
    _openLink = YES;
    [askView removeFromSuperview];
    [webView loadRequest:_wvRequest];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [alertView release];
}

- (IBAction)backAction:(UIButton*)sender
{
    if ([webView canGoBack]) [webView goBack];
}
- (IBAction)forwardAction:(UIButton*)sender
{
    if ([webView canGoForward]) [webView goForward];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([touches count]==2) {
        [scrollView nextResponder];
    }
    UITouch *touch = [touches anyObject];
    if([touch locationInView:shadowImage].x){
        [addressField resignFirstResponder];
        [shadowImage removeFromSuperview];
        [previousSearchKeywords removeFromSuperview];
    }
}


- (void)scale:(UIPinchGestureRecognizer *)sender
{
    
    //[self.view bringSubviewToFront:[sender view]];
    NSLog(@"scale = %f",sender.scale);
    if(sender.state == UIGestureRecognizerStateBegan){
        [scrollView nextResponder];
        [scrollView setZoomScale:sender.scale animated:YES];
    }
    NSLog(@"%f",scrollView.zoomScale);
    
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return webView ;
}


- (IBAction)saveFile:(UIButton *)sender
{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [dirPaths objectAtIndex:0];
    [filemgr  createFileAtPath:[docPath stringByAppendingPathComponent:confirmTextField.text] contents:nil attributes:nil];
    [filemgr release];
    if (downloadHelper) {
        [downloadHelper release],downloadHelper = nil;
    }
    downloadHelper = [[DownloadHelper alloc] init];
    [downloadHelper setDelegate:self];
    [downloadHelper  download:[_wvRequest.URL absoluteString] toPath:[docPath stringByAppendingPathComponent:confirmTextField.text]];
    [confirmDownload removeFromSuperview];
    [askView removeFromSuperview];
    [self.view addSubview:progressView];
}



- (IBAction)cancel:(UIButton *)sender
{
    [downloadHelper cancelDownload];
    
}

#pragma mark - FileDetectorDelegate

- (void)fileDetected:(BOOL)flag fileName:(NSString *)fileName
{
    if (flag) {
        [webView stopLoading];
        [self.view addSubview:askView];
        [confirmTextField setText:fileName];
    }
}

#pragma mark - DownloadHelperDelegate
- (void) dataDownloadFailed: (NSString *) reason
{
    NSLog(@"%@",reason);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Download Failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    [progressView removeFromSuperview];
}
- (void) dataDownloadAtPercent: (NSNumber *) aPercent
{
    [progress setProgress:[aPercent floatValue]];
}

- (void)DHDidReceiveData:(NSData *)theData
{
    [progressView removeFromSuperview];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
}

- (IBAction)close
{
    [askView removeFromSuperview];
}
@end

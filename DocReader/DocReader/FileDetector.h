//
//  FileDetector.h
//  GetFileTest
//
//  Created by Admin on 30.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol FileDetectorDelegate
@optional
- (void)fileDetected:(BOOL)flag fileName:(NSString*)fileName;
@end


@interface FileDetector : NSObject <NSURLConnectionDelegate>
{
    NSURLConnection *urlConnection;
    NSMutableURLRequest *urlRequest;
}
@property (assign) id <FileDetectorDelegate> delegate;

- (void) isFile:(NSURL*)url;
@end

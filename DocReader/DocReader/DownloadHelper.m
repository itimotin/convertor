//
//  DownloadHelper.m
//  GetFileTest
//
//  Created by Admin on 30.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DownloadHelper.h"

@implementation DownloadHelper

@synthesize _receivedData,_response,_theConnection,_URLString,delegate,file;

- (void)clean
{
    isDownloading = NO;
    _URLString = nil;
    _theConnection = nil;
    _response = nil;
    _receivedData = nil;
    [filePath release],filePath = nil;
}


#pragma mark - Class methods: Download, CancelDownload

- (void)download:(NSString *)_urlString toPath:(NSString *)path
{
    [self createConnection:_urlString];
    filePath = [path retain];
}

- (void)cancelDownload
{
    if(isDownloading)[_theConnection cancel];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    [filemgr removeItemAtPath:filePath error:nil];
}


#pragma mark - Creating connection

- (void)createConnection:(NSString*) URLString
{
    isDownloading = NO;
    
    NSURL *url = [NSURL URLWithString:URLString];
    if (!url) {
        NSString *msg = [NSString stringWithFormat:@"Failed to create URL with %@",URLString];
        [delegate dataDownloadFailed:msg];
        NSLog(@"%@",msg);
        //show user that URL coldn't be created
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    if (!request) {
        NSString *msg = [NSString stringWithFormat:@"Failed to create URLRequest with %@",URLString];
        [delegate dataDownloadFailed:msg];
        NSLog(@"%@",msg);
        //show user that URLRequest coldn't be created
    }
    
    _theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (!_theConnection) {
        NSString *msg = [NSString stringWithFormat:@"Failed to create Connection with %@",URLString];
        [delegate dataDownloadFailed:msg];
        NSLog(@"%@",msg);
        //show user that URLConnection coldn't be created
    }
    
    isDownloading = YES;
    
    ///_receivedData = [NSMutableData data];
    _response = nil;
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _response = [response retain];
    if ([_response expectedContentLength]<0) {
        NSLog(@"Invalid URL");
        [delegate dataDownloadFailed:@"Invalid URL"];
        [_theConnection cancel];
        [self clean];
        return;
    }
    if ([_response suggestedFilename]) {
        //[delegate didReceiveFilename:[_response suggestedFilename]];
    }
    if (file) {
        [file release];
    }
    file = [[NSFileHandle fileHandleForUpdatingAtPath:filePath] retain];
    
    if (file)   {
        [file seekToEndOfFile];
        NSLog(@"fileOk go ");
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    
    NSLog(@"data recieved %u",[data length]);
    if (_response)
    {
        if (file) {
            [file seekToEndOfFile];
            NSLog(@"fileOk go ");
        }
        
        [file writeData:data];
        
        NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
        NSString *fileSize = [fileAttributes objectForKey:NSFileSize];
        long long currentFileSize = [fileSize longLongValue];
        
        float expectedLength = [_response expectedContentLength];
        
        float percent = currentFileSize / expectedLength;
        //show download progress to user
        if (self.delegate) {
            [delegate dataDownloadAtPercent:[NSNumber numberWithFloat:percent]];
        }
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    isDownloading = NO;
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    //show user a message that the connection has failed
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // finished downloading the data, cleaning up
    _response = nil;
    
    
    //if delegate exist send delegate the data 
    // delegate is responsible for releasing data
    if (self.delegate)
    {
        NSData *theData = [_receivedData retain];
        [delegate DHDidReceiveData:theData];
        
    }
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSLog(@"finished");
    [self clean];
}


@end
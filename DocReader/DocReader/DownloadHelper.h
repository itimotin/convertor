//
//  DownloadHelper.h
//  GetFileTest
//
//  Created by Admin on 30.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol DownloadHelperDelegate
@optional
- (void) DHDidReceiveData: (NSData *) theData;
- (void) didReceiveFilename: (NSString *) aName;
- (void) dataDownloadFailed: (NSString *) reason;
- (void) dataDownloadAtPercent: (NSNumber *) aPercent;
@end


@interface DownloadHelper : NSObject <NSURLConnectionDelegate>
{
    id <DownloadHelperDelegate> delegate;
    BOOL isDownloading;
    NSString *filePath;
        
}
@property (assign) id <DownloadHelperDelegate> delegate;
@property (retain) NSString *_URLString;
@property (retain) NSURLConnection *_theConnection;
@property (retain) NSURLResponse *_response;
@property (retain) NSMutableData *_receivedData;
@property (nonatomic, retain) NSFileHandle *file;

- (void)download:(NSString*)_urlString toPath:(NSString*)path;
- (void)cancelDownload;
- (void)createConnection:(NSString*)URLString;
- (void)clean;
@end


//
//  CustomSwitch.m
//  SpyKit
//
//  Created by Admin on 14.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomSwitch.h"

@implementation CustomSwitch
@synthesize backImage=_backImage,thmbImage=_thmbImage,off,delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        touchMoved = NO;
        [self customInit];
    }
    return self;
}

- (void)customInit
{
    
    //self.backgroundColor = [UIColor whiteColor];
    UIImage *bgImg = [UIImage imageNamed:@"off_back.png"];
    backgroundImage = [UIImage imageWithCGImage:bgImg.CGImage scale:2.0f orientation:UIImageOrientationUp];
    UIImage *tbImg = [UIImage imageNamed:@"on-off_top.png"];
    thumbImage  = [UIImage imageWithCGImage:tbImg.CGImage scale:2.0f orientation:UIImageOrientationUp];
    CGSize bgSize = backgroundImage.size;
    CGSize tbSize = thumbImage.size;
    CGRect bgRect = CGRectMake(0, 0, bgSize.width, bgSize.height);
    CGRect tbRect = CGRectMake(2, 3, tbSize.width, tbSize.height);
    UIImageView *bgImgView = [[UIImageView alloc] initWithImage:bgImg];
    UIImageView *tbImgView = [[UIImageView alloc] initWithImage:tbImg];
    bgImgView.frame = bgRect;
    tbImgView.frame = tbRect;
    _backImage = bgImgView;
    _thmbImage = tbImgView;
    
    [self addSubview:_backImage];
    [self addSubview:_thmbImage];
    off = YES;
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    if (!CGRectContainsPoint(_thmbImage.frame, [touch locationInView:self])) {
        if (off) {
            CGRect frame = CGRectMake(_backImage.bounds.size.width-_thmbImage.bounds.size.width, 
                                      3,
                                      _thmbImage.bounds.size.width, _thmbImage.bounds.size.height);
            [UIView beginAnimations:nil context:nil];
            _thmbImage.frame = frame;
            [UIView commitAnimations];
            off = NO;
            UIImage *bgImg = [UIImage imageNamed:@"on_back.png"];
            _backImage.image = [UIImage imageWithCGImage:bgImg.CGImage scale:2.0f orientation:UIImageOrientationUp];
            [self setNeedsDisplay];
            [delegate switchValueChanged:self];
        }else{
            CGRect frame = CGRectMake(2,  
                                      3,
                                      _thmbImage.bounds.size.width, _thmbImage.bounds.size.height);
            [UIView beginAnimations:nil context:nil];
            _thmbImage.frame = frame;
            [UIView commitAnimations];
            off = YES;
            UIImage *bgImg = [UIImage imageNamed:@"off_back.png"];
            _backImage.image = [UIImage imageWithCGImage:bgImg.CGImage scale:2.0f orientation:UIImageOrientationUp];
            [self setNeedsDisplay];
            [delegate switchValueChanged:self];
        }
    }
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint originalTouchLocation = [touch locationInView:self];
    touchMoved = YES;
    NSLog(@"off %d",off);
    if (CGRectContainsPoint(_thmbImage.frame, originalTouchLocation)) {
        CGPoint newCenter = CGPointMake(originalTouchLocation.x, _thmbImage.center.y);
        [_thmbImage setCenter:newCenter];
        //prevent moving to far to the right or left
        if (_thmbImage.center.x-_thmbImage.bounds.size.width/2<2) {
            CGRect rect = {2,3,_thmbImage.bounds.size};
            _thmbImage.frame = rect;
            
        }else if(2+_thmbImage.center.x+_thmbImage.bounds.size.width/2>self.frame.size.width){
            CGRect rect = {self.frame.size.width-_thmbImage.bounds.size.width-2,3,_thmbImage.bounds.size};
            _thmbImage.frame = rect;
        }

        if (off) {
            
            if(_thmbImage.center.x>self.frame.size.width/2){
                
                CGRect frame = CGRectMake(self.frame.size.width-_thmbImage.bounds.size.width-2, 
                                          3,
                                          _thmbImage.bounds.size.width, _thmbImage.bounds.size.height);
                [UIView beginAnimations:nil context:nil];
                _thmbImage.frame = frame;
                [UIView commitAnimations];
                off = NO;
                UIImage *bgImg = [UIImage imageNamed:@"on_back.png"];
                _backImage.image = [UIImage imageWithCGImage:bgImg.CGImage scale:2.0f orientation:UIImageOrientationUp];
                [delegate switchValueChanged:self];
                
            }
        }else{
            if (_thmbImage.center.x<self.frame.size.width/2) {
                
                CGRect frame = CGRectMake(2,  
                                          3,
                                          _thmbImage.bounds.size.width, _thmbImage.bounds.size.height);
                [UIView beginAnimations:nil context:nil];
                _thmbImage.frame = frame;
                [UIView commitAnimations];
                off = YES;
                UIImage *bgImg = [UIImage imageNamed:@"off_back.png"];
                _backImage.image = [UIImage imageWithCGImage:bgImg.CGImage scale:2.0f orientation:UIImageOrientationUp];
                [delegate switchValueChanged:self];
                
            }
        }
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touches canceled");
    

}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (touchMoved) {
        if(_thmbImage.center.x>self.frame.size.width/2){
            
            CGRect frame = CGRectMake(self.frame.size.width-_thmbImage.bounds.size.width-2, 
                                      3,
                                      _thmbImage.bounds.size.width, _thmbImage.bounds.size.height);
            [UIView beginAnimations:nil context:nil];
            _thmbImage.frame = frame;
            [UIView commitAnimations];
            off = NO;
            touchMoved = NO;
            UIImage *bgImg = [UIImage imageNamed:@"on_back.png"];
            _backImage.image = [UIImage imageWithCGImage:bgImg.CGImage scale:2.0f orientation:UIImageOrientationUp];
            [delegate switchValueChanged:self];///6ob naverneaka uje :D
            
        }
        if (_thmbImage.center.x<self.frame.size.width/2) {
            
            CGRect frame = CGRectMake(2,  
                                      3,
                                      _thmbImage.bounds.size.width, _thmbImage.bounds.size.height);
            [UIView beginAnimations:nil context:nil];
            _thmbImage.frame = frame;
            [UIView commitAnimations];
            off = YES;
            touchMoved  = NO;
            UIImage *bgImg = [UIImage imageNamed:@"off_back.png"];
            _backImage.image = [UIImage imageWithCGImage:bgImg.CGImage scale:2.0f orientation:UIImageOrientationUp];
            [delegate switchValueChanged:self];///6ob naverneaka uje :D
            
        }
    }
}
@end

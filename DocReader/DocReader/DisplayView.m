//
//  DisplayView.m
//  simpleReader
//
//  Created by Symon on 11/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DisplayView.h"


@implementation DisplayView
@synthesize videoLoading;



-(void) prepareView:(NSString *)filePath {
	
    
    if (videoLoading) {
    }
	NSURL *url = [NSURL fileURLWithPath:filePath];
	[webView loadRequest:[NSURLRequest requestWithURL:url]];
    [label setText:[filePath lastPathComponent]];
	webView.scalesPageToFit = YES;
	webView.delegate = self;
	activity.hidden = 1;
    
}

-(void)webViewDidStartLoad:(UIWebView *)wv {
	//NSLog(@"WebView Did Start Load");
	activity.hidden = 0;
    webView.hidden = YES;
    
    
}

- (void)webViewDidFinishLoad:(UIWebView *)wv {
    
    backButton.enabled = YES;
    webView.hidden = NO;
	//NSLog(@"WebView Did Finish Load");
	activity.hidden = 1;
	
	[webView stringByEvaluatingJavaScriptFromString:
	 [NSString stringWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'",10*
	  [[NSUserDefaults standardUserDefaults] integerForKey:@"fontSize"]
	  ]];
	int pageYOffset = [[NSUserDefaults standardUserDefaults] integerForKey:@"YOFFSET"];
	//NSLog(@"pageYOffset = %i",pageYOffset);	
	[webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat: @"window.scrollTo(0, %d);",pageYOffset]];
	
	if ([[NSUserDefaults standardUserDefaults] boolForKey:@"autodetectLinks"]) {
		webView.dataDetectorTypes = UIDataDetectorTypeAll;
	}
	else {
		webView.dataDetectorTypes = UIDataDetectorTypeNone;
	}
}


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    BOOL should = [[NSUserDefaults standardUserDefaults] boolForKey:@"autorotation"];
    if (should == NO)
	return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"autorotation"];
}

-(void)viewWillAppear:(BOOL)animated
{
    backButton.enabled = NO;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    
	[webView stopLoading];
	[a stopAnimating];
	
	activity.hidden = 1;
	int pageYOffset = [[webView stringByEvaluatingJavaScriptFromString:@"window.pageYOffset"] intValue];
	//NSLog(@"pageYOffset = %i",pageYOffset);
	[[NSUserDefaults standardUserDefaults] setInteger:pageYOffset forKey:@"YOFFSET"];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (IBAction)back:(UIButton *)sender
{
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void)dealloc {
    [super dealloc];
}



@end

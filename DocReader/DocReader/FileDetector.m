//
//  FileDetector.m
//  GetFileTest
//
//  Created by Admin on 30.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FileDetector.h"
static const NSString *acceptedFileTypes = @"application/pdf;application/msword;application/vnd.ms-excel;application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;application/vnd.ms-powerpoint;application/rtf;text/plain;image/png;image/gif;image/jpeg;image/bmp;application/octet-stream";

@implementation FileDetector
@synthesize delegate;
- (void)clean
{
    [urlConnection cancel];
    [urlRequest release],urlRequest = nil;
    urlConnection = nil;
}

- (void)isFile:(NSURL *)url
{
    NSLog(@"%@",[url absoluteString]);
    urlRequest = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60.0f];
    [urlRequest setHTTPMethod:@"HEAD"];
    urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"recieved response");
    if ([response respondsToSelector:@selector(allHeaderFields)])
    {
        NSDictionary *dict = [[NSDictionary alloc] initWithDictionary:[(NSHTTPURLResponse *)response allHeaderFields]];
        NSLog(@"=======HEAD RESPONSE===========================");
        
        NSString *contentType;
        if ([[dict valueForKey:@"Content-Type"] rangeOfString:@";"].location==NSNotFound) {
            contentType = [dict valueForKey:@"Content-Type"];
        }else{
            contentType = [[dict valueForKey:@"Content-Type"] substringWithRange:NSMakeRange(0, [[dict valueForKey:@"Content-Type"] rangeOfString:@";"].location)];
        }
        NSLog(@"%@",contentType);
        if (contentType) {
            if ([acceptedFileTypes rangeOfString:contentType].location!=NSNotFound)
            {
                NSLog(@"file detected");
                NSLog(@"%@",[response suggestedFilename]);
                [delegate fileDetected:YES fileName:[response suggestedFilename]];
                [self clean];
            }else{
                [delegate fileDetected:NO fileName:[response suggestedFilename]];
                [self clean];
            }
        }else{
            [delegate fileDetected:NO fileName:[response suggestedFilename]];
            [self clean];
        }

    }
    
}
@end

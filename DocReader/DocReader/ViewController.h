//
//  ViewController.h
//  DocReader
//
//  Created by Admin on 19.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class DownloadView,CustomSwitch,HTTPServer,DisplayView;

@interface ViewController : UIViewController <UIWebViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    //http server
    HTTPServer *httpServer;
	NSDictionary *addresses;
	NSString *ip;
	UISwitch *fileSharing;
    
    //table view contents
    NSString *curentDirectory;
    UIImage *image;
    
    //file type buttons - documents view
    IBOutlet UIButton *pdfBtn;
    IBOutlet UIButton *xlsBtn;
    IBOutlet UIButton *docBtn;
    IBOutlet UIButton *pptBtn;
    IBOutlet UIButton *txtBtn;
    IBOutlet UIButton *imgBtn;
    IBOutlet UIButton *vidBtn;
    //file type labels - documents view
    IBOutlet UILabel *pdfLbl;
    IBOutlet UILabel *xlsLbl;
    IBOutlet UILabel *docLbl;
    IBOutlet UILabel *pptLbl;
    IBOutlet UILabel *txtLbl;
    IBOutlet UILabel *imgLbl;
    IBOutlet UILabel *vidLbl;
    //bar elements
    IBOutlet UIButton *backBtn;
    IBOutlet UIButton *editDoneBtn;
    IBOutlet UIButton *documentsBtn;
    IBOutlet UIButton *downloadBtn;
    IBOutlet UIButton *settingsBtn;
    IBOutlet UIImageView *chooseFile;
    //files view elements
    
    IBOutlet UITableView *filesList;
    IBOutlet UILabel *filesFoundLbl;
    
    //pointer to view that will be hidden
    UIView *viewToHide;
    //Top Bar View
    IBOutlet UIView *topBarView;
    //main background
    IBOutlet UIImageView *mainBackground;
    //download view elements
    IBOutlet UIButton *goDownloadBnt;
    DownloadView *brouser;
    IBOutlet UILabel *addressLabel;
    IBOutlet UIImageView *greenBack;
    
    //settings view
    //switches
    CustomSwitch *autorotation;
    CustomSwitch *sleepMode;
    CustomSwitch *linksDetection;
    CustomSwitch *httpSwitch;//in download sheet
    
    IBOutlet UILabel *textSizeLabel;
    IBOutlet UIButton *plus;
    IBOutlet UIButton *minus;
    
    int pdfCount;
    int xlsCount;
    int docCount;
    int pptCount;
    int txtCount;
    int imgCount;
    int vidCount;
    
    int countToShow;
    
    int itemsToShow;//0-pdf;1-xls;2-doc;3-ppt;4-txt;5-img;6-vid;
    
    NSMutableArray *filesToShowInTableView;
    BOOL loadingVideo;
    IBOutlet UIWebView *webView;
    UIViewController *displayView;
    BOOL should;
    DisplayView *viewContr;
    UIWebView *videoWebView;
}
///tabbar views
@property (nonatomic, retain) IBOutlet UIView *documentsView;
@property (nonatomic, retain) IBOutlet UIView *downloadView;
@property (nonatomic, retain) IBOutlet UIView *settingsView;
//fileView
@property (nonatomic, retain) IBOutlet UIView *filesView;

- (IBAction)tabbarButtonPressed:(UIButton*)sender;
- (IBAction)fileTypeButtonPressed:(UIButton*)sender;
- (IBAction)goDownloadBtnPressed:(UIButton*)sender;
- (IBAction)textSizeChanged:(UIButton*)sender;
- (void)updateLabels;
- (void)openDoc:(NSString*)path;
@end

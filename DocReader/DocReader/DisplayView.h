//
//  DisplayView.h
//  DocReader
//
//  Created by Admin on 28.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface DisplayView : UIViewController <UIWebViewDelegate>{
	IBOutlet UIWebView *webView;
	IBOutlet UIView *activity;
	IBOutlet UIActivityIndicatorView *a;
    IBOutlet UILabel *label;
    
    IBOutlet UIButton *backButton;
}
@property (nonatomic) BOOL videoLoading;
- (void) prepareView:(NSString *)filePath;
- (IBAction)back:(UIButton*)sender;

@end



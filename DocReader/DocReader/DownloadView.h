//
//  DownloadView.h
//  DocReader
//
//  Created by Admin on 21.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FileDetector.h"
#import "DownloadHelper.h"

@interface DownloadView : UIViewController <UIWebViewDelegate,NSURLConnectionDelegate,UITableViewDelegate,
                                            UIGestureRecognizerDelegate,UITextViewDelegate,UIScrollViewDelegate
                                            ,UIAlertViewDelegate,FileDetectorDelegate,DownloadHelperDelegate>
{
    
    NSURLRequest *requestObj;
    
    IBOutlet UIActivityIndicatorView *indicator;
    IBOutlet UIView *loadShadow;
    IBOutlet UIView *askView;
    int count;
    //custom search
    //custom search display
    IBOutlet UIView *shadowImage;
    IBOutlet UITextView *addressField;
    IBOutlet UITableView *previousSearchKeywords;
    //storing browsing info
    NSArray *browserHistory;                            //storing curent session visited pages URLs
    NSMutableArray *searchHistory;                      //storing search history
    UIPanGestureRecognizer *panGesture;
    UIPinchGestureRecognizer *pinchGesture;
    IBOutlet UIScrollView *scrollView;
                                                
    CGFloat lastScale;
    CGFloat firstX;
	CGFloat firstY;
    //progress view
    IBOutlet UIView *progressView;              
    IBOutlet UIProgressView *progress;
    IBOutlet UILabel *progressLabel;
    //confirm download view
    IBOutlet UIView *confirmDownload;      
    IBOutlet UITextField *confirmTextField;
    
    long long download_size;
    NSURLRequest *currentUrlRequest;
	NSURLConnection *theConnection;
    
    NSString *filePath;
    
    IBOutlet UIButton *downloadButton;
    
    FileDetector *fileDetector;
    DownloadHelper *downloadHelper;
    
    NSURLConnection *urlConnection;
    NSMutableURLRequest *urlRequest;
    NSURLRequest *_wvRequest;
    
    BOOL _openLink;
}

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (retain) NSString *URLaddress;
@property (nonatomic, retain) NSString *extension;
@property (nonatomic, retain) NSFileHandle *file;

-(IBAction)dismisBtnPressed:(UIButton*)sender;

//browser actions
-(IBAction)downloadFile:(UIButton*)sender;
-(IBAction)backAction:(UIButton*)sender;
-(IBAction)forwardAction:(UIButton*)sender;
-(IBAction)cancel:(UIButton*)sender;

-(void)scale:(UIPinchGestureRecognizer*)sender;

-(IBAction)saveFile:(UIButton*)sender;

- (IBAction)openLink;
- (IBAction)close;
@end

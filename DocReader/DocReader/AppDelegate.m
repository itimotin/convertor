//
//  AppDelegate.m
//  DocReader
//
//  Created by Admin on 19.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"фпв");
    //[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication] setIdleTimerDisabled:[[NSUserDefaults standardUserDefaults] boolForKey:@"sleepMode"]];
    if (![[NSUserDefaults standardUserDefaults] integerForKey:@"fontSize"]) {
		[[NSUserDefaults standardUserDefaults] setInteger:10 forKey:@"fontSize"];
	}
    
    NSFileManager *filemgr;
    NSArray *dirPaths;
    NSString *docsDir;
    NSString *newDir;
    
    NSArray *filelist;
    int count;
//    NSError *error = nil;
    BOOL isDir=NO;
    
    
    filemgr =[NSFileManager defaultManager];
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, 
                                                   NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    newDir = [docsDir stringByAppendingPathComponent:@"data"];
    NSString *searchHistory = [newDir stringByAppendingPathComponent:@"history.txt"];
    
    if ([filemgr fileExistsAtPath:newDir isDirectory:&isDir]&&!isDir) {
        NSLog(@"data directory exist");
    }else{
        [filemgr createDirectoryAtPath:newDir withIntermediateDirectories:YES attributes:nil error:nil];
        NSLog(@"data directory created");
    }
    
    
    if ([filemgr fileExistsAtPath:searchHistory isDirectory:&isDir]&&!isDir) {
        NSLog(@"searchHistory.txt exist");
    }else{
        NSString *str = @"";
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        [filemgr createFileAtPath:searchHistory contents:data attributes:nil];
        NSLog(@"searchHistory.txt created");
    }

    
    filelist = [filemgr contentsOfDirectoryAtPath:docsDir error:NULL];
    count = [filelist count];
    NSLog(@"count ==%d",count);
    for (int i = 0; i < count; i++)
        NSLog(@"%@", [filelist objectAtIndex: i]);
    
    NSString *historyReview = [[NSString alloc] initWithContentsOfFile:searchHistory encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"history review :%@",historyReview);
    [historyReview release];
    [filemgr release];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if (url != nil && [url isFileURL]) {
        NSArray *dirPaths;
        NSString *docsDir;
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, 
                                                       NSUserDomainMask, YES);
        
        docsDir = [dirPaths objectAtIndex:0];
        
        NSString *filePath = [docsDir stringByAppendingPathComponent:[url lastPathComponent]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        [data writeToFile:filePath atomically:YES];
        [_viewController updateLabels];
        [_viewController openDoc:filePath];
    }     
    return YES;
}
@end

//
//  CustomSwitch.h
//  SpyKit
//
//  Created by Admin on 14.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomSwitch;
@protocol CustomSwitchDelegate
-(void)switchValueChanged:(CustomSwitch*)sender;
@end


@interface CustomSwitch : UIView
{
    
    UIImage *backgroundImage;
    UIImage *thumbImage;
    BOOL touchMoved;
    
}
@property (assign) id<CustomSwitchDelegate>delegate;
@property (nonatomic, strong) UIImageView *backImage;
@property (nonatomic, strong) UIImageView *thmbImage;
@property (nonatomic,assign) BOOL off;
- (void)customInit;

@end


//
//  ViewController.m
//  DocReader
//
//  Created by Admin on 19.12.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "DisplayView.h"
#import "DownloadView.h"
#import "CustomSwitch.h"
#import "HTTPServer.h"
#import "MyHTTPConnection.h"
#import "localhostAddresses.h"

@implementation ViewController

@synthesize documentsView;
@synthesize downloadView;
@synthesize settingsView;
@synthesize filesView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        viewToHide = nil;
        pdfCount = 0;
        xlsCount = 0;
        docCount = 0;
        pptCount = 0;
        txtCount = 0;
        imgCount = 0;
        vidCount = 0;
        countToShow = 0;
        
        filesToShowInTableView = [[NSMutableArray alloc] initWithCapacity:1];
        
        loadingVideo = NO;
    }
    return self;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)updateLabels {
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *dirPaths;
    NSString *docsDir;
    
    filemgr = [NSFileManager defaultManager];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];

    
    pdfCount = 0;
    xlsCount = 0;
    docCount = 0;
    pptCount = 0;
    txtCount = 0;
    imgCount = 0;
    vidCount = 0;
    
    NSArray *fileList = [filemgr contentsOfDirectoryAtPath:docsDir error:nil];
    for (NSString* str in fileList) {
//NSLog(@"str=%@",str);
        if ([[str lowercaseString] hasSuffix:@".pdf"]) {
            pdfCount++;
        }
        if ([[str lowercaseString] hasSuffix:@".xls"]||[[str lowercaseString] hasSuffix:@".xlsx"]) {
            xlsCount++;
        }
        if ([[str lowercaseString] hasSuffix:@".doc"]||[[str lowercaseString] hasSuffix:@".docx"]) {
            docCount++;
        }
        if ([[str lowercaseString] hasSuffix:@".ppt"]||[[str lowercaseString] hasSuffix:@".pptx"]) {
            pptCount++;
        }
        if ([[str lowercaseString] hasSuffix:@".txt"]||[[str lowercaseString] hasSuffix:@".rtf"]) {
            txtCount++;
        }
        if ([[str lowercaseString] hasSuffix:@".img"]||[[str lowercaseString] hasSuffix:@".png"]||
            [[str lowercaseString] hasSuffix:@".gif"]||[[str lowercaseString] hasSuffix:@".bmp"]||
            [[str lowercaseString] hasSuffix:@".jpg"]||[[str lowercaseString] hasSuffix:@".jpeg"]) {
            imgCount++;
        }
        if ([[str lowercaseString] hasSuffix:@".3gp"]||[[str lowercaseString] hasSuffix:@".mov"]||
            [[str lowercaseString] hasSuffix:@".m4v"]||[[str lowercaseString] hasSuffix:@".mp4"]) {
            vidCount++;
        }
    }
    [pdfLbl setText:[NSString stringWithFormat:@"%d Files",pdfCount]];
    
    [xlsLbl setText:[NSString stringWithFormat:@"%d Files",xlsCount]];

    [docLbl setText:[NSString stringWithFormat:@"%d Files",docCount]];

    [pptLbl setText:[NSString stringWithFormat:@"%d Files",pptCount]];

    [txtLbl setText:[NSString stringWithFormat:@"%d Files",txtCount]];

    [imgLbl setText:[NSString stringWithFormat:@"%d Files",imgCount]];

    [vidLbl setText:[NSString stringWithFormat:@"%d Files",vidCount]];

    
    [filemgr release];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    viewContr = [[DisplayView alloc] initWithNibName:@"DisplayView" bundle:nil];
    [self updateLabels];
    filesList.backgroundColor = [UIColor clearColor];
    addressLabel.hidden = YES;
    greenBack.hidden = YES;
	// Do any additional setup after loading the view, typically from a nib.
    [editDoneBtn removeFromSuperview];
    [backBtn removeFromSuperview];
    [filesView removeFromSuperview];
    [downloadView removeFromSuperview];
    [settingsView removeFromSuperview];
    mainBackground.image = [UIImage imageNamed:@"back_1.png"];
    viewToHide = documentsView;
    brouser = [[DownloadView alloc] initWithNibName:@"DownloadView" bundle:nil];
    
    /////settings view tunning
    autorotation = [[CustomSwitch alloc] initWithFrame:CGRectMake(168, 6, 117, 35)];
    sleepMode = [[CustomSwitch alloc] initWithFrame:CGRectMake(168, 76, 117, 35)];
    linksDetection = [[CustomSwitch alloc] initWithFrame:CGRectMake(168, 156, 117, 35)];
    autorotation.delegate = (id)self;
    sleepMode.delegate = (id)self;
    sleepMode.tag = 1;
    linksDetection.delegate = (id)self;
    linksDetection.tag = 2;
    [settingsView addSubview:autorotation];
    [settingsView addSubview:sleepMode];
    [settingsView addSubview:linksDetection];
    textSizeLabel.text = [NSString stringWithFormat:@"%d",[[NSUserDefaults standardUserDefaults] integerForKey:@"fontSize"]];
    
    
    //download enable http switch
    httpSwitch = [[CustomSwitch alloc] initWithFrame:CGRectMake(168, 80, 117, 35)];
    httpSwitch.delegate = (id)self;
    httpSwitch.tag = 3;
    [downloadView addSubview:httpSwitch];
    
}

- (void)displayInfoUpdate:(NSNotification *) notification {
	//NSLog(@"displayInfoUpdate:");
	
	if(notification)
	{
		[addresses release];
		addresses = [[notification object] copy];
		NSLog(@"addressess: %@", addresses);
	}
	
	if(addresses == nil)
	{
		return;
	}
	
	UInt16 port = [httpServer port];
	
	NSString *localIP = nil;
	
	localIP = [addresses objectForKey:@"en0"];
	
	if (!localIP) {
		localIP = [addresses objectForKey:@"en1"];
	}
	
	if (!localIP)
		ip = [@"Wifi: No Connection!\n" retain];
	else
		ip = [[NSString stringWithFormat:@"http://%@:%d", localIP, port] retain];
    
    addressLabel.text = [NSString stringWithFormat:@"Open web browser on PC browse to:\n%@",ip];
	
}

- (void)switchValueChanged:(CustomSwitch*)sender
{
    switch (sender.tag) {
        case 0:
            NSLog(@"autorotation");
            [[NSUserDefaults standardUserDefaults] setBool:!sender.off forKey:@"autorotation"];
            break;
        case 1:
            NSLog(@"sleepMode");
            [[NSUserDefaults standardUserDefaults] setBool:!sender.off forKey:@"sleepMode"];
            [[UIApplication sharedApplication] setIdleTimerDisabled:!sender.off];
            break;
        case 2:
            NSLog(@"linkz");
            [[NSUserDefaults standardUserDefaults] setBool:!linksDetection.off forKey:@"autodetectLinks"];
            break;
        case 3:
            NSLog(@"enable http");
            NSString *root = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            if (!sender.off) {
                addressLabel.hidden = NO;
                greenBack.hidden = NO;
                httpServer = [HTTPServer new];
                [httpServer setType:@"_http._tcp."];
                [httpServer setConnectionClass:[MyHTTPConnection class]];
                [httpServer setDocumentRoot:[NSURL fileURLWithPath:root]];
                [httpServer setPort:8080];
                
                NSError *error;
                if(![httpServer start:&error]) {
                    NSLog(@"Error starting HTTP Server: %@", error);
                }
                
                [self displayInfoUpdate:nil];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(displayInfoUpdate:) name:@"LocalhostAdressesResolved" object:nil];
                [localhostAddresses performSelectorInBackground:@selector(list) withObject:nil];
                
                
            }
            else {
                [httpServer stop];
                addressLabel.hidden = YES;
                greenBack.hidden = YES;
            }
        default:
            break;
    }
}


- (IBAction)textSizeChanged:(UIButton *)sender
{
    if (sender.tag==0) {
        int size = [[NSUserDefaults standardUserDefaults] integerForKey:@"fontSize"];
        [minus setImage:[UIImage imageNamed:@"-_.png"] forState:UIControlStateNormal];
        [plus setImage:[UIImage imageNamed:@"+.png"] forState:UIControlStateNormal];
        size--;
        if (size<10) size=10;
        [[NSUserDefaults standardUserDefaults] setInteger:size forKey:@"fontSize"];
        textSizeLabel.text = [NSString stringWithFormat:@"%d",size];
    }else
    {
        int size = [[NSUserDefaults standardUserDefaults] integerForKey:@"fontSize"];
        [minus setImage:[UIImage imageNamed:@"-.png"] forState:UIControlStateNormal];
        [plus setImage:[UIImage imageNamed:@"+_.png"] forState:UIControlStateNormal];
        size++;
        if (size>20) size=20;
        [[NSUserDefaults standardUserDefaults] setInteger:size forKey:@"fontSize"];
        textSizeLabel.text = [NSString stringWithFormat:@"%d",size];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [viewContr release],viewContr = nil;
    [downloadView release],downloadView = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (loadingVideo) {
        mainBackground.frame = self.view.frame;
        [displayView dismissModalViewControllerAnimated:NO];
        
        loadingVideo = NO;
    }
    if ([filesList isDescendantOfView:self.view]) {
        [filesList setUserInteractionEnabled:NO];
        [self.view addSubview:editDoneBtn];
    }
    [self updateLabels];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([filesList isDescendantOfView:self.view]) {
        [filesList setUserInteractionEnabled:YES];
        
    }
    should = UIInterfaceOrientationPortrait;
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
        
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    //return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    //return (interfaceOrientation==UIInterfaceOrientationPortrait);
    NSLog(@"loading video is %d",loadingVideo);
    if (loadingVideo&&[[NSUserDefaults standardUserDefaults] boolForKey:@"autorotation"]) {
        if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
            CGRect rect = CGRectMake(0, 56, 480, 244);
            videoWebView.frame = rect;
            videoWebView.center = CGPointMake(240, 176);
        }else{
            CGRect rect = CGRectMake(0, 56, 320, 403);
            videoWebView.frame = rect;
            videoWebView.center = CGPointMake(160, 256);
        }
        return [[NSUserDefaults standardUserDefaults] boolForKey:@"autorotation"];
    }
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    
}
#pragma mark - Handling button actions
- (IBAction)fileTypeButtonPressed:(UIButton *)sender
{
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *dirPaths;
    NSString *docsDir;
    
    filemgr = [NSFileManager defaultManager];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSArray *fileList = [filemgr contentsOfDirectoryAtPath:docsDir error:nil];
    [filesToShowInTableView removeAllObjects];
    
    switch (sender.tag) {
        case 0:
            for (NSString *str in fileList) {
                if ([[str lowercaseString] hasSuffix:@".pdf"]) {
                    [filesToShowInTableView addObject:str];
                }
            }
            
            
            image = nil;
            image = [[UIImage imageNamed:@"pdf_icon.png"] retain];
            [self.view addSubview:editDoneBtn];
            [filesFoundLbl setText:[NSString stringWithFormat:@"%d Files Found",pdfCount]];
            countToShow = pdfCount;
            itemsToShow = 0;
            chooseFile.hidden = YES;
            [filesList reloadData];
            [self.view addSubview:filesView];
            [topBarView addSubview:backBtn];
            viewToHide = filesView;
            break;
        case 1:
            for (NSString *str in fileList) {
                if ([[str lowercaseString] hasSuffix:@".xls"]||[[str lowercaseString] hasSuffix:@".xlsx"]) {
                    [filesToShowInTableView addObject:str];
                }
            }
            
            [self.view addSubview:editDoneBtn];
            image = nil;
            image = [[UIImage imageNamed:@"xlf_icon.png"] retain];
            [filesFoundLbl setText:[NSString stringWithFormat:@"%d Files Found",xlsCount]];
            countToShow = xlsCount;
            itemsToShow = 1;
            chooseFile.hidden = YES;
            [filesList reloadData];
            [self.view addSubview:filesView];
            [topBarView addSubview:backBtn];
            viewToHide = filesView;
            break;
        case 2:
            for (NSString *str in fileList) {
                if ([[str lowercaseString] hasSuffix:@".doc"]||[[str lowercaseString] hasSuffix:@".docx"]) {
                    [filesToShowInTableView addObject:str];
                }
            }
            
            [self.view addSubview:editDoneBtn];
            image = nil;
            image = [[UIImage imageNamed:@"doc_icon.png"] retain];
            [filesFoundLbl setText:[NSString stringWithFormat:@"%d Files Found",docCount]];
            countToShow = docCount;
            itemsToShow = 2;
            chooseFile.hidden = YES;
            [filesList reloadData];
            [self.view addSubview:filesView];
            [topBarView addSubview:backBtn];
            viewToHide = filesView;
            break;
        case 3:
            for (NSString *str in fileList) {
                if ([[str lowercaseString] hasSuffix:@".ppt"]||[[str lowercaseString] hasSuffix:@".pptx"]) {
                    [filesToShowInTableView addObject:str];
                }
            }
            
            [self.view addSubview:editDoneBtn];
            image = nil;
            image = [[UIImage imageNamed:@"prt_icon.png"] retain];
            [filesFoundLbl setText:[NSString stringWithFormat:@"%d Files Found",pptCount]];
            countToShow = pptCount;
            itemsToShow = 3;
            chooseFile.hidden = YES;
            [filesList reloadData];
            [self.view addSubview:filesView];
            [topBarView addSubview:backBtn];
            viewToHide = filesView;
            break;
        case 4:
            for (NSString *str in fileList) {
                if ([[str lowercaseString] hasSuffix:@".txt"]||[[str lowercaseString] hasSuffix:@".rtf"]) {
                    [filesToShowInTableView addObject:str];
                }
            }
            
            [self.view addSubview:editDoneBtn];
            image = nil;
            image = [[UIImage imageNamed:@"txt_icon.png"] retain];
            [filesFoundLbl setText:[NSString stringWithFormat:@"%d Files Found",txtCount]];
            countToShow = txtCount;
            itemsToShow = 4;
            chooseFile.hidden = YES;
            [filesList reloadData];
            [self.view addSubview:filesView];
            [topBarView addSubview:backBtn];
            viewToHide = filesView;
            break;
        case 5:
            for (NSString *str in fileList) {
                if ([[str lowercaseString] hasSuffix:@".png"]||[[str lowercaseString] hasSuffix:@".bmp"]||
                    [[str lowercaseString] hasSuffix:@".jpeg"]||[[str lowercaseString] hasSuffix:@".jpg"]||[[str lowercaseString] hasSuffix:@".gif"]) {
                    [filesToShowInTableView addObject:str];
                }
            }
            
            [self.view addSubview:editDoneBtn];
            image = nil;
            image = [[UIImage imageNamed:@"img_icon.png"] retain];
            [filesFoundLbl setText:[NSString stringWithFormat:@"%d Files Found",imgCount]];
            countToShow = imgCount;
            itemsToShow = 5;
            chooseFile.hidden = YES; 
            [filesList reloadData];
            [self.view addSubview:filesView];
            [topBarView addSubview:backBtn];
            viewToHide = filesView;
            break;
        case 6:
            for (NSString *str in fileList) {
                if ([[str lowercaseString] hasSuffix:@".3gp"]||[[str lowercaseString] hasSuffix:@".mov"]||
                    [[str lowercaseString] hasSuffix:@".mp4"]||[[str lowercaseString] hasSuffix:@".m4v"]) {
                    [filesToShowInTableView addObject:str];
                }
            }
            
            [self.view addSubview:editDoneBtn];
            image = nil;
            image = [[UIImage imageNamed:@"vid_icon.png"] retain];
            [filesFoundLbl setText:[NSString stringWithFormat:@"%d Files Found",vidCount]];
            countToShow = vidCount;
            itemsToShow = 6;
            chooseFile.hidden = YES;
            [filesList reloadData];
            [self.view addSubview:filesView];
            [topBarView addSubview:backBtn];
            viewToHide = filesView;
            break;
        default:
            break;
    }
}

- (IBAction)tabbarButtonPressed:(UIButton *)sender
{
   
    
    if ([sender.titleLabel.text isEqualToString:documentsBtn.titleLabel.text]) {/*documents botton pressed*/
        if (viewToHide==filesView) {
            [backBtn removeFromSuperview];
            [editDoneBtn removeFromSuperview];
            [viewToHide removeFromSuperview];
            
            [webView removeFromSuperview];
            loadingVideo = NO;
            chooseFile.hidden = NO;
            viewToHide = documentsView;
        }
        if (!documentsBtn.selected) {
            //update main background
            mainBackground.image = [UIImage imageNamed:@"back_1.png"];
            //update buttons
            [chooseFile setImage:[UIImage imageNamed:@"choose_file.png"]];
            [editDoneBtn removeFromSuperview];
            [self updateLabels];
            [webView removeFromSuperview];
            loadingVideo = NO;
            [documentsBtn setSelected:YES];
            [downloadBtn setSelected:NO];
            [settingsBtn setSelected:NO];
            //remove previous view
            [viewToHide removeFromSuperview];
            //show desired view
            [self.view addSubview:documentsView];
            //assign the viewToHide
            viewToHide = documentsView;
        }
                    
        
    }else if([sender.titleLabel.text isEqualToString:downloadBtn.titleLabel.text]){/*download botton pressed*/
        if (viewToHide==filesView) {
            [backBtn removeFromSuperview];
            [viewToHide removeFromSuperview];
            viewToHide = documentsView;
        }
        if (!downloadBtn.selected) {
            mainBackground.image = [UIImage imageNamed:@"back_03.png"];
            [editDoneBtn removeFromSuperview];
            
            [webView removeFromSuperview];
            loadingVideo = NO;
            chooseFile.hidden = NO;
            [chooseFile setImage:[UIImage imageNamed:@"dowload_files.png"]];
            [documentsBtn setSelected:NO];
            [downloadBtn setSelected:YES];
            [settingsBtn setSelected:NO];
            [viewToHide removeFromSuperview];
            [self.view addSubview:downloadView];
            viewToHide = downloadView;
        }
        
    }else if([sender.titleLabel.text isEqualToString:settingsBtn.titleLabel.text]){/*settings botton pressed*/
        if (viewToHide==filesView) {
            [backBtn removeFromSuperview];
            [viewToHide removeFromSuperview];
            viewToHide = documentsView;
        }
        if (!settingsBtn.selected) {
            mainBackground.image = [UIImage imageNamed:@"back_04.png"];
            [editDoneBtn removeFromSuperview];
            
            [webView removeFromSuperview];
            loadingVideo = NO;
            chooseFile.hidden = NO;
            [chooseFile setImage:[UIImage imageNamed:@"settings_button_top.png"]];
            [documentsBtn setSelected:NO];
            [downloadBtn setSelected:NO];
            [settingsBtn setSelected:YES];
            [viewToHide removeFromSuperview];
            [self.view addSubview:settingsView];
            viewToHide = settingsView;
        }
        
    }else if([sender.titleLabel.text isEqualToString:editDoneBtn.titleLabel.text]){/*edit btn pressed*/
        if (filesList.editing == NO) {
            filesList.editing = YES;
            [editDoneBtn setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
            
            [webView removeFromSuperview];
            loadingVideo = NO;
            if (!loadingVideo) {
                UIViewController *c = [[UIViewController alloc]init];
                [self presentModalViewController:c animated:NO];
                [self dismissModalViewControllerAnimated:NO];
                [c release];
            }
        } else {
            [editDoneBtn setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal];
            filesList.editing = NO;
            
            [webView removeFromSuperview];
            
            loadingVideo = NO;
            if (!loadingVideo) {
                UIViewController *c = [[UIViewController alloc]init];
                [self presentModalViewController:c animated:NO];
                [self dismissModalViewControllerAnimated:NO];
                [c release];
            }
        }
    }else{/*Back button pressed*/
        if (loadingVideo) {
            
            [self.view addSubview:editDoneBtn];
            loadingVideo = NO;
            if (!loadingVideo) {
                if (videoWebView!=nil) {
                    [videoWebView stopLoading];
                    [videoWebView reload];
                    [videoWebView removeFromSuperview];
                    videoWebView.delegate = nil;
                    [videoWebView release];
                }

                UIViewController *c = [[UIViewController alloc]init];
                [self presentModalViewController:c animated:NO];
                [self dismissModalViewControllerAnimated:NO];
                [c release];
            }
            return;
        }
        
        if (viewToHide==filesView) {
            [backBtn removeFromSuperview];
            [editDoneBtn removeFromSuperview];
            chooseFile.hidden = NO;
        }
        [viewToHide removeFromSuperview];
        [editDoneBtn removeFromSuperview];
        chooseFile.hidden = NO;
    }
    
    
}

#pragma mark - DownLoadPage Events

-(IBAction)goDownloadBtnPressed:(UIButton *)sender{
    [self presentModalViewController:brouser animated:YES];
}

#pragma mark - TableView Delegate Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return countToShow;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UIView *bgView = [[UIView alloc] initWithFrame:cell.frame];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"table_cel_back.png"]];
//    imageView.frame = cell.frame;
//    [bgView addSubview:imageView];
//    cell.backgroundView = bgView;
//    [bgView release];
//    [imageView release];
//    cell.textLabel.backgroundColor = [UIColor clearColor];
//    cell.contentView.backgroundColor = [UIColor clearColor];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.frame];
    imageView.image = [UIImage imageNamed:@"table_cel_back.png"];
    cell.backgroundView = imageView;
    [imageView release];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    cell.imageView.image = image;
    [cell.textLabel setFont:[UIFont boldSystemFontOfSize:13]];
    cell.textLabel.textColor = [UIColor colorWithRed:0.3f green:0.3f blue:0.3f alpha:1.0f];
    
    cell.textLabel.text = [[filesToShowInTableView objectAtIndex:indexPath.row] lastPathComponent];
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
    
    
    return cell;
}


 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 


 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 
     if (editingStyle == UITableViewCellEditingStyleDelete) {
     
         NSFileManager *filemgr = [NSFileManager defaultManager];
         NSArray *dirPaths;
         NSString *docsDir;
         
         filemgr = [NSFileManager defaultManager];
         dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
         docsDir = [dirPaths objectAtIndex:0];
         [filemgr removeItemAtPath:[docsDir stringByAppendingPathComponent:[filesToShowInTableView objectAtIndex:indexPath.row]] error:nil];
         [filesToShowInTableView removeObjectAtIndex:indexPath.row];
         countToShow--;
         [filemgr release];

 
         // Delete the row from the data source
     
         [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
     
     }   
     [self updateLabels];
 }
 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [editDoneBtn removeFromSuperview];
    // Navigation logic may go here. Create and push another view controller.
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *dirPaths;
    NSString *docsDir;
    
    filemgr = [NSFileManager defaultManager];
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    
    NSString *filePath = [docsDir stringByAppendingPathComponent:[filesToShowInTableView objectAtIndex:indexPath.row]];
    if ([[filePath lowercaseString] hasSuffix:@".3gp"]||[[filePath lowercaseString] hasSuffix:@".m4v"]||
        [[filePath lowercaseString] hasSuffix:@".mp4"]||[[filePath lowercaseString] hasSuffix:@".mov"]) 
    {
        NSLog(@"file path is =%@",filePath);
        videoWebView = [[UIWebView alloc] initWithFrame:documentsView.frame];
        videoWebView.delegate = self;
        CGRect rect = CGRectMake(0, 56, 320, 403);
        videoWebView.frame = rect;
        [self.view addSubview:videoWebView];
        NSURL *url = [NSURL fileURLWithPath:filePath];
        [videoWebView loadRequest:[NSURLRequest requestWithURL:url]];

        loadingVideo = YES;
        [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO animated:NO];
        return;
    }
    
    [self presentModalViewController:viewContr animated:NO];
    [viewContr prepareView:filePath];
    NSLog(@"%@",[docsDir stringByAppendingPathComponent:[filesToShowInTableView objectAtIndex:indexPath.row]]);
    
}

- (void)openDoc:(NSString *)path
{
    
    if ([[path lowercaseString] hasSuffix:@".3gp"]||[[path lowercaseString] hasSuffix:@".m4v"]||
        [[path lowercaseString] hasSuffix:@".mp4"]||[[path lowercaseString] hasSuffix:@".mov"]) 
    {
        
        videoWebView = [[UIWebView alloc] initWithFrame:documentsView.frame];
        //CGRect rect = CGRectMake(0, 56, 320, 403);
        //videoWebView.frame = rect;
        [self.view addSubview:videoWebView];
        NSURL *url = [NSURL fileURLWithPath:path];
        [videoWebView loadRequest:[NSURLRequest requestWithURL:url]];
        
        loadingVideo = YES;
        
        return;
    }
    
    [self presentModalViewController:viewContr animated:NO];
    [viewContr prepareView:path];
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"finish");
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}


@end

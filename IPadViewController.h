//
//  IPadViewController.h
//  ToConvert
//
//  Created by iWanea on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SVSegmentedControl.h"

@class ConverterViewController,ConverterEditorVC;
@interface IPadViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

{
    IBOutlet UITableView *tb;
    NSInteger compoundNumber;
    NSInteger row;
    //UIViewController *converterViewController;
    NSMutableDictionary *convertersDict;
    
    NSMutableArray *searchResults;
    
    IBOutlet UIView *topBar;
    
    IBOutlet UIButton *allConverters;
    IBOutlet UIButton *favConverters;
    IBOutlet UIButton *search;
    IBOutlet UIButton *edit;
    
    BOOL searching;
    IBOutlet UIView *searchView;
    IBOutlet UITextField *searchTextField;
    UILabel *footerLabel;
    
    float previousWidth;
    
    IBOutlet UIView *sectionOneHeader;
    IBOutlet UILabel *addTemp;
    
    BOOL currentFileFavorites;
    BOOL keyboardOnScreen;
    int sectionNumber;
    
    ConverterViewController *converterViewController;
    ConverterEditorVC *converterEditor;
}

- (IBAction)buttonActions:(UIButton*)sender;
- (void)segmentedControlChangedValue:(SVSegmentedControl*)segmentedControl;
@end
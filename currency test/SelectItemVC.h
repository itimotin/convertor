//
//  SelectItemVC.h
//  currency test
//
//  Created by Admin on 15.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectItemVCDelegate
- (void)selectedItem:(NSUInteger)item;
@end

@interface SelectItemVC : UITableViewController
{
    id<SelectItemVCDelegate>delegate;
    NSArray *itemsArray;
}

@property (nonatomic,assign) id<SelectItemVCDelegate>delegate;


- (void)setItemsArray:(NSArray*)array;
@end

//
//  CompoundCell.m
//  currency test
//
//  Created by Admin on 23.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CompoundCell.h"

@interface CompoundCell (Private)

- (void)strechAccessoryViewFor:(NSUInteger)compNum;
- (float)getWidthForCompounds:(NSUInteger)compNum;

@end

@implementation CompoundCell
@synthesize delegate;
@synthesize _ctableView = containerTableView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        CGRect frame = self.frame;
        frame.origin = CGPointZero;
        filterView.frame = frame;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}

- (void)showFilterView:(BOOL)show
{
    [filterView removeFromSuperview];
    if (show) {
        [self addSubview:filterView];
    }
}

- (void)addCompounds:(NSUInteger)compNum
{
//    if (compsAdded) return;
//    compsAdded = YES;
    if (selection!=nil) {
        [selection release],selection=nil;
    }
    selection = [[UIImageView alloc] initWithFrame:CGRectZero];
    selection.backgroundColor = [UIColor colorWithRed:0.2f green:0.6f blue:0.95f alpha:0.35f];
    
    valueFontSize = 16.0f;
    descFontSize = 12.0f;
    
    selectedCompoundNumber = 0;
    compoundsNumber = compNum;
    
    if (compNum==0)return;
    if (valueFontSize==9)return;
    
    CGRect frame = CGRectZero;
    
    if (compOneValue!=nil && compOneDesc!=nil) {
        [compOneValue removeFromSuperview];
        [compOneDesc removeFromSuperview];
        [compOneValue release],compOneValue = nil;
        [compOneDesc release],compOneDesc = nil;
        
    }
    if (compTwoValue!=nil && compTwoDesc!=nil) {
        [compTwoValue removeFromSuperview];
        [compTwoDesc removeFromSuperview];
        [compTwoValue release],compTwoValue = nil;
        [compTwoDesc release],compTwoDesc = nil;
    }
    if (compThrValue!=nil && compThrDesc!=nil) {
        [compThrValue removeFromSuperview];
        [compThrDesc removeFromSuperview];
        [compThrValue release],compThrValue = nil;
        [compThrDesc release],compThrDesc = nil;
    }
    if (compFouValue!=nil && compFouDesc!=nil) {
        [compFouValue removeFromSuperview];
        [compFouDesc removeFromSuperview];
        [compFouValue release],compFouValue = nil;
        [compFouDesc release],compFouDesc = nil;
    }
    
    if (compNum==1) {
        
        
        
        compOneValue = [[UILabel alloc] initWithFrame:frame];
        compOneValue.backgroundColor = [UIColor clearColor];
        compOneValue.textColor = [UIColor blackColor];
//        compOneValue.1font = [UIFont systemFontOfSize:valueFontSize];
        [compOneValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        
        compOneDesc = [[UILabel alloc] initWithFrame:frame];
        compOneDesc.backgroundColor = [UIColor clearColor];
//        compOneDesc.1font = [UIFont systemFontOfSize:descFontSize];
        [compOneDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        
        [self.accessoryView addSubview:compOneValue];
        [self.accessoryView addSubview:compOneDesc];
        
    }else if (compNum==2){
        
        
        
        compOneValue = [[UILabel alloc] initWithFrame:frame];
        compOneValue.backgroundColor = [UIColor clearColor];
        compOneValue.textColor = [UIColor blackColor];
//        compOneValue.1font = [UIFont systemFontOfSize:valueFontSize];
        [compOneValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        compOneDesc = [[UILabel alloc] initWithFrame:frame];
        compOneDesc.backgroundColor = [UIColor clearColor];
//        compOneDesc.1font = [UIFont systemFontOfSize:descFontSize];
        [compOneDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        
        compTwoValue = [[UILabel alloc] initWithFrame:frame];
        compTwoValue.backgroundColor = [UIColor clearColor];
        compTwoValue.textColor = [UIColor blackColor];
        [compTwoValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        compTwoDesc = [[UILabel alloc] initWithFrame:frame];
        compTwoDesc.backgroundColor = [UIColor clearColor];
//        compTwoDesc.1font = [UIFont systemFontOfSize:descFontSize];
        [compTwoDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        
        [self.accessoryView addSubview:compOneValue];
        [self.accessoryView addSubview:compOneDesc];
        [self.accessoryView addSubview:compTwoValue];
        [self.accessoryView addSubview:compTwoDesc];
        
    }else if (compNum==3){
        
        
        
        compOneValue = [[UILabel alloc] initWithFrame:frame];
        compOneValue.backgroundColor = [UIColor clearColor];
        compOneValue.textColor = [UIColor blackColor];
//        compOneValue.1font = [UIFont systemFontOfSize:valueFontSize];
        [compOneValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        compOneDesc = [[UILabel alloc] initWithFrame:frame];
        compOneDesc.backgroundColor = [UIColor clearColor];
//        compOneDesc.1font = [UIFont systemFontOfSize:descFontSize];
        [compOneDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        
        compTwoValue = [[UILabel alloc] initWithFrame:frame];
        compTwoValue.backgroundColor = [UIColor clearColor];
        compTwoValue.textColor = [UIColor blackColor];
//        compTwoValue.1font = [UIFont systemFontOfSize:valueFontSize];
        [compTwoValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        compTwoDesc = [[UILabel alloc] initWithFrame:frame];
        compTwoDesc.backgroundColor = [UIColor clearColor];
//        compTwoDesc.1font = [UIFont systemFontOfSize:descFontSize];
        [compTwoDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        
        compThrValue = [[UILabel alloc] initWithFrame:frame];
        compThrValue.backgroundColor = [UIColor clearColor];
        compThrValue.textColor = [UIColor blackColor];
//        compThrValue.1font = [UIFont systemFontOfSize:valueFontSize];
        [compThrValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        [compOneValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        compThrDesc = [[UILabel alloc] initWithFrame:frame];
        compThrDesc.backgroundColor = [UIColor clearColor];
//        compThrDesc.1font = [UIFont systemFontOfSize:descFontSize];
        [compThrDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        
        [self.accessoryView addSubview:compOneValue];
        [self.accessoryView addSubview:compOneDesc];
        [self.accessoryView addSubview:compTwoValue];
        [self.accessoryView addSubview:compTwoDesc];
        [self.accessoryView addSubview:compThrValue];
        [self.accessoryView addSubview:compThrDesc];
        
    }else{
        
        
        
        compOneValue = [[UILabel alloc] initWithFrame:frame];
        compOneValue.backgroundColor = [UIColor clearColor];
        compOneValue.textColor = [UIColor blackColor];
//        compOneValue.1font = [UIFont systemFontOfSize:valueFontSize];
        [compOneValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        compOneDesc = [[UILabel alloc] initWithFrame:frame];
        compOneDesc.backgroundColor = [UIColor clearColor];
//        compOneDesc.1font = [UIFont systemFontOfSize:descFontSize];
        [compOneDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        
        compTwoValue = [[UILabel alloc] initWithFrame:frame];
        compTwoValue.backgroundColor = [UIColor clearColor];
        compTwoValue.textColor = [UIColor blackColor];
//        compTwoValue.1font = [UIFont systemFontOfSize:valueFontSize];
        [compTwoValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        compTwoDesc = [[UILabel alloc] initWithFrame:frame];
        compTwoDesc.backgroundColor = [UIColor clearColor];
//        compTwoDesc.1font = [UIFont systemFontOfSize:descFontSize];
        [compTwoDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]]; 
        
        compThrValue = [[UILabel alloc] initWithFrame:frame];
        compThrValue.backgroundColor = [UIColor clearColor];
        compThrValue.textColor = [UIColor blackColor];
//        compThrValue.1font = [UIFont systemFontOfSize:valueFontSize];
        [compThrValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        compThrDesc = [[UILabel alloc] initWithFrame:frame];
        compThrDesc.backgroundColor = [UIColor clearColor];
//        compThrDesc.1font = [UIFont systemFontOfSize:descFontSize];
        [compThrDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        
        compFouValue = [[UILabel alloc] initWithFrame:frame];
        compFouValue.textColor = [UIColor blackColor];
        compFouValue.backgroundColor = [UIColor clearColor]; 
//        compFouValue.1font = [UIFont systemFontOfSize:valueFontSize];
        [compFouValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        compFouDesc = [[UILabel alloc] initWithFrame:frame];
        compFouDesc.backgroundColor = [UIColor clearColor];
//        compFouDesc.1font = [UIFont systemFontOfSize:descFontSize];
        [compFouDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
        
        [self.accessoryView addSubview:compOneValue];
        [self.accessoryView addSubview:compOneDesc];
        [self.accessoryView addSubview:compTwoValue];
        [self.accessoryView addSubview:compTwoDesc];
        [self.accessoryView addSubview:compThrValue];
        [self.accessoryView addSubview:compThrDesc];
        [self.accessoryView addSubview:compFouValue];
        [self.accessoryView addSubview:compFouDesc];

    }
    
    [self strechAccessoryViewFor:compNum];
}

- (void)setNameLabelText:(NSString *)text
{
    [nameLabel setText:text];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
    [nameLabel setFont:[UIFont fontWithName:@"Olney-Light" size:23.0]];
    }else{
    [nameLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19.0]];
    }
    
    
//    NSLog(@"compound cell Font : %@", nameLabel.font);

}
- (void)setDescripionLabelText:(NSString *)text
{
    [descriptionLabel setText:text];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){ 
        [descriptionLabel setFont:[UIFont fontWithName:@"Olney-Light" size:24]];
    }else{
        [descriptionLabel setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
    }
}



- (void)setCompound:(NSInteger)compNum Value:(NSString*)value
{
    if (compNum==0)return;
    switch (compNum) {
        case 1:
            [compOneValue setText:value];
            [compOneDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            break;
        case 2:
            [compTwoValue setText:value];
            [compTwoValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            break;
        case 3:
            [compThrValue setText:value];
            [compThrValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            break;
        case 4:
            [compFouValue setText:value];
            [compFouValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            break;
        default:
            break;
    }
    
    [self strechAccessoryViewFor:compoundsNumber];
    [self selectCompound:compNum];
}

- (void)setCompound:(NSInteger)compNum Description:(NSString *)description
{
    if (compNum==0)return;
    switch (compNum) {
        case 1:
            [compOneDesc setText:description];
            [compOneDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            break;
        case 2:
            [compTwoDesc setText:description];
            [compTwoDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            break;
        case 3:
            [compThrDesc setText:description];
            [compThrDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            break;
        case 4:
            [compFouDesc setText:description];
            [compFouDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            break;
        default:
            break;
    }
    
    [self strechAccessoryViewFor:compoundsNumber];
    [self selectCompound:compNum];
}

- (void)setCompoundDescription:(NSString*)description value:(NSString*)value compNum:(NSInteger)compNum
{
    if (compNum==0)return;
    switch (compNum) {
        case 1:
            [compOneDesc setText:description];
            [compOneValue setText:value];
            break;
        case 2:
            [compTwoDesc setText:description];
            [compTwoValue setText:value];
            break;
        case 3:
            [compThrDesc setText:description];
            [compThrValue setText:value];
            break;
        case 4:
            [compFouDesc setText:description];
            [compFouValue setText:value];
            break;
        default:
            break;
    }
    
    [self strechAccessoryViewFor:compoundsNumber];
}

- (void)selectCompound:(NSUInteger)compNum
{
    
    CGRect frame = CGRectZero;
    switch (compNum) {
        case 1:
            
            frame.origin = compOneValue.frame.origin;
            frame.size = CGSizeMake(compOneValue.frame.size.width+2+compOneDesc.frame.size.width, compOneDesc.frame.size.height);
            selection.frame = frame;
            selection.layer.cornerRadius = 5;
            if (![selection isDescendantOfView:self.accessoryView]) {
                [self.accessoryView addSubview:selection];
            }
            break;
        case 2:
            frame.origin = compTwoValue.frame.origin;
            frame.size = CGSizeMake(compTwoValue.frame.size.width+2+compTwoDesc.frame.size.width, compTwoDesc.frame.size.height);
            selection.frame = frame;
            selection.layer.cornerRadius = 5;
            if (![selection isDescendantOfView:self.accessoryView]) {
                [self.accessoryView addSubview:selection];
            }

            break;
        case 3:
            frame.origin = compThrValue.frame.origin;
            frame.size = CGSizeMake(compThrValue.frame.size.width+2+compThrDesc.frame.size.width, compThrDesc.frame.size.height);
            selection.frame = frame;
            selection.layer.cornerRadius = 5;
            if (![selection isDescendantOfView:self.accessoryView]) {
                [self.accessoryView addSubview:selection];
            }

            break;
        case 4:
            frame.origin = compFouValue.frame.origin;
            frame.size = CGSizeMake(compFouValue.frame.size.width+2+compFouDesc.frame.size.width, compFouDesc.frame.size.height);
            selection.frame = frame;
            selection.layer.cornerRadius = 5;
            if (![selection isDescendantOfView:self.accessoryView]) {
                [self.accessoryView addSubview:selection];
            }

            break;
        default:
            if ([selection isDescendantOfView:self.accessoryView]) {
                [selection removeFromSuperview];
            }

            break;
    }
}

- (NSString*)getCompoundValue:(NSUInteger)compNum
{
    
    switch (compNum) {
        case 1:
            return compOneValue.text;
            break;
        case 2:
            return compTwoValue.text;
            break;
        case 3:
            return compThrValue.text;
            break;
        case 4:
            return compFouValue.text;
            break;
        default:
            break;
    }
    return @"KAWABUNGA!";
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:containerTableView];
    
    CGRect comp1;
    comp1.origin = compOneValue.frame.origin;
    comp1.size = CGSizeMake(compOneValue.frame.size.width+2+compOneDesc.frame.size.width, compOneDesc.frame.size.height);
    CGRect comp2;
    comp2.origin = compTwoValue.frame.origin;
    comp2.size = CGSizeMake(compTwoValue.frame.size.width+2+compTwoDesc.frame.size.width, compTwoDesc.frame.size.height);
    CGRect comp3;
    comp3.origin = compThrValue.frame.origin;
    comp3.size = CGSizeMake(compThrValue.frame.size.width+2+compThrDesc.frame.size.width, compThrDesc.frame.size.height);
    CGRect comp4;
    comp4.origin = compFouValue.frame.origin;
    comp4.size = CGSizeMake(compFouValue.frame.size.width+2+compFouDesc.frame.size.width, compFouDesc.frame.size.height);
    
    if (CGRectContainsPoint(comp1, [touch locationInView:self.accessoryView])) {
        [delegate selectedCompound:1 rowLocation:location];
        //[self selectCompound:1];
        selectedCompoundNumber = 1;
    }else if (CGRectContainsPoint(comp2, [touch locationInView:self.accessoryView])) {
        [delegate selectedCompound:2 rowLocation:location];
        //[self selectCompound:2];
        selectedCompoundNumber = 2;
    }else if (CGRectContainsPoint(comp3, [touch locationInView:self.accessoryView])) {
        [delegate selectedCompound:3 rowLocation:location];
        //[self selectCompound:3];
        selectedCompoundNumber = 3;
    }else if (CGRectContainsPoint(comp4, [touch locationInView:self.accessoryView])) {
        [delegate selectedCompound:4 rowLocation:location];
        //[self selectCompound:4];
        selectedCompoundNumber = 4;
    }else{
        //[self selectCompound:selectedCompoundNumber];
        [delegate selectedCompound:selectedCompoundNumber rowLocation:location];
    }
    
}



@end

#pragma mark - 
#pragma mark - CompoundCell Private

@implementation CompoundCell (Private)

- (float)getWidthForCompounds:(NSUInteger)compNum
{
    float width = 0;
    
    if (compNum == 1){
        CGRect rect1 = compOneDesc.frame;
        CGRect rect2 = compOneValue.frame;
        rect1.size = [compOneDesc.text sizeWithFont:compOneDesc.font];
        rect2.size = [compOneValue.text sizeWithFont:compOneValue.font];
        compOneDesc.frame = rect1;
        compOneValue.frame = rect2;
        width  = compOneDesc.frame.size.width + compOneValue.frame.size.width;
    }else if (compNum == 2){
        CGRect rect1 = compOneDesc.frame;
        CGRect rect2 = compOneValue.frame;
        rect1.size = [compOneDesc.text sizeWithFont:compOneDesc.font];
        rect2.size = [compOneValue.text sizeWithFont:compOneValue.font];
        compOneDesc.frame = rect1;
        compOneValue.frame = rect2;
        
        rect1 = compTwoDesc.frame;
        rect2 = compTwoValue.frame;
        
        rect1.size = [compTwoDesc.text sizeWithFont:compTwoDesc.font];
        rect2.size = [compTwoValue.text sizeWithFont:compTwoValue.font];
        compTwoDesc.frame = rect1;
        compTwoValue.frame = rect2;
        
        width  = compOneDesc.frame.size.width + compOneValue.frame.size.width;
        width += compTwoDesc.frame.size.width + compTwoValue.frame.size.width;
    }else if (compNum == 3){
        CGRect rect1 = compOneDesc.frame;
        CGRect rect2 = compOneValue.frame;
        rect1.size = [compOneDesc.text sizeWithFont:compOneDesc.font];
        rect2.size = [compOneValue.text sizeWithFont:compOneValue.font];
        compOneDesc.frame = rect1;
        compOneValue.frame = rect2;
        
        rect1 = compTwoDesc.frame;
        rect2 = compTwoValue.frame;
        
        rect1.size = [compTwoDesc.text sizeWithFont:compTwoDesc.font];
        rect2.size = [compTwoValue.text sizeWithFont:compTwoValue.font];
        compTwoDesc.frame = rect1;
        compTwoValue.frame = rect2;
        
        rect1 = compThrDesc.frame;
        rect2 = compThrValue.frame;
        
        rect1.size = [compThrDesc.text sizeWithFont:compThrDesc.font];
        rect2.size = [compThrValue.text sizeWithFont:compThrValue.font];
        compThrDesc.frame = rect1;
        compThrValue.frame = rect2;
        
        width  = compOneDesc.frame.size.width + compOneValue.frame.size.width;
        width += compTwoDesc.frame.size.width + compTwoValue.frame.size.width;
        width += compThrDesc.frame.size.width + compThrValue.frame.size.width;
    }else{
        CGRect rect1 = compOneDesc.frame;
        CGRect rect2 = compOneValue.frame;
        rect1.size = [compOneDesc.text sizeWithFont:compOneDesc.font];
        rect2.size = [compOneValue.text sizeWithFont:compOneValue.font];
        compOneDesc.frame = rect1;
        compOneValue.frame = rect2;
        
        rect1 = compTwoDesc.frame;
        rect2 = compTwoValue.frame;
        
        rect1.size = [compTwoDesc.text sizeWithFont:compTwoDesc.font];
        rect2.size = [compTwoValue.text sizeWithFont:compTwoValue.font];
        compTwoDesc.frame = rect1;
        compTwoValue.frame = rect2;
        
        rect1 = compThrDesc.frame;
        rect2 = compThrValue.frame;
        
        rect1.size = [compThrDesc.text sizeWithFont:compThrDesc.font];
        rect2.size = [compThrValue.text sizeWithFont:compThrValue.font];
        compThrDesc.frame = rect1;
        compThrValue.frame = rect2;
        
        rect1 = compFouDesc.frame;
        rect2 = compFouValue.frame;
        
        rect1.size = [compFouDesc.text sizeWithFont:compFouDesc.font];
        rect2.size = [compFouValue.text sizeWithFont:compFouValue.font];
        compFouDesc.frame = rect1;
        compFouValue.frame = rect2;
        
        width  = compOneDesc.frame.size.width + compOneValue.frame.size.width;
        width += compTwoDesc.frame.size.width + compTwoValue.frame.size.width;
        width += compThrDesc.frame.size.width + compThrValue.frame.size.width;
        width += compFouDesc.frame.size.width + compFouValue.frame.size.width;
    }
    return width;
}

- (void)strechAccessoryViewFor:(NSUInteger)compNum
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        CGRect frame = self.accessoryView.frame;
        frame.size.width = [self getWidthForCompounds:compNum] + 142;
        if (frame.size.width > 480) {
            return;
        }
        
        while (frame.size.width > 420) {
            
            if (valueFontSize>11) {
                --valueFontSize;
                --descFontSize;
            }else{
                
                frame.size.width = [self getWidthForCompounds:compNum] + 142;
                break;
            }
            
            [compOneValue setFont:[UIFont fontWithName:@"Olney-Light" size:20]];
            compOneValue.textAlignment = UITextAlignmentRight;

            [compOneDesc setFont:[UIFont fontWithName:@"Olney-Light" size:20]];
            compOneDesc.textAlignment = UITextAlignmentRight;
 
            [compTwoValue setFont:[UIFont fontWithName:@"Olney-Light" size:20]];
            compTwoValue.textAlignment = UITextAlignmentRight;
 
            [compTwoDesc setFont:[UIFont fontWithName:@"Olney-Light" size:20]];
            compTwoDesc.textAlignment = UITextAlignmentRight;

            [compThrValue setFont:[UIFont fontWithName:@"Olney-Light" size:20]];
            compThrValue.textAlignment = UITextAlignmentRight;
    
            [compThrDesc setFont:[UIFont fontWithName:@"Olney-Light" size:20]];
            compThrDesc.textAlignment = UITextAlignmentRight;

            [compFouValue setFont:[UIFont fontWithName:@"Olney-Light" size:20]];
            compFouValue.textAlignment = UITextAlignmentRight;

            [compFouDesc setFont:[UIFont fontWithName:@"Olney-Light" size:20]];
            compFouDesc.textAlignment = UITextAlignmentRight;
            frame.size.width = [self getWidthForCompounds:compNum] + 142;

        }
        
        self.accessoryView.frame = frame;

        CGRect rect1;
        CGRect rect2;
        
        if (compNum==1) {
            rect1 = compOneDesc.frame;
            rect2 = compOneValue.frame;
            
            rect1.origin = CGPointMake(frame.size.width - rect1.size.width + 2, 25);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width + 2, 25);
            
            compOneDesc.frame  = rect1;
            compOneValue.frame = rect2;
            
        }else if (compNum == 2){
            rect1 = compTwoDesc.frame;
            rect2 = compTwoValue.frame;
            
            rect1.origin = CGPointMake(frame.size.width - rect1.size.width - 2, 25);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 25);
            
            compTwoDesc.frame  = rect1;
            compTwoValue.frame = rect2;
            
            rect1 = compOneDesc.frame;
            rect2 = compOneValue.frame;
            
            rect1.origin = CGPointMake(compTwoValue.frame.origin.x - rect1.size.width - 2, 25);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 25);
            
            compOneDesc.frame  = rect1;
            compOneValue.frame = rect2;
            
        }else if (compNum == 3){
            rect1 = compThrDesc.frame;
            rect2 = compThrValue.frame;
            
            rect1.origin = CGPointMake(frame.size.width - rect1.size.width - 2, 25);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 25);
            
            compThrDesc.frame  = rect1;
            compThrValue.frame = rect2;
            
            rect1 = compTwoDesc.frame;
            rect2 = compTwoValue.frame;
            
            rect1.origin = CGPointMake(compThrValue.frame.origin.x - rect1.size.width - 2, 25);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 25);
            
            compTwoDesc.frame  = rect1;
            compTwoValue.frame = rect2;
            
            rect1 = compOneDesc.frame;
            rect2 = compOneValue.frame;
            
            rect1.origin = CGPointMake(compTwoValue.frame.origin.x - rect1.size.width - 2, 25);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 25);
            
            compOneDesc.frame  = rect1;
            compOneValue.frame = rect2;
            
        }else{
            rect1 = compFouDesc.frame;
            rect2 = compFouValue.frame;
            
            rect1.origin = CGPointMake(frame.size.width - rect1.size.width - 2, 25);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 25);
            
            compFouDesc.frame  = rect1;
            compFouValue.frame = rect2;
            
            rect1 = compThrDesc.frame;
            rect2 = compThrValue.frame;
            
            rect1.origin = CGPointMake(compFouValue.frame.origin.x - rect1.size.width - 2, 25);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 25);
            
            compThrDesc.frame  = rect1;
            compThrValue.frame = rect2;
            
            rect1 = compTwoDesc.frame;
            rect2 = compTwoValue.frame;
            
            rect1.origin = CGPointMake(compThrValue.frame.origin.x - rect1.size.width - 2, 25);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 25);
            
            compTwoDesc.frame  = rect1;
            compTwoValue.frame = rect2;
            
            rect1 = compOneDesc.frame;
            rect2 = compOneValue.frame;
            
            rect1.origin = CGPointMake(compTwoValue.frame.origin.x - rect1.size.width - 2, 25);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 25);
            
            compOneDesc.frame  = rect1;
            compOneValue.frame = rect2;
            
        }
        
    }else{
        
        CGRect frame = self.accessoryView.frame;
        frame.size.width = [self getWidthForCompounds:compNum] + 42;
        if (frame.size.width > 280) {
            return;
        }

        while (frame.size.width > 220) {
            
            if (valueFontSize>11) {
                --valueFontSize;
                --descFontSize;
            }else{
                
                frame.size.width = [self getWidthForCompounds:compNum] + 42;
                break;
            }
            
    //        compOneValue.1font = [UIFont systemFontOfSize:valueFontSize];
            [compOneValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            compOneValue.textAlignment = UITextAlignmentRight;
    //        compOneDesc.1font = [UIFont systemFontOfSize:descFontSize];
            [compOneDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            compOneDesc.textAlignment = UITextAlignmentRight;
    //        compTwoValue.1font = [UIFont systemFontOfSize:valueFontSize];
            [compTwoValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            compTwoValue.textAlignment = UITextAlignmentRight;
    //        compTwoDesc.1font = [UIFont systemFontOfSize:descFontSize];
            [compTwoDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            compTwoDesc.textAlignment = UITextAlignmentRight;
    //        compThrValue.1font = [UIFont systemFontOfSize:valueFontSize];
            [compThrValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            compThrValue.textAlignment = UITextAlignmentRight;
    //        compThrDesc.1font = [UIFont systemFontOfSize:descFontSize];
            [compThrDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            compThrDesc.textAlignment = UITextAlignmentRight;
    //        compFouValue.1font = [UIFont systemFontOfSize:valueFontSize];
            [compFouValue setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            compFouValue.textAlignment = UITextAlignmentRight;
    //        compFouDesc.1font = [UIFont systemFontOfSize:descFontSize];
            [compFouDesc setFont:[UIFont fontWithName:@"Olney-Light" size:16]];
            compFouDesc.textAlignment = UITextAlignmentRight;
            frame.size.width = [self getWidthForCompounds:compNum] + 42;
    //        accessoryBackground.image = [[UIImage imageNamed:@"mony_back.png"] stretchableImageWithLeftCapWidth:42 topCapHeight:10];
        }
        
        self.accessoryView.frame = frame;
    //    self.accessoryView.frame = CGRectMake(468, 0, 300, 66);
        CGRect rect1;
        CGRect rect2;
        
        if (compNum==1) {
            rect1 = compOneDesc.frame;
            rect2 = compOneValue.frame;
            
            rect1.origin = CGPointMake(frame.size.width - rect1.size.width+2, 5);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width +2, 5);
            
            compOneDesc.frame  = rect1;
            compOneValue.frame = rect2;
            
        }else if (compNum == 2){
            rect1 = compTwoDesc.frame;
            rect2 = compTwoValue.frame;
            
            rect1.origin = CGPointMake(frame.size.width - rect1.size.width - 2, 5);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 5);
            
            compTwoDesc.frame  = rect1;
            compTwoValue.frame = rect2;
            
            rect1 = compOneDesc.frame;
            rect2 = compOneValue.frame;
            
            rect1.origin = CGPointMake(compTwoValue.frame.origin.x - rect1.size.width - 2, 5);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 5);
            
            compOneDesc.frame  = rect1;
            compOneValue.frame = rect2;
            
        }else if (compNum == 3){
            rect1 = compThrDesc.frame;
            rect2 = compThrValue.frame;
            
            rect1.origin = CGPointMake(frame.size.width - rect1.size.width - 2, 5);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 5);
            
            compThrDesc.frame  = rect1;
            compThrValue.frame = rect2;
            
            rect1 = compTwoDesc.frame;
            rect2 = compTwoValue.frame;
            
            rect1.origin = CGPointMake(compThrValue.frame.origin.x - rect1.size.width - 2, 5);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 5);
            
            compTwoDesc.frame  = rect1;
            compTwoValue.frame = rect2;
            
            rect1 = compOneDesc.frame;
            rect2 = compOneValue.frame;
            
            rect1.origin = CGPointMake(compTwoValue.frame.origin.x - rect1.size.width - 2, 5);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 5);
            
            compOneDesc.frame  = rect1;
            compOneValue.frame = rect2;
            
        }else{
            rect1 = compFouDesc.frame;
            rect2 = compFouValue.frame;
            
            rect1.origin = CGPointMake(frame.size.width - rect1.size.width - 2, 5);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 5);
            
            compFouDesc.frame  = rect1;
            compFouValue.frame = rect2;
            
            rect1 = compThrDesc.frame;
            rect2 = compThrValue.frame;
            
            rect1.origin = CGPointMake(compFouValue.frame.origin.x - rect1.size.width - 2, 5);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 5);
            
            compThrDesc.frame  = rect1;
            compThrValue.frame = rect2;
            
            rect1 = compTwoDesc.frame;
            rect2 = compTwoValue.frame;
            
            rect1.origin = CGPointMake(compThrValue.frame.origin.x - rect1.size.width - 2, 5);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 5);
            
            compTwoDesc.frame  = rect1;
            compTwoValue.frame = rect2;
            
            rect1 = compOneDesc.frame;
            rect2 = compOneValue.frame;
            
            rect1.origin = CGPointMake(compTwoValue.frame.origin.x - rect1.size.width - 2, 5);
            rect2.origin = CGPointMake(rect1.origin.x - rect2.size.width - 2, 5);
            
            compOneDesc.frame  = rect1;
            compOneValue.frame = rect2;
            
        }
    }
}


@end

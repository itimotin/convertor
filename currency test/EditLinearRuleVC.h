//
//  EditLinearRuleVC.h
//  currency test
//
//  Created by Admin on 14.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@protocol EditLinearRuleVCDelegate

- (void)saveLinearRule:(NSDictionary*)ruleDict;

@end

@interface EditLinearRuleVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
{
    id<EditLinearRuleVCDelegate>delegate;
    IBOutlet UITableView *editorTable;
    NSString *itemName;
    NSString *refItemName;
    
    IBOutlet UITableViewCell *slopeValCell;
    IBOutlet UITableViewCell *interceptValCell;
    
    NSArray *itemsArray;
    
    BOOL itemORrefitem;
    
    BOOL keyboardOnScreen;
    
    //textField pointers
    UITextField *_pSlope;
    UITextField *_pIntercept;
    
    NSMutableDictionary *ruleDict;
}

@property (nonatomic, assign) id<EditLinearRuleVCDelegate>delegate;

@property (nonatomic, retain) IBOutlet UITableViewCell *slopeValCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *interceptValCell;

- (IBAction)goBack;
- (IBAction)save;

- (void)setItemsArray:(NSArray*)array;
- (void)setRuleDictionary:(NSDictionary*)dictionary;

@end

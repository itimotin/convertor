//
//  AddItemVC.m
//  currency test
//
//  Created by Admin on 15.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AddItemVC.h"

@implementation AddItemVC

@synthesize delegate;
@synthesize nameCell,descCell,valCell,deciCell,compCell;

- (void)goBack
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)save
{
    BOOL goodForSave = YES;
    char errorType = 0;
    //chech for valid item
    if (itemType==1) {
         tf.font =[UIFont fontWithName:@"Olney-Light" size:17];
        tf.textColor = [UIColor blackColor];
        if (tf!=NULL) {
            if ([tf.text rangeOfString:@"##"].location!=NSNotFound ||
                [tf.text rangeOfString:@"  "].location!=NSNotFound ||
                [tf.text rangeOfString:@" #"].location!=NSNotFound ||
                [tf.text characterAtIndex:0]!='#') {
                
                goodForSave = NO;
                errorType = 1;//invalid compound formula
            }
        }else{
            goodForSave = NO;
            errorType = 2;//compound type selected but pointer is NULL
        }
    }
    
    if (ntf==NULL) {
        goodForSave = NO;
        errorType = 3;//name pointer is NULL
    }else{
        if ([ntf.text isEqualToString:@""]) {
            goodForSave = NO;
            errorType = 4;//name string is empty
        }
    }
    
    //to save or not to save...
    
    if (errorType!=0) {
        UIView *errorAllertview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 150)];
        UILabel *errorAllertLabel = [[UILabel alloc] initWithFrame:errorAllertview.frame];
        errorAllertview.center= self.view.center;
        errorAllertLabel.textColor = [UIColor blackColor];
        errorAllertLabel.textAlignment = UITextAlignmentCenter;
        errorAllertLabel.font = [UIFont fontWithName:@"Olney-Light" size:17];
    
        errorAllertLabel.numberOfLines = 5;
        errorAllertLabel.backgroundColor = [UIColor clearColor];
        [errorAllertview.layer setCornerRadius:15];
        errorAllertview.backgroundColor = [UIColor colorWithRed:1.0f green:0.3f blue:0.3f alpha:0.8f];
        errorAllertview.alpha = 0.0f;
        
        

        if (keyboardOnScreen) {
            switch (currentTf) {
                    
                case 1:
                    
                    if (keyboardOnScreen) {
                        CGRect frame = addItemTableView.frame;
                        frame.size.height += 216.0f;
                        [UIView animateWithDuration:0.3f animations:^{
                            addItemTableView.frame = frame;
                        }];
                        keyboardOnScreen = NO;
                    }
                    [ntf resignFirstResponder];
                    break;
                case 2:
                    if (keyboardOnScreen) {
                        CGRect frame = addItemTableView.frame;
                        frame.size.height += 216.0f;
                        [UIView animateWithDuration:0.3f animations:^{
                            addItemTableView.frame = frame;
                        }];
                        keyboardOnScreen = NO;
                    }
                    [dtf resignFirstResponder];
                    break;
                case 3:
                    if (keyboardOnScreen) {
                        CGRect frame = addItemTableView.frame;
                        frame.size.height += 216.0f;
                        [UIView animateWithDuration:0.3f animations:^{
                            addItemTableView.frame = frame;
                        }];
                        keyboardOnScreen = NO;
                    }
                    
                    [vtf resignFirstResponder];
                    break;
                case 4:
                    if (keyboardOnScreen) {
                        CGRect frame = addItemTableView.frame;
                        frame.size.height += 216.0f;
                        [UIView animateWithDuration:0.3f animations:^{
                            addItemTableView.frame = frame;
                        }];
                        keyboardOnScreen = NO;
                    }
                    [tf resignFirstResponder];
                    break;
                
            }
        }
        
        switch (errorType) {
            case 1:
                [errorAllertLabel setText:@"Error\n\ninvalid compound formula"];
                errorAllertLabel.font =[UIFont fontWithName:@"Olney-Light" size:17];
                [errorAllertview addSubview:errorAllertLabel];
                [self.view addSubview:errorAllertview];
                
                [UIView animateWithDuration:0.2f animations:^{
                    errorAllertview.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:2.0f animations:^{
                        errorAllertview.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        [errorAllertview removeFromSuperview];
                        [errorAllertview release];
                        [errorAllertLabel release];
                    }];                
                }];
                break;
            case 2:
                [errorAllertLabel setText:@"Error\n\ncompound type selected but pointer is NULL"];
                 errorAllertLabel.font =[UIFont fontWithName:@"Olney-Light" size:17];
                [errorAllertview addSubview:errorAllertLabel];
                [self.view addSubview:errorAllertview];
                [UIView animateWithDuration:0.2f animations:^{
                    errorAllertview.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:2.0f animations:^{
                        errorAllertview.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        [errorAllertview removeFromSuperview];
                        [errorAllertview release];
                        [errorAllertLabel release];
                    }];                
                }];
                break;
            case 3:
                [errorAllertLabel setText:@"Error\n\nname pointer is NULL"];
                 errorAllertLabel.font =[UIFont fontWithName:@"Olney-Light" size:17];
                [errorAllertview addSubview:errorAllertLabel];
                [self.view addSubview:errorAllertview];
                [UIView animateWithDuration:0.2f animations:^{
                    errorAllertview.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:2.0f animations:^{
                        errorAllertview.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        [errorAllertview removeFromSuperview];
                        [errorAllertview release];
                        [errorAllertLabel release];
                    }];                
                }];
                break;
            case 4:
                [errorAllertLabel setText:@"Error\n\nname string is empty"];
                 errorAllertLabel.font =[UIFont fontWithName:@"Olney-Light" size:17];
                [errorAllertview addSubview:errorAllertLabel];
                [self.view addSubview:errorAllertview];
                [UIView animateWithDuration:0.2f animations:^{
                    errorAllertview.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:2.0f animations:^{
                        errorAllertview.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        [errorAllertview removeFromSuperview];
                        [errorAllertview release];
                        [errorAllertLabel release];
                    }];                
                }];
                break;
            
        }
        
        
        return;
    }
    
    if (itemDictionary == nil) {
        //[itemDictionary release],itemDictionary = nil;
        itemDictionary = [[NSMutableDictionary alloc] initWithCapacity:1];
        [itemDictionary setValue:@"decimal" forKey:@"type"];
        [itemDictionary setValue:[NSNumber numberWithDouble:0] forKey:@"value"];
        [itemDictionary setValue:[NSNumber numberWithBool:NO] forKey:@"checked"];
        [itemDictionary setValue:@"" forKey:@"description"];
    }
    if (itemType==1) {
        [itemDictionary setValue:@"compound" forKey:@"type"];
        [itemDictionary setValue:tf.text forKey:@"compound"];
        int compNum = 0;
        for (int i = 0; i<[tf.text length]; i++) {
            if ([tf.text characterAtIndex:i]=='#') {
                tf.font =[UIFont fontWithName:@"Olney-Light" size:17];
                tf.textColor = [UIColor blackColor];
                compNum++;
            }
        }
        [itemDictionary setValue:[NSNumber numberWithInt:compNum] forKey:@"compNum"];
    }
    if (![vtf.text isEqualToString:@""]) {
        [itemDictionary setValue:[NSNumber numberWithDouble:[vtf.text doubleValue]] forKey:@"value"];
    }
    
    if(dtf)
        [itemDictionary setValue:dtf.text forKey:@"description"];
    else
        [itemDictionary setValue:@"" forKey:@"description"];
//    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *filePath = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"testItem.plist"];
//    [itemDictionary writeToFile:filePath atomically:YES];
    [delegate saveItem:itemDictionary WithKeyName:ntf.text];
    [self dismissModalViewControllerAnimated:YES];
}

-(void)setItem:(NSString *)itemName Dictionary:(NSDictionary *)dictionary
{
    if (itemDictionary) {
        [itemDictionary release],itemDictionary = nil;
    }
    if (_aitemName)
        [_aitemName release],_aitemName = nil;
    
    itemDictionary = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
    [[itemDictionary valueForKey:@"type"] isEqualToString:@"compound"] ? (itemType=1):(itemType=0);
    [titleLabel setText:@"Edit Item"];
    _aitemName = [[NSString alloc] initWithString:itemName];
    ntf.text = _aitemName;
     ntf.font =[UIFont fontWithName:@"Olney-Light" size:17];
    dtf.text = [itemDictionary valueForKey:@"description"];
     dtf.font =[UIFont fontWithName:@"Olney-Light" size:17];
    ntf.textColor = [UIColor blackColor];
    dtf.textColor = [UIColor blackColor];
    if (itemType==0) {
        
    }else{
        tf.text = [itemDictionary valueForKey:@"compound"];
        tf.font= [UIFont fontWithName:@"Olney-Light" size:17];
        tf.textColor = [UIColor blackColor];
    }
    
    [addItemTableView reloadData];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self = [super initWithNibName:@"IPadAddItemVC" bundle:nibBundleOrNil];
        
    }
    else
    {
        self = [super initWithNibName:@"AddItemVC" bundle:nibBundleOrNil];
    }
    if (self) {
        // Custom initialization
        [titleLabel setText:@"Add Item"];
        titleLabel.font = [UIFont fontWithName:@"Olney-Light" size:17];
        titleLabel.textColor = [UIColor whiteColor];
//        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
        self.view.backgroundColor = [UIColor blackColor];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    itemType = 0;
    
//   self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
   self.view.backgroundColor = [UIColor blackColor ];    
    [back.imageView.layer setCornerRadius:5];
    [save.imageView.layer setCornerRadius:5];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    if (itemDictionary)
        [itemDictionary release],itemDictionary = nil;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    if (itemDictionary==nil) {
        itemDictionary = [[NSMutableDictionary alloc] initWithCapacity:1];
        [itemDictionary setValue:@"decimal" forKey:@"type"];
        [itemDictionary setValue:[NSNumber numberWithDouble:0] forKey:@"value"];
        [itemDictionary setValue:[NSNumber numberWithBool:NO] forKey:@"checked"];
        [itemDictionary setValue:@"" forKey:@"description"];
    }
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    if (itemDictionary)
        [itemDictionary release],itemDictionary = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    if (section==1) {
        return 2;
    }
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *nameCellID = @"_kNameCell";
    static NSString *descCellID = @"_kDescCell";
    static NSString *valCellID  = @"_kValCell";
    static NSString *deciCellID = @"_kDeciCell";
    static NSString *compCellID = @"_kCompCell";
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [[NSBundle mainBundle] loadNibNamed:@"IPadAddItemCells" owner:self options:nil];
        
    }
    else
    {
        [[NSBundle mainBundle] loadNibNamed:@"AddItemCells" owner:self options:nil];
    }
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nameCellID];
            if (cell == nil) {
                cell = nameCell;
                self.nameCell = nil;
                for (UIView *subV in [cell.contentView subviews]) {
                    
                    if  ([subV isKindOfClass:[UITextField class]])
                    {
                        ntf = (UITextField*)subV;
                        ntf.font = [UIFont fontWithName:@"Olney-Light" size:17];
                        ntf.textColor = [UIColor blackColor];
                    }
                }
            }
            // Configure the cell...
            if (_aitemName) 
                [ntf setText:_aitemName];
                ntf.font = [UIFont fontWithName:@"Olney-Light" size:17];
                ntf.textColor = [UIColor blackColor];
            
            return cell;
            
        }else if (indexPath.row == 1){
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:descCellID];
            if (cell == nil) {
                cell = descCell;
                self.descCell = nil;
                for (UIView *subV in [cell.contentView subviews]) {
                    
                    if  ([subV isKindOfClass:[UITextField class]])
                    {
                        dtf = (UITextField*)subV;
                        dtf.font =[UIFont fontWithName:@"Olney-Light" size:17];
                        dtf.textColor = [UIColor blackColor];
                    }
                }
            }
            // Configure the cell...
            if ([itemDictionary valueForKey:@"description"]) {
               
                [dtf setText:[itemDictionary valueForKey:@"description"]];
                dtf.font = [UIFont fontWithName:@"Olney-Light" size:17];
                dtf.textColor = [UIColor blackColor];
            }
            return cell;
            
        }else if (indexPath.row == 2){
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:valCellID];
            if (cell == nil) {
                cell = valCell;
                self.valCell = nil;
                for (UIView *subV in [cell.contentView subviews]) {
                    
                    if  ([subV isKindOfClass:[UITextField class]])
                    {
                        vtf = (UITextField*)subV;
                        vtf.font = [UIFont fontWithName:@"Olney-Light" size:17];
                        vtf.textColor = [UIColor blackColor];
                    }
                }
            }
            // Configure the cell...
            
            return cell;
            
        }
    }else if (indexPath.section == 1){
        
        if (indexPath.row == 0) {
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:deciCellID];
            if (cell == nil) {
                cell = deciCell;
                self.deciCell = nil;
                
            }
            // Configure the cell...
            if (itemType==0) cell.backgroundColor = [UIColor colorWithRed:0.3f green:1.0f blue:0.3f alpha:.55f];
            else cell.backgroundColor = [UIColor whiteColor];
            return cell;
            
        }else if (indexPath.row == 1){
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:compCellID];
            if (cell == nil) {
                cell = compCell;
                self.compCell = nil;
                //UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(113, 6, 172, 31)];
                for (UIView *subV in [cell.contentView subviews]) {
                    
                    if  ([subV isKindOfClass:[UITextField class]])
                    {
                        tf = (UITextField*)subV;
                        tf.font =[UIFont fontWithName:@"Olney-Light" size:17];
                        tf.textColor =[UIColor blackColor];
                    }
                }
                
            }
            // Configure the cell...
            if ([itemDictionary valueForKey:@"compound"]) {
                [tf setText:[itemDictionary valueForKey:@"compound"]];
                tf.font = [UIFont fontWithName:@"Olney-Light" size:17];
                tf.textColor = [UIColor blackColor];
            }
            if (itemType==1) cell.backgroundColor = [UIColor colorWithRed:0.3f green:1.0f blue:0.3f alpha:.55f];
            else cell.backgroundColor = [UIColor whiteColor];
            return cell;
            
        }
        
    }
    
    
    
    

    
    
    
    return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section==1)
        return @"Format";
    return @"Item";
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return nil;
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
    if (indexPath.row==1)
    {
        [tableView cellForRowAtIndexPath:indexPath].backgroundColor = [UIColor colorWithRed:0.3f green:1.0f blue:0.3f alpha:.55f];
        [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]].backgroundColor = [UIColor whiteColor];
        [[tableView cellForRowAtIndexPath:indexPath].accessoryView becomeFirstResponder];
        itemType = 1;//compound
    }else{
        itemType = 0;//decimal
        [tableView cellForRowAtIndexPath:indexPath].backgroundColor = [UIColor colorWithRed:0.3f green:1.0f blue:0.3f alpha:.55f];;
        [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]].backgroundColor = [UIColor whiteColor];
        if (tf) {
            if (keyboardOnScreen) {
                CGRect frame = addItemTableView.frame;
                frame.size.height += 216.0f;
                [UIView animateWithDuration:0.3f animations:^{
                    addItemTableView.frame = frame;
                }];
                keyboardOnScreen = NO;
            }
            [tf resignFirstResponder];
        }
        
    }
    
    
}

#pragma mark - TextField delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.font =[UIFont fontWithName:@"Olney-Light" size:17];
    textField.textColor = [UIColor blackColor];
    
    if ([textField.placeholder isEqualToString:@"Item Name"])
        if (ntf==NULL)
            ntf = textField,currentTf=1;
    ntf.font = [UIFont fontWithName:@"Olney-Light" size:17];
    ntf.textColor = [UIColor blackColor];
    
    if ([textField.placeholder isEqualToString:@"Item Description"])
        if (dtf==NULL)
            textField.font =[UIFont fontWithName:@"Olney-Light" size:17];
            dtf = textField,currentTf=2;
    dtf.textColor = [UIColor blackColor];
    dtf.font =[UIFont fontWithName:@"Olney-Light" size:17];
    
    if ([textField.placeholder isEqualToString:@"(Optional) Default Value"])
        if (vtf==NULL)
            vtf = textField,currentTf=3;
    vtf.font = [UIFont fontWithName:@"Olney-Light" size:17];
    vtf.textColor = [UIColor blackColor];
    
    
    if (!keyboardOnScreen) {
        CGRect frame = addItemTableView.frame;
        frame.size.height -= 216.0f;
        [UIView animateWithDuration:0.3f animations:^{
            addItemTableView.frame = frame;
        }];
        keyboardOnScreen = YES;
    }  
    if ([textField.placeholder isEqualToString:@"N1#postfix N2#postfix"]) {
        currentTf = 4;
        itemType = 1;//compound
        [addItemTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]].backgroundColor = [UIColor colorWithRed:0.3f green:1.0f blue:0.3f alpha:.55f];
        [addItemTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]].backgroundColor = [UIColor whiteColor];
        if(tf==NULL)
            tf = textField;
        tf.font = [UIFont fontWithName:@"Olney-Light" size:17];
        tf.textColor = [UIColor blackColor];
        if ([textField.text isEqualToString:@""]) 
            [textField setText:@"#"];
        textField.font = [UIFont fontWithName:@"Olney-Light" size:17];
        textField.textColor = [UIColor blackColor];
        [addItemTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        [addItemTableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1] animated:YES];
    }  
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
     textField.font =[UIFont fontWithName:@"Olney-Light" size:17];
    if (keyboardOnScreen) {
        CGRect frame = addItemTableView.frame;
        frame.size.height += 216.0f;
        [UIView animateWithDuration:0.3f animations:^{
            addItemTableView.frame = frame;
        }];
        keyboardOnScreen = NO;
    }
    [textField resignFirstResponder];
    return YES;
}


@end

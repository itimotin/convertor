//
//  Converter.m
//  currency test
//
//  Created by Admin on 27.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Converter.h"
#import "Item.h"
#import "FormulaParser.h"


@interface Converter (Private)
- (void)updateValues;
- (void)updateCurencyRates;
@end

@implementation Converter


@synthesize convNum,removeZeros;
@synthesize delegate;
@synthesize isFav;

- (void)writeDictToFile
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"All.plist"];
    NSString *favPath = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"Favorites.plist"];
    NSMutableDictionary *dictToWrite;
    if (isFav) {
        dictToWrite = [NSMutableDictionary dictionaryWithContentsOfFile:favPath];
    }else{
        dictToWrite = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    }
    
    NSString *converterKey = [NSString stringWithFormat:@"Converter %d",convNum];
    [dictToWrite setValue:converter forKey:converterKey];
    if (isFav) {
        [dictToWrite writeToFile:favPath atomically:YES];
    }else{
        [dictToWrite writeToFile:path atomically:YES];
    }
    
}

- (void)dealloc
{
    [itemsToShow release],itemsToShow = nil;
    [converter release],converter = nil;
}

- (void)updateItemsToShow
{
    if (itemsToShow!=nil) {
        [itemsToShow release],itemsToShow=nil;
    }
    itemsToShow = [[NSMutableArray alloc] initWithCapacity:1];
    for (NSString *s in items) {
        if ([[[converter valueForKey:s] valueForKey:@"checked"] boolValue]) {
            [itemsToShow addObject:s];
        }
    }
    
}

- (NSDictionary *)getConverterDictionary
{
    return converter;
}

- (void)hideItemForRow:(NSUInteger)row
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[self allItems_itemForRow:row]];
    
    BOOL showing = [[dict valueForKey:@"checked"] boolValue];
    
    showing ? (showing = NO) : (showing = YES);
    
    
    [dict setValue:[NSNumber numberWithBool:showing] forKey:@"checked"];
    
    [converter setValue:dict forKey:[self allItems_itemNameForRow:row]];
    [dict release];
    [self updateItemsToShow];
    
}

- (void)moveItemFrom:(NSUInteger)sourceRow to:(NSUInteger)destinationRow
{
    NSString *itemToMove = [[NSString alloc] initWithString:[items objectAtIndex:sourceRow]];
    [items removeObjectAtIndex:sourceRow];
    [items insertObject:itemToMove atIndex:destinationRow];
    [self updateItemsToShow];
    [converter setValue:items forKey:@"Items"];
    //[self writeDictToFile];
    //[delegate convUpdated];
    
}

- (id)initWithDict:(NSDictionary*)dict
{
    if (self = [super init]) {
        if (converter) {
            [converter release],converter = nil;
        }
        converter = [[NSMutableDictionary alloc] initWithDictionary:dict];
        if ([[converter valueForKey:@"Name"] isEqualToString:@"Currency"]) {
            [self updateCurencyRates];
        }
        if (converter!=nil) {
            
            editedKey = [[NSString alloc] initWithString:@""];
            NSArray *rules = [converter valueForKey:@"rules"];
            formulaRuleCount = 0;
            linearRuleCount = 0;
            for (NSDictionary *rule in rules) {
                if ([[rule valueForKey:@"type"] isEqualToString:@"formula"])
                    formulaRuleCount++;
                if ([[rule valueForKey:@"type"] isEqualToString:@"linear"])
                    linearRuleCount++;
            }
            if (itemsToShow)
                [itemsToShow release],itemsToShow = nil;
            if (items)
                [items release],items = nil;
            
            itemsToShow = [[NSMutableArray alloc] initWithCapacity:1];
            items = [[NSMutableArray alloc] initWithArray:[converter valueForKey:@"Items"]];
            removeZeros = YES;
            [self updateItemsToShow];
        }
    }
    
    
    
    return self;
}

- (NSUInteger)showingItems
{
    return [itemsToShow count];
}

- (NSDictionary*)itemForRow:(NSUInteger)row
{
    return [converter valueForKey:[itemsToShow objectAtIndex:row]];
}

- (NSString *)itemNameForRow:(NSUInteger)row
{
    return [itemsToShow objectAtIndex:row];
}

- (NSDictionary*)allItems_itemForRow:(NSUInteger)row
{
    return [converter valueForKey:[items objectAtIndex:row]];
}
- (NSString*)allItems_itemNameForRow:(NSUInteger)row
{
    return [items objectAtIndex:row];
}

- (NSString*)valueForRow:(NSUInteger)row
{
    double val = [[[converter valueForKey:[itemsToShow objectAtIndex:row]] valueForKey:@"value"] doubleValue];
    NSMutableString *valStr;
    if (val == 0) {
        valStr = [NSMutableString stringWithFormat:@"0.00"];
    }else if ( val>99999999999 || fabs(val)<0.000000001) {
        valStr = [NSMutableString stringWithFormat:@"%E",val];
    }else{
        valStr = [NSMutableString stringWithFormat:@"%.10f",val];
    }
    
    int maxIndex = [valStr length] - 1;
    
    for (int i = maxIndex; i>=0; i--) {
        if ([valStr characterAtIndex:i]=='0')
        {
            [valStr deleteCharactersInRange:NSMakeRange(i, 1)];
            continue;
        }else{
            if ([valStr characterAtIndex:i]=='.') {
                [valStr deleteCharactersInRange:NSMakeRange(i, 1)];
                break;
            }
            break;
        }
        
    }
    
    return valStr;
}



- (void)setNewValue:(NSString*)value forKey:(NSString*)key
{
    
    
    NSMutableDictionary *dict = [converter valueForKey:key];
    NSString *etalonKey = [converter valueForKey:@"etalonValue"];
    editedKey = key;
    
    if (etalonKey) {
        double newEtVal = [value doubleValue] / [[dict valueForKey:@"rate"] doubleValue] ;
        NSMutableDictionary *etDict = [converter valueForKey:etalonKey];
        [etDict setValue:[NSNumber numberWithDouble:newEtVal] forKey:@"value"];
        [converter setValue:etDict forKey:etalonKey];
    }
    
    [dict setValue:[NSNumber numberWithDouble:[value doubleValue]] forKey:@"value"];
    [converter setValue:dict forKey:key];
    
    [self updateValues];
}


@end
#pragma mark - Converter (Private)
@implementation Converter (Private)

- (void)updateCurencyRates
{
    
    NSMutableArray *mutaro;
    NSURLResponse *response;
    NSError *error;
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://raw.github.com/currencybot/open-exchange-rates/master/latest.json"]];
    NSData *responseData = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];   
    NSString *responseString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    int statusCode = [httpResponse statusCode];
    
    if (responseString!=NULL) {
        mutaro = [[NSMutableArray alloc] initWithCapacity:1];
        [responseString enumerateLinesUsingBlock:^(NSString *line, BOOL *stop) {
            [mutaro addObject:line]; 
        }];
        
    }
    if (statusCode!=200) {
        return;
    }
    while ([[mutaro lastObject] rangeOfString:@"}"].location!=NSNotFound) {
        [mutaro removeLastObject];
    }
    
    
    if (statusCode==200) {
        
        for (int i = ([mutaro count]-1); i>=0; i--) {
            NSString *curString = [mutaro objectAtIndex:i];
            NSLog(@"%@",curString);
            if ([curString rangeOfString:@"rates"].location!=NSNotFound) {
                continue;
            }else if ([curString rangeOfString:@"base"].location!=NSNotFound) {
                
                int colonLoc = [curString rangeOfString:@":"].location;
                NSString *currencyName = [curString substringFromIndex:(colonLoc+1)];
                int apsLoc = [currencyName rangeOfString:@"\""].location;
                currencyName = [currencyName substringWithRange:NSMakeRange(apsLoc+1, 3)];
                NSLog(@"etCur = %@",currencyName);
                [converter setValue:currencyName forKey:@"etalonValue"];
                break;
                
            }else{
                int colonLoc = [curString rangeOfString:@":"].location;
                int apsLoc = [curString rangeOfString:@"\""].location;
                NSString *currencyName = [curString substringWithRange:NSMakeRange(apsLoc+1, 3)];
                double rate = [[curString substringFromIndex:(colonLoc+1)] doubleValue];
                if ([converter valueForKey:currencyName]){
                    NSMutableDictionary *item = [NSMutableDictionary dictionaryWithDictionary:[converter valueForKey:currencyName]];
                    [item setValue:[NSNumber numberWithDouble:rate] forKey:@"rate"];
                    [converter setValue:item forKey:currencyName];
                }
            }
        }
    }
    
}

- (void)setValuesWithLinearRules
{
    
    
    NSArray *conv_items = [converter valueForKey:@"Items"];
    int count = [conv_items count];
    
    NSArray *rules = [NSArray arrayWithArray:[converter valueForKey:@"rules"]];
    int edIndex = [conv_items indexOfObject:editedKey];//edited index
    
    int elemToUpdate[count];
    int updatedElem[count];
    for (int i = 0; i < count; i++) {
        elemToUpdate[i] = -1;
        updatedElem[i] = 0;
    }
    updatedElem[edIndex] = 1;
    
    
    int leftToUpdate = 0;
    
    for (NSDictionary *dict in rules) {
        if ([[dict valueForKey:@"type"] isEqualToString:@"linear"]) {
            NSString *ref = [dict valueForKey:@"refItem"];
            NSString *itm = [dict valueForKey:@"item"];
            
            int iIdx = [conv_items indexOfObject:itm];
            int rIdx = [conv_items indexOfObject:ref];
            
            if (elemToUpdate[iIdx]==-1){elemToUpdate[iIdx]=1;leftToUpdate++;}
            if (elemToUpdate[rIdx]==-1){elemToUpdate[rIdx]=1;leftToUpdate++;}
        }
        
    }
    elemToUpdate[edIndex] = -1;
    leftToUpdate--;
    int iteration = 0;
    int loops = 0;
    
    while (leftToUpdate > 0) {
        
        if (iteration>count) {iteration = 0; loops++;}
        
        
        
        for (NSDictionary *rule in rules)
        {
            if ([[rule valueForKey:@"type"] isEqualToString:@"linear"]) {
                NSString *ref = [rule valueForKey:@"refItem"];
                NSString *itm = [rule valueForKey:@"item"];
                
                int refIdx = [conv_items indexOfObject:ref];
                int itmIdx = [conv_items indexOfObject:itm];
                
                
                if ([[rule valueForKey:@"type"] isEqualToString:@"linear"]) {
                    if (refIdx==iteration && elemToUpdate[refIdx]==1) {
                        
                        if (elemToUpdate[itmIdx]==-1) {
                            
                            NSMutableDictionary *refDict = [NSMutableDictionary dictionaryWithDictionary:[converter valueForKey:ref]];
                            NSMutableDictionary *itmDict = [NSMutableDictionary dictionaryWithDictionary:[converter valueForKey:itm]];
                            
                            double val,slope,intercept;
                            slope = [[rule valueForKey:@"slope"] doubleValue];
                            intercept = [[rule valueForKey:@"intercept"] doubleValue];
                            val = ([[itmDict valueForKey:@"value"] doubleValue] - intercept ) / slope;  //4
                            [refDict setValue:[NSNumber numberWithDouble:val] forKey:@"value"];
                            
                            [converter setValue:refDict forKey:ref];
                            elemToUpdate[refIdx] = -1;
                            leftToUpdate--;
                        }
                        continue;
                        
                    }
                    
                    if (itmIdx==iteration && elemToUpdate[itmIdx]==1) {
                        
                        if (elemToUpdate[refIdx]==-1) {
                            
                            NSMutableDictionary *refDict = [NSMutableDictionary dictionaryWithDictionary:[converter valueForKey:ref]];
                            NSMutableDictionary *itmDict = [NSMutableDictionary dictionaryWithDictionary:[converter valueForKey:itm]];
                            
                            double val,slope,intercept;
                            slope = [[rule valueForKey:@"slope"] doubleValue];
                            intercept = [[rule valueForKey:@"intercept"] doubleValue];
                            val = [[refDict valueForKey:@"value"] doubleValue] * slope + intercept;  //4
                            [itmDict setValue:[NSNumber numberWithDouble:val] forKey:@"value"];
                            
                            [converter setValue:itmDict forKey:itm];
                            elemToUpdate[itmIdx] = -1;
                            leftToUpdate--;
                        }
                        continue;
                    }
                }
            }
            
            
            
        }
        iteration++;
    }
    
    
}

- (void)setValuesWithFormulaRules
{
    NSArray *rules = [converter valueForKey:@"rules"];
    FormulaParser *parser = [[FormulaParser alloc] init];
    [parser setConverter:converter];
    for (NSDictionary *rule in rules) {
        if ([[rule valueForKey:@"type"] isEqualToString:@"formula"]) {
            NSString *itm = [rule valueForKey:@"item"]; 
            if (![itm isEqualToString:@"?"]) {
                NSMutableDictionary *itmDict = [NSMutableDictionary dictionaryWithDictionary:[converter valueForKey:itm]];
                NSLog(@"%f",[[itmDict valueForKey:@"value"] doubleValue]);
                double newVal = [parser parse:[rule valueForKey:@"formula"]];
                [itmDict setValue:[NSNumber numberWithDouble:newVal] forKey:@"value"];
                [converter setValue:itmDict forKey:itm];
            }
        }
    }
}

- (void)updateValues
{
    
    //NSMutableDictionary *editedKeyDict = [converter valueForKey:editedKey]; 
    NSString *etalonKey = [converter valueForKey:@"etalonValue"];
    double etDictVal = [[[converter valueForKey:etalonKey] valueForKey:@"value"] doubleValue];
    NSArray *itemsArray = [converter valueForKey:@"Items"];
    if (etalonKey){
        
        for (int i = 0; i<[itemsArray count]; i++) {
            if ([[itemsArray objectAtIndex:i] isEqualToString:editedKey]) {
                continue;
            }
            if ([[itemsArray objectAtIndex:i] isEqualToString:etalonKey]) {
                continue;
            }
            NSMutableDictionary *dict = [converter valueForKey:[itemsArray objectAtIndex:i]];
            if ([dict valueForKey:@"rate"]) {
                double newValue = etDictVal * [[dict valueForKey:@"rate"] doubleValue];
                [dict setValue:[NSNumber numberWithDouble:newValue] forKey:@"value"];
                [converter setValue:dict forKey:[itemsArray objectAtIndex:i]];
            }
            
        }
    }
    if (linearRuleCount>0) {
        [self setValuesWithLinearRules];
    }
    if (formulaRuleCount>0) {
        [self setValuesWithFormulaRules];
    }
    
}

@end

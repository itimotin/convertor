//
//  CompoundCell.h
//  currency test
//
//  Created by Admin on 23.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol CompoundCellDelegate

- (void)selectedCompound:(NSUInteger)compNum rowLocation:(CGPoint)location;

@end

@interface CompoundCell : UITableViewCell
{
    id<CompoundCellDelegate>delegate;
    
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *descriptionLabel;
    
    IBOutlet UIImageView *accessoryBackground;
    
    UILabel *compOneValue;
    UILabel *compOneDesc;
    
    UILabel *compTwoValue;
    UILabel *compTwoDesc;
    
    UILabel *compThrValue;
    UILabel *compThrDesc;
    
    UILabel *compFouValue;
    UILabel *compFouDesc;
    
    float valueFontSize;
    float descFontSize;
    
    NSUInteger compoundsNumber;
    NSUInteger selectedCompoundNumber;
    
    UIImageView *selection;
    
    BOOL compsAdded;
    
    IBOutlet UIView *filterView;
}

@property (nonatomic, retain) UITableView *_ctableView;
@property (nonatomic, assign) id<CompoundCellDelegate>delegate;

- (void)addCompounds:(NSUInteger)compNum;

- (void)setNameLabelText:(NSString*)text;
- (void)setDescripionLabelText:(NSString*)text;

- (void)setCompound:(NSInteger)compNum Value:(NSString*)value;
- (void)setCompoundDescription:(NSString*)description value:(NSString*)value compNum:(NSInteger)compNum;
- (void)setCompound:(NSInteger)compNum Description:(NSString *)description;

- (void)selectCompound:(NSUInteger)compNum;

- (NSString*)getCompoundValue:(NSUInteger)compNum;

- (void)showFilterView:(BOOL)show;

@end

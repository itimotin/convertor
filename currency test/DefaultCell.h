//
//  DefaultCell.h
//  currency test
//
//  Created by Admin on 23.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DefaultCell : UITableViewCell
{
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UILabel *valLabel;
    IBOutlet UIImageView *accBack;
    IBOutlet UIView *filterView;
    CGSize prevSize;
    
}
- (NSString*)valLabelText;
- (void)setNameLabelText:(NSString*)text;
- (void)setDescLabelText:(NSString*)text;
- (void)setValueLabelText:(NSString*)text;

- (void)showFilterView:(BOOL)show;
@end

//
//  EditLinearRuleVC.m
//  currency test
//
//  Created by Admin on 14.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EditLinearRuleVC.h"
#import "SelectItemVC.h"

@implementation EditLinearRuleVC

@synthesize delegate;
@synthesize slopeValCell;
@synthesize interceptValCell;

- (void)dealloc
{
    [ruleDict release],ruleDict = nil;
    [itemName release],itemName = nil;
    [refItemName release];refItemName = nil;
    [itemsArray release],itemsArray = nil;
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self = [super initWithNibName:@"IPadEditLinearRuleVC" bundle:nibBundleOrNil];
        
    }
    else
    {
        self = [super initWithNibName:@"EditLinearRuleVC" bundle:nibBundleOrNil];
    }
    
    
    if (self) {
        // Custom initialization
        itemsArray = [[NSArray alloc] init];
        itemName = [[NSString alloc] initWithString:@""];
        refItemName = [[NSString alloc] initWithString:@""];
        ruleDict = [[NSMutableDictionary alloc] initWithCapacity:0];
        keyboardOnScreen = NO;
//        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
        self.view.backgroundColor = [UIColor yellowColor];
        
    }
    return self;
}

- (void)setItemsArray:(NSArray *)array
{
    if (itemsArray) {
        [itemsArray release],itemsArray=nil;
    }
    itemsArray = [[NSArray alloc] initWithArray:array];
    
}

- (void)setRuleDictionary:(NSDictionary *)dictionary
{
    if (ruleDict) {
        [ruleDict release],ruleDict = nil;
    }
    ruleDict = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
    if (itemName) {
        [itemName release],itemName = nil;
    }
    if (refItemName) {
        [refItemName release],refItemName = nil;
    }
    
    itemName = [[NSString alloc] initWithString:[ruleDict valueForKey:@"item"]];
    refItemName = [[NSString alloc] initWithString:[ruleDict valueForKey:@"refItem"]];
    
    
    
}

- (void)selectedItem:(NSUInteger)item
{
    if (itemORrefitem) {
        if (itemName) 
            [itemName release],itemName = nil;
        itemName = [[NSString alloc] initWithString:[itemsArray objectAtIndex:item]];
    }else{
        if (refItemName) 
            [refItemName release],refItemName = nil;
        refItemName = [[NSString alloc] initWithString:[itemsArray objectAtIndex:item]];
    }
    
    [editorTable reloadData];
}

- (void)goBack
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)save
{
    if (refItemName == nil || [refItemName isEqualToString:@""] || itemName == nil || [itemName isEqualToString:@""]) {
        UIView *errorAllertview = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 150)] autorelease];
        UILabel *errorAllertLabel = [[UILabel alloc] initWithFrame:errorAllertview.frame];
        errorAllertview.center= self.view.center;
        errorAllertLabel.textColor = [UIColor whiteColor];
        errorAllertLabel.textAlignment = UITextAlignmentCenter;
        errorAllertLabel.font = [UIFont systemFontOfSize:17];
        errorAllertLabel.numberOfLines = 5;
        errorAllertLabel.backgroundColor = [UIColor clearColor];
        [errorAllertview.layer setCornerRadius:15];
        errorAllertview.backgroundColor = [UIColor colorWithRed:1.0f green:0.3f blue:0.3f alpha:0.8f];
        errorAllertview.alpha = 0.0f;
        
        [errorAllertLabel setText:@"Error\nItem or reference item are not set"];
        [errorAllertview addSubview:errorAllertLabel];
        [self.view addSubview:errorAllertview];
        
        [UIView animateWithDuration:0.2f animations:^{
            errorAllertview.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:2.0f animations:^{
                errorAllertview.alpha = 0.0f;
            } completion:^(BOOL finished) {
                [errorAllertview removeFromSuperview];
                [errorAllertLabel release];
                [errorAllertview release];
            }];                
        }];
        
        return;
    }
    
    [ruleDict setValue:itemName forKey:@"item"];
    [ruleDict setValue:refItemName forKey:@"refItem"];
    [ruleDict setValue:@"linear" forKey:@"type"];
    
    if (_pSlope.text == NULL || [_pSlope.text isEqualToString:@""])
        [ruleDict setValue:[NSNumber numberWithDouble:1] forKey:@"slope"];
    else
        [ruleDict setValue:[NSNumber numberWithDouble:[_pSlope.text doubleValue]] forKey:@"slope"];
    
    if  (_pIntercept.text == NULL || [_pIntercept.text isEqualToString:@""])
        [ruleDict setValue:[NSNumber numberWithDouble:0] forKey:@"intercept"];
    else
        [ruleDict setValue:[NSNumber numberWithDouble:[_pIntercept.text doubleValue]] forKey:@"intercept"];
    
    [delegate saveLinearRule:ruleDict];
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    editorTable.backgroundColor = [UIColor clearColor];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.slopeValCell = nil;
    self.interceptValCell = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (keyboardOnScreen){
        CGRect frame = editorTable.frame;
        
        frame.size.height +=216.0f;
        
        [UIView animateWithDuration:0.3 animations:^{
            editorTable.frame = frame;
        }];
        keyboardOnScreen = NO;
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    static NSString *SlopeValCellID = @"_kSlopeCellID";
    static NSString *InterceptValCellID = @"_kInterceptCellID";
    
    UITableViewCell *cell;
    
    // Configure the cell...
    if  (indexPath.section == 0)
    {
        if (indexPath.row == 0){
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            cell.textLabel.text = itemName;
            cell.textLabel.font = [UIFont fontWithName:@"Olney-Light" size:16];
            cell.textLabel.textColor = [UIColor blackColor];
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            cell.textLabel.text = refItemName;
            cell.textLabel.font = [UIFont fontWithName:@"Olney-Light" size:16];
            cell.textLabel.textColor = [UIColor blackColor];
        }
    }else{
        if (indexPath.row==0) {
            cell = [tableView dequeueReusableCellWithIdentifier:SlopeValCellID];
            if (cell == nil) {
                cell = slopeValCell;
                
                for (UIView *view in [cell.contentView subviews]) {
                    if ([view isKindOfClass:[UITextField class]]) {
                        _pSlope = (UITextField*)view;
                    }
                }
            }
            //cell config
            _pSlope.text = [NSString stringWithFormat:@"%f",[[ruleDict valueForKey:@"slope"] doubleValue]];
            _pSlope.font = [UIFont fontWithName:@"Olney-Light" size:16];
            cell.textLabel.textColor = [UIColor blackColor];
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:InterceptValCellID];
            if (cell == nil) {
                cell = interceptValCell;
                for (UIView *view in [cell.contentView subviews]) {
                    if ([view isKindOfClass:[UITextField class]]) {
                        _pIntercept = (UITextField*)view;
                    }
                }
                
            }
            //cell config
            _pIntercept.text = [NSString stringWithFormat:@"%f",[[ruleDict valueForKey:@"intercept"] doubleValue]];
            _pIntercept.font = [UIFont fontWithName:@"Olney-Light" size:16];
            cell.textLabel.textColor = [UIColor blackColor];
        }
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section==1)
        return @" ";
        return @" ";

        
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    
//    NSString *sectionTitle;
//    if (section == 1) {
//        sectionTitle = @"Constants";
//    }
//    else {
//        sectionTitle = @"";
//    }
//    
//    // Create label with section title
//    UILabel *label = [[[UILabel alloc] init] autorelease];
//    label.frame = CGRectMake(0, 0, 320,25);
//    label.textColor = [UIColor whiteColor];
//    label.font = [UIFont fontWithName:@"Olney-Light" size:20];
//    label.text = sectionTitle;
//    label.backgroundColor = [UIColor blackColor];
//    
//    // Create header view and add label as a subview
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
//    [view autorelease];
//    [view addSubview:label];
//    
//    return view;
//}



//- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
//{
//    if (section==1) 
//        return @"A and B are constants in the equation\n[Item] = a * [RefItem] +b\nFor example: [\u00B0F] = 1.8* [\u00B0C] + 32";
//    return @"" ;
//}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    NSString *sectionFooterTitle;
    if (section == 1) {
        sectionFooterTitle = @"A and B are constants in the equation\n[Item] = a * [RefItem] +b\nFor example: [\u00B0F] = 1.8* [\u00B0C] + 32";
    }
    else {
        sectionFooterTitle = @"";
    }
    UILabel *label = [[[UILabel alloc] init] autorelease];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {

           label.frame = CGRectMake(0, -1, 768, 50); 
    }
    else
    {
        label.frame = CGRectMake(0, -1, 320, 50);
    }

    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:@"Olney-Light" size:18];
    label.text = sectionFooterTitle;
    label.numberOfLines  = 5;
    label.textAlignment = UITextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    UIView *sectionFooterView;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
        sectionFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 30, 768, 100)];
    }
    else
    {
        sectionFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 30, 320, 100)];
    }
    [sectionFooterView autorelease];
    [sectionFooterView addSubview:label];
    
    return sectionFooterView;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        return nil;
    }
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section==0 && [itemsArray count]>0) {
        if (indexPath.row == 0){
            itemORrefitem = YES;
            SelectItemVC *selectItemVC = [[[SelectItemVC alloc] initWithNibName:@"SelectItemVC" bundle:nil] autorelease];
            [selectItemVC setDelegate:(id)self];
            [selectItemVC setItemsArray:itemsArray];
            selectItemVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentModalViewController:selectItemVC animated:YES];
        }else{
            itemORrefitem = NO;
            SelectItemVC *selectItemVC = [[[SelectItemVC alloc] initWithNibName:@"SelectItemVC" bundle:nil] autorelease];
            [selectItemVC setDelegate:(id)self];
            [selectItemVC setItemsArray:itemsArray];
            selectItemVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentModalViewController:selectItemVC animated:YES];
        }
            
    }
}


#pragma mark - TextField delegate


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if ([textField.placeholder isEqualToString:@"multiplier"]) {
        _pSlope = textField;
    }else{
        _pIntercept = textField;
    }
    
    if (!keyboardOnScreen) {
        CGRect frame = editorTable.frame;
        
        frame.size.height -=216.0f;
        
        [UIView animateWithDuration:0.3 animations:^{
            editorTable.frame = frame;
        } completion:^(BOOL finished) {
            [editorTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            
        }];
        keyboardOnScreen = YES;
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (keyboardOnScreen){
        CGRect frame = editorTable.frame;
        
        frame.size.height +=216.0f;
        
        [UIView animateWithDuration:0.3 animations:^{
            editorTable.frame = frame;
        }];
        keyboardOnScreen = NO;
    }
    
    [textField resignFirstResponder];
    return YES;
}

@end

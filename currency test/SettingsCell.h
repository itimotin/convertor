//
//  SettingsCell.h
//  currency test
//
//  Created by Admin on 23.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell
{
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UIImageView *checkMark;
}
- (void)setNameLabelText:(NSString*)text;
- (void)setDescripionLabelText:(NSString*)text;
- (void)setChecked:(BOOL)checked;
@end

//
//  ConverterViewController.h
//  currency test
//
//  Created by Admin on 22.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
enum opType
{
    opType_plus,
    opType_minus,
    opType_multipl,
    opType_divide,
    opType_noOperation
    
};

@class CompoundCell,DefaultCell,SettingsCell,Converter;

@interface ConverterViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
{
    IBOutlet UITableView *converterTable;
    CompoundCell *compoundCell;
    DefaultCell *defaultCell;
    SettingsCell *settingsCell;
    
    IBOutlet UILabel *converterTitleLabel;
    int convNumber;
    
    NSUInteger selectedCompound;
    NSIndexPath *ccellIndexPath;
    
    IBOutlet UIView *keyboard;
    UIButton *arrowbtn;
    BOOL showsMenu;
    UILabel *footerLabel;
    
    IBOutlet UIButton *editBtn;
    
    NSMutableDictionary *converterDictionary;
    
    BOOL isAlreadyFavorites;
    
    NSMutableString *valueInputString;
    
    Converter *converter;
    //for keyboard =-/*
    double prevVal;
    double memoredVal;//M+
    BOOL operation;
    enum opType op_type;
    //info View
    IBOutlet UIView *infoView;
    IBOutlet UIButton *editTemplateButton;
    IBOutlet UILabel *info_TypeLabel;
    IBOutlet UILabel *info_TotalItems;
    IBOutlet UILabel *info_Showing;
    //headear view buttons
    UIButton *addToFavButton;
    UIButton *sendByEmail;
    UIButton *moreInfo;
    UIButton *selectAll;
    UIButton *unselectAll;
}

@property (nonatomic, retain) IBOutlet CompoundCell *compoundCell;
@property (nonatomic, retain) IBOutlet DefaultCell *defaultCell;
@property (nonatomic, retain) IBOutlet SettingsCell *settingsCell;

- (void)setConverter:(NSUInteger)convNum favoritesFile:(BOOL)currentFileFavorites;
- (IBAction)goBack;
- (IBAction)goEdit;
- (IBAction)editTemplate;
- (IBAction)closeInfo;
- (IBAction)keyboardButtonPressed:(UIButton*)sender;


@end

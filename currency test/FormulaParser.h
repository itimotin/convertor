//
//  FormulaParser.h
//  NewEmpty
//
//  Created by Admin on 28.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FormulaParser : NSObject
{
    int charIndex;
    double tokenValue;
    NSString *inputString;
}

@property (nonatomic, retain) NSDictionary *converter;

- (double)parse:(NSString*)string;

@end

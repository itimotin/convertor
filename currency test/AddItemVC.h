//
//  AddItemVC.h
//  currency test
//
//  Created by Admin on 15.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol AddItemVCDelegate

- (void)saveItem:(NSDictionary*)item WithKeyName:(NSString*)name;

@end

@interface AddItemVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
{
    id <AddItemVCDelegate> delegate;
    NSMutableDictionary *itemDictionary;
    
    NSString *_aitemName;
    
    char itemType;
    IBOutlet UITableView *addItemTableView;
    BOOL keyboardOnScreen;
    char currentTf;
    IBOutlet UITableViewCell *nameCell;
    IBOutlet UITableViewCell *descCell;
    IBOutlet UITableViewCell *valCell;
    IBOutlet UITableViewCell *compCell;
    IBOutlet UITableViewCell *deciCell;
    
    
    IBOutlet UILabel *titleLabel;
//    IBOutlet UITextField *nameTF;
//    IBOutlet UITextField *descTF;
//    IBOutlet UITextField *valTF;
//    IBOutlet UITextField *deciTF;
    
    
    IBOutlet UIButton *back;
    IBOutlet UIButton *save;
    
    NSIndexPath *_ccindexPath;
    UITextField *tf;//comp tf_pointer
    UITextField *ntf;//name tf_pointer
    UITextField *dtf;//description tf_pointer
    UITextField *vtf;//value tf_pointer
    
}

@property (nonatomic, assign) id <AddItemVCDelegate> delegate;

@property (nonatomic, retain) IBOutlet UITableViewCell *nameCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *descCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *valCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *compCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *deciCell;




- (void)setItem:(NSString*)itemName Dictionary:(NSDictionary*)dictionary;
- (IBAction)goBack;
- (IBAction)save;


@end

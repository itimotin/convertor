//
//  AppDelegate.m
//  currency test
//
//  Created by Admin on 01.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"
#import "IPadViewController.h"
@interface AppDelegate ()
{
    NSMutableArray *mutStr;
    NSUInteger lineIndex;
    NSMutableArray *convSeparatorIndexes;
    NSString *separatorLine;
    NSMutableDictionary *mutDict;
}
@end
@implementation AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *filePathALL = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"All.plist"];
    NSString *filePathFAV = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"Favorites.plist"];
    
    if (![filemgr fileExistsAtPath:filePathALL]) {
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"All" ofType:@"plist"]];
        [dict writeToFile:filePathALL atomically:YES];
    }
    if (![filemgr fileExistsAtPath:filePathFAV]) {
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Favorites" ofType:@"plist"]];
        [dict writeToFile:filePathFAV atomically:YES];
    }
    
    [filemgr release];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
//    self.viewController = [[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] autorelease];
    ViewController *controllerInstance;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        controllerInstance = [[ViewController alloc] initWithNibName:@"IPadViewController" bundle:nil];
       
    }
    else
    {
        controllerInstance = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
        
    }
    self.viewController = controllerInstance;
    [controllerInstance release];
   // NSLog(@"AppDelegate - iPhone/iPodTouch");
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end

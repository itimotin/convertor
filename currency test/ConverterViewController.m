//
//  ConverterViewController.m
//  currency test
//
//  Created by Admin on 22.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ConverterViewController.h"
#import "ConverterEditorVC.h"
#import "CompoundCell.h"
#import "DefaultCell.h"
#import "Converter.h"
#import "SettingsCell.h"


#define k_DefaultCellType 0
#define k_SettingsCellType 1
#define k_CompoundCellType 2



@implementation ConverterViewController

@synthesize compoundCell,defaultCell,settingsCell;

- (void)convUpdated//reload converter after transmutations
{
    [converterTable reloadData];
}

- (void)CEDidFinishEditing:(NSDictionary*)tempDict
{
    if (converter)
    {
        [converter release],converter = nil;
    }
    converter = [[Converter alloc] initWithDict:tempDict];
    if (converterDictionary)
        [converterDictionary release],converterDictionary = nil;
    converterDictionary = [[NSMutableDictionary alloc] initWithDictionary:tempDict];
    [converterTable reloadData];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self = [super initWithNibName:@"IPadConverterViewController" bundle:nibBundleOrNil];
        
    }
    else
    {
        self = [super initWithNibName:@"ConverterViewController" bundle:nibBundleOrNil];
    }
    if (self) {
        // Custom initialization
        selectedCompound = 0;
        valueInputString = [[NSMutableString alloc] initWithCapacity:0];
        prevVal = INFINITY;
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)arowBtnPressed
{
    if (!showsMenu) {
        showsMenu = YES;
        [converterTable setContentOffset:CGPointMake(0, 0)];
    }else{
        showsMenu = NO;
        [converterTable setContentOffset:CGPointMake(0, 42)];
    }
}

- (void)goEdit
{
    converterTable.editing ? [converterTable setEditing:NO]:[converterTable setEditing:YES];
    if (converterTable.editing){
        if ([keyboard isDescendantOfView:self.view]){
            CGPoint point = keyboard.center;
            point.y +=200;
            CGRect frame = converterTable.frame;
            frame.size.height += keyboard.frame.size.height;
            [UIView animateWithDuration:0.25f animations:^{
                converterTable.frame = frame;
                keyboard.center = point;
            } completion:^(BOOL finished) {
                [keyboard removeFromSuperview];
                CGPoint newPoint = point;
                newPoint.y -=200;
                keyboard.center = newPoint;
                converterTable.frame = frame;
                [converterTable scrollToRowAtIndexPath:ccellIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
                if (!showsMenu && converterTable.contentOffset.y < 42) {
                    converterTable.contentOffset = CGPointMake(0, 42);
                }
            }];
        }
        [addToFavButton removeFromSuperview];
        [sendByEmail removeFromSuperview];
        [moreInfo removeFromSuperview];
        [converterTable.tableHeaderView addSubview:selectAll];
        [converterTable.tableHeaderView addSubview:unselectAll];
    }else{
        
        [converterTable.tableHeaderView addSubview:addToFavButton];
        [converterTable.tableHeaderView addSubview:sendByEmail];
        [converterTable.tableHeaderView addSubview:moreInfo];
        [selectAll removeFromSuperview];
        [unselectAll removeFromSuperview];
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){}else{}
    (converterTable.editing==NO)?[editBtn setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal]:[editBtn setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [converterTable reloadData];
}

- (void)closeInfo
{
    [infoView removeFromSuperview];
}

- (void)editTemplate
{
    ConverterEditorVC *converterEditor = [[[ConverterEditorVC alloc] initWithNibName:@"ConverterEditorVC" bundle:nil] autorelease];
    converterEditor.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [converterEditor setTemplateDictionary:converterDictionary];
    [converterEditor setConvNum:convNumber favorites:isAlreadyFavorites];
    converterEditor.delegate = (id)self;
    [self presentModalViewController:converterEditor animated:YES];
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error :(NSError*)error {
    
    [self dismissModalViewControllerAnimated:YES];
    
}

- (void)headerButtonPressed:(UIButton*)sender
{
    switch (sender.tag) {
        case 0:
            if (!isAlreadyFavorites) {
                NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *favPath = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"Favorites.plist"];
                NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:favPath];
//                NSArray *arr = [NSArray arrayWithArray:[[dict allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
//                    return [obj1 compare:obj2 options:NSNumericSearch];
//                }]];
                BOOL add = YES;
                for (NSString *s in [dict allKeys]) {
                    NSString *name = [[dict valueForKey:s] valueForKey:@"Name"];
                    if ([name isEqualToString:[converterDictionary valueForKey:@"Name"]]) {
                        add = NO;
                    }
                }
                
                
                
                if (add) {
                    [dict setValue:converterDictionary forKey:[NSString stringWithFormat:@"Converter %d",[[dict allKeys] count]]];
                    [dict writeToFile:favPath atomically:YES];
                }
            }
            break;
        case 1:
            //send email
            if ([MFMailComposeViewController canSendMail]) {
                CGRect frame = converterTable.frame;
                CGSize size = frame.size;
                frame.size = converterTable.contentSize;
                converterTable.frame = frame;
                MFMailComposeViewController *mailComposeVC = [[[MFMailComposeViewController alloc] init] autorelease];
                mailComposeVC.mailComposeDelegate = (id)self;
                [mailComposeVC setSubject:[converterDictionary valueForKey:@"Name"]];
                [mailComposeVC setMessageBody:@"Converter App calculations" isHTML:NO];
                
                UIGraphicsBeginImageContext(converterTable.frame.size);
                CGContextRef ctx = UIGraphicsGetCurrentContext();
//                CGContextTranslateCTM(ctx, 0, converterTable.frame.size.height);
//                
//                CGContextScaleCTM(ctx, 1.0f, -1.0f);
                [converterTable.layer renderInContext:ctx];
                
                UIImage *contentImg = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                
                [mailComposeVC addAttachmentData:UIImagePNGRepresentation(contentImg) mimeType:@"image/png" fileName:@"conversion_results.png"];
                [self presentModalViewController:mailComposeVC animated:YES];
                frame.size = size;
                converterTable.frame = frame;
                //UIImage *contentImage = []
                
            }
            break;
        case 2:
            [self.view addSubview:infoView];
            infoView.layer.cornerRadius = 5;
            [info_TypeLabel setText:[@"Type:" stringByAppendingString:[converterDictionary valueForKey:@"Type"]]];
            [info_TotalItems setText:[NSString stringWithFormat:@"Total Items: %d items",[[converterDictionary valueForKey:@"Items"] count]]];
            [info_TotalItems setFont:[UIFont fontWithName:@"Olney-Light" size:14]];
            [info_Showing setText:[NSString stringWithFormat:@"Showing: %d items",[converter showingItems]]];
            [info_Showing setFont:[UIFont fontWithName:@"Olney-Light" size:14]];
            break;
            
        case 3:
        {
            int count = [[converterDictionary valueForKey:@"Items"] count];
            
            for (int i = 0; i < count; i++) {
                BOOL hidden = [[[converter allItems_itemForRow:i] valueForKey:@"checked"] boolValue];
                if (!hidden) 
                    [converter hideItemForRow:i];
            }
            [converterTable reloadData];
        }
            break;
        case 4:
        {
            int count = [[converterDictionary valueForKey:@"Items"] count];
            
            for (int i = 0; i < count; i++) {
                BOOL hidden = [[[converter allItems_itemForRow:i] valueForKey:@"checked"] boolValue];
                if (hidden) 
                    [converter hideItemForRow:i];
            }
            [converterTable reloadData];
        }
            break;
        default:
            break;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect frame = keyboard.frame;
    frame.origin = CGPointMake(0, self.view.frame.size.height-frame.size.height);
    keyboard.frame = frame;
    NSString *path =[[NSBundle mainBundle] pathForResource:@"List" ofType:@"plist"];
    
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    if (dict==nil) {
        NSLog(@"nil");
    }
    
    
 
    infoView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back_calc.png"]];
    infoView.center = self.view.center;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
    converterTable.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
    
    
    
    UIView *tbHeader;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){ 
        tbHeader= [[UIView alloc] initWithFrame:CGRectMake(0, 0, converterTable.frame.size.width, 75)];
    }else{ 
        tbHeader= [[UIView alloc] initWithFrame:CGRectMake(0, 0, converterTable.frame.size.width, 42)];
    }
   
    tbHeader.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
    
    //header buttons


    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            addToFavButton = [[UIButton alloc] initWithFrame:CGRectMake(1, 0, 255, 75)];
        [addToFavButton setImage:[UIImage imageNamed:@"ip_add_to_fav_.png"] forState:UIControlStateNormal];
        [addToFavButton setImage:[UIImage imageNamed:@"ip_add_to_fav.png"] forState:UIControlStateHighlighted];
        [addToFavButton setImage:[UIImage imageNamed:@"ip_add_to_fav.png"] forState:UIControlStateSelected];
            [addToFavButton setTag:0];
            [addToFavButton addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }else{
            addToFavButton = [[UIButton alloc] initWithFrame:CGRectMake(1, 0, 106, 42)];
        [addToFavButton setImage:[UIImage imageNamed:@"add_to_fav_.png"] forState:UIControlStateNormal];
        [addToFavButton setImage:[UIImage imageNamed:@"add_to_fav.png"] forState:UIControlStateHighlighted];
        [addToFavButton setImage:[UIImage imageNamed:@"add_to_fav.png"] forState:UIControlStateSelected];
            [addToFavButton setTag:0];
        
            [addToFavButton addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }


   
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){  
         sendByEmail = [[UIButton alloc] initWithFrame:CGRectMake(256, 0, 255, 75)];
        [sendByEmail setImage:[UIImage imageNamed:@"ip_send_email_.png"] forState:UIControlStateNormal];
        [sendByEmail setImage:[UIImage imageNamed:@"ip_send_email.png"] forState:UIControlStateHighlighted];
        [sendByEmail setImage:[UIImage imageNamed:@"ip_send_email.png"] forState:UIControlStateSelected];
        [sendByEmail setTag:1];
            [sendByEmail addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }else{ 
         sendByEmail = [[UIButton alloc] initWithFrame:CGRectMake(107, 0, 106, 42)];
        [sendByEmail setImage:[UIImage imageNamed:@"send_email_.png"] forState:UIControlStateNormal];
        [sendByEmail setImage:[UIImage imageNamed:@"send_email.png"] forState:UIControlStateHighlighted];
        [sendByEmail setImage:[UIImage imageNamed:@"send_email.png"] forState:UIControlStateSelected];
       
        [sendByEmail setTag:1];
            [sendByEmail addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        }



    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        moreInfo = [[UIButton alloc] initWithFrame:CGRectMake(511, 0, 255, 75)];

        [moreInfo setImage:[UIImage imageNamed:@"ip_more_info_.png"] forState:UIControlStateNormal];
        [moreInfo setImage:[UIImage imageNamed:@"ip_more_info.png"] forState:UIControlStateHighlighted];
        [moreInfo setImage:[UIImage imageNamed:@"ip_more_info.png"] forState:UIControlStateSelected];
         [moreInfo setTag:2];
            [moreInfo addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        moreInfo = [[UIButton alloc] initWithFrame:CGRectMake(213, 0, 106, 42)];
        [moreInfo setImage:[UIImage imageNamed:@"more_info_.png"] forState:UIControlStateNormal];
        [moreInfo setImage:[UIImage imageNamed:@"more_info.png"] forState:UIControlStateHighlighted];
        [moreInfo setImage:[UIImage imageNamed:@"more_info.png"] forState:UIControlStateSelected];
         [moreInfo setTag:2];
            [moreInfo addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }


   
    
   
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
         selectAll = [[UIButton alloc] initWithFrame:CGRectMake(1, 0, 383, 75)];
        [selectAll setImage:[UIImage imageNamed:@"ip_select_all_.png"] forState:UIControlStateNormal];
        [selectAll setImage:[UIImage imageNamed:@"ip_select_all.png"] forState:UIControlStateHighlighted];
        [selectAll setTag:3];
         [selectAll addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }else{
         selectAll = [[UIButton alloc] initWithFrame:CGRectMake(1, 0, 159, 42)];
        [selectAll setImage:[UIImage imageNamed:@"select_all_.png"] forState:UIControlStateNormal];
        [selectAll setImage:[UIImage imageNamed:@"select_all.png"] forState:UIControlStateHighlighted];
        [selectAll setTag:3];
         [selectAll addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }



    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            unselectAll = [[UIButton alloc] initWithFrame:CGRectMake(384, 0, 383, 75)];
        [unselectAll setImage:[UIImage imageNamed:@"ip_unselect_all_.png"] forState:UIControlStateNormal];
        [unselectAll setImage:[UIImage imageNamed:@"ip_unselect_all.png"] forState:UIControlStateHighlighted];
        [unselectAll setTag:4];
        [unselectAll addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }else{
            unselectAll = [[UIButton alloc] initWithFrame:CGRectMake(160, 0, 159, 42)];
        [unselectAll setImage:[UIImage imageNamed:@"unselect_all_.png"] forState:UIControlStateNormal];
        [unselectAll setImage:[UIImage imageNamed:@"unselect_all.png"] forState:UIControlStateHighlighted];
        [unselectAll setTag:4];
        [unselectAll addTarget:self action:@selector(headerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }

    
    [tbHeader addSubview:addToFavButton];
    [tbHeader addSubview:sendByEmail];
    [tbHeader addSubview:moreInfo];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        arrowbtn = [[UIButton alloc] initWithFrame:CGRectMake(tbHeader.center.x-25, tbHeader.frame.size.height-25, 50, 50)];
    }else {
            arrowbtn = [[UIButton alloc] initWithFrame:CGRectMake(tbHeader.center.x-14, tbHeader.frame.size.height-14, 30, 30)];
            [arrowbtn setImage:[UIImage imageNamed:@"arrow_orange.png"] forState:UIControlStateNormal];
    }
      
    [arrowbtn addTarget:self action:@selector(arowBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [tbHeader addSubview:arrowbtn];
    [converterTable setTableHeaderView:tbHeader];
    
    [converterTable setContentOffset:CGPointMake(0, 22)];
    showsMenu = NO;
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, converterTable.frame.size.width, 40)];
    view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];

    footerLabel  = [[UILabel alloc] initWithFrame:CGRectMake(view.center.x-42, view.center.y -15, 160, 30)];
    
    
    footerLabel.textAlignment = UITextAlignmentCenter;
    footerLabel.textColor = [UIColor colorWithRed:0.32f green:0.32f blue:0.32f alpha:1.0f];
    footerLabel.shadowColor = [UIColor whiteColor];
    footerLabel.shadowOffset = CGSizeMake(-1, -1);
    footerLabel.backgroundColor = [UIColor clearColor];
    
    [view addSubview:footerLabel];
    [converterTable setTableFooterView:view];
    [view release];
    
    operation = NO;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    if (ccellIndexPath!=nil) {[ccellIndexPath release],ccellIndexPath=nil;}
    [footerLabel release],footerLabel = nil;
    [addToFavButton release];
    [sendByEmail release];
    [moreInfo release];
}

- (void)viewWillAppear:(BOOL)animated
{
    if ([converter showingItems]==0) {
        converterTable.editing = YES;
    }
    
    
    [converterTitleLabel setText:[converterDictionary valueForKey:@"Name"]];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [converterTitleLabel setFont:[UIFont fontWithName:@"Olney-Light" size:20]];
    }else{
        [converterTitleLabel setFont:[UIFont fontWithName:@"Olney-Light" size:15]];
    }
    
//    [converterTitleLabel setTextColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"gr.png"]]];
    
    if (!showsMenu) {
        [converterTable setContentOffset:CGPointMake(0, 42)];
    }else{
        [converterTable setContentOffset:CGPointMake(0, 0)];
    }
    [converterTable reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)goBack
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSMutableDictionary *mDict = [NSMutableDictionary dictionaryWithDictionary:[converter getConverterDictionary]];
    if (isAlreadyFavorites) {
        NSString *path = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"Favorites.plist"];
        NSMutableDictionary *convsDict = [NSMutableDictionary dictionaryWithContentsOfFile:path];
        [convsDict setValue:mDict forKey:[NSString stringWithFormat:@"Converter %d",convNumber]];
        [convsDict writeToFile:path atomically:YES];
    }else{
        NSString *path = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"All.plist"];
        NSMutableDictionary *convsDict = [NSMutableDictionary dictionaryWithContentsOfFile:path];
        [convsDict setValue:mDict forKey:[NSString stringWithFormat:@"Converter %d",convNumber]];
        [convsDict writeToFile:path atomically:YES];
    }
    [self dismissModalViewControllerAnimated:YES];
}

- (void)setConverter:(NSUInteger)convNum favoritesFile:(BOOL)currentFileFavorites
{
    
    convNumber = convNum;
    if (converter!=nil) {
        [converter release],converter = nil;
    }
    if (converterDictionary != nil) {
        [converterDictionary release],converterDictionary = nil;
    }
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    if (currentFileFavorites) {
        isAlreadyFavorites = YES;
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:[[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"Favorites.plist"]];
        converterDictionary = [[NSMutableDictionary alloc] initWithDictionary:[dict valueForKey:[NSString stringWithFormat:@"Converter %d",convNum]]];
        converter = [[Converter alloc] initWithDict:converterDictionary];
        converter.delegate = (id)self;
        converter.isFav = YES;
        [dict release];
                               
    }else{
        isAlreadyFavorites = NO;
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:[[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"All.plist"]];
        converterDictionary = [[NSMutableDictionary alloc] initWithDictionary:[dict valueForKey:[NSString stringWithFormat:@"Converter %d",convNum]]];
        converter = [[Converter alloc] initWithDict:converterDictionary];
        converter.isFav = NO;
        [dict release];
    }
    
    
}

#pragma mark - TableView Delegate/Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (converterTable.editing) {
        footerLabel.text = @"Check items to show";
        footerLabel.textAlignment = UITextAlignmentLeft;
        return [[converterDictionary valueForKey:@"Items"] count];
        
    }
    footerLabel.text = [NSString stringWithFormat:@"Showing %d",[converter showingItems]];
    footerLabel.textAlignment = UITextAlignmentLeft;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [footerLabel setFont:[UIFont fontWithName:@"Olney-Light" size:20]];
    }else{
        [footerLabel setFont:[UIFont fontWithName:@"Olney-Light" size:14]];
    }
    return [converter showingItems];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *tbcellID = @"UITBCELL";
    static NSString *compCellID = @"_kCompoundCellID";
    static NSString *defCellID = @"_kDefaultCellID";
    static NSString *setCellID = @"_kSettingsCell";
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [[NSBundle mainBundle] loadNibNamed:@"IPadCells" owner:self options:nil];
        
    }
    else
    {
        [[NSBundle mainBundle] loadNibNamed:@"Cells" owner:self options:nil];
    }

        
    if (tableView.editing) {
        
        SettingsCell *cell = (SettingsCell*)[tableView dequeueReusableCellWithIdentifier:setCellID];
        if (cell==nil) {
            cell = settingsCell;
            self.settingsCell = nil;
            
            cell.showsReorderControl = YES;
            
    
        }
        //cell Update
        NSDictionary *item = [converter allItems_itemForRow:indexPath.row];
        [cell setNameLabelText:[converter allItems_itemNameForRow:indexPath.row]];
        [cell setDescripionLabelText:[item valueForKey:@"description"]];
        [cell setChecked:[[item valueForKey:@"checked"] boolValue]];
        return cell;
        
    }else{
        NSDictionary *item = [converter itemForRow:indexPath.row];
        
        if ([[item valueForKey:@"type"] isEqualToString:@"compound"]) {
            CompoundCell *cell = (CompoundCell*)[tableView dequeueReusableCellWithIdentifier:compCellID];
            if (cell==nil) {
                
                cell = compoundCell;
                self.compoundCell = nil;
                cell._ctableView = converterTable;
                [cell setDelegate:(id)self];
                
            }
            //cell Update
            //for calculator cells
            if ([item valueForKey:@"resItem"]) {
                [cell showFilterView:YES];
            }else{
                [cell showFilterView:NO];
            }
            [cell setNameLabelText:[converter itemNameForRow:indexPath.row]];
            [cell setDescripionLabelText:[item valueForKey:@"description"]];
            if (indexPath.row!=ccellIndexPath.row || ccellIndexPath==nil) {
                
                int compNum = [[item valueForKey:@"compNum"] intValue];
                [cell addCompounds:compNum];
                NSString *compFormula = [item valueForKey:@"compound"];
                double val = [[item valueForKey:@"value"] doubleValue];
                int tok = [compFormula length];
                int maxIndx = [compFormula length] - 1;
                for (int i = maxIndx; i>=0; i--) {
                    int loc=0,len=0;
                    if ([compFormula characterAtIndex:i]=='#') {
                        
                        if (compNum == 1) {
                            [cell setCompound:compNum Value:[NSString stringWithFormat:@"%.0f",val]];
                            
                        }
                        
                        loc = i+1;
                        len = tok - loc;
                        if (loc >= tok) 
                            [cell setCompound:compNum Description:@""];
                        else
                            [cell setCompound:compNum Description:[compFormula substringWithRange:NSMakeRange(loc, len)]];
                        
                        tok = i;
                    }
                    if ([compFormula characterAtIndex:i]==' ') {
                        loc = i+1;
                        len = tok - loc;
                        if (loc >= tok) 
                            [cell setCompound:compNum Value:@""];
                        else
                        {
                            double compVal = [[compFormula substringWithRange:NSMakeRange(loc, len)] doubleValue];
                            double valToSet = fmod(val, compVal);
                            
                            if (compNum < [[item valueForKey:@"compNum"] intValue]) {
                                [cell setCompound:compNum Value:[NSString stringWithFormat:@"%.0f",valToSet]];
                            }else{
                                NSMutableString *vts = [NSMutableString stringWithFormat:@"%f",valToSet];
                                for (int i = ([vts length] - 1); i>=0; i--) {
                                    if ([vts characterAtIndex:i]=='0')
                                    {
                                        
                                        [vts deleteCharactersInRange:NSMakeRange(i, 1)];
                                        continue;
                                    }else{
                                        if ([vts characterAtIndex:i]=='.'){
                                            [vts deleteCharactersInRange:NSMakeRange(i, 1)];
                                            break;
                                        }
                                        break;
                                    }
                                }
                                [cell setCompound:compNum Value:vts];
                            }
                            //val = (val/compVal);
                            double frpart = modf((val/compVal), &val); 
                            frpart=0;//get rid of warning
                            tok = i;
                        }
                        compNum--;
                    }
                }
            }
            [cell selectCompound:0];
            if (ccellIndexPath.row == indexPath.row && ![item valueForKey:@"resItem"])
                [cell selectCompound:selectedCompound];
            
            return cell;

        }else if([[item valueForKey:@"type"] isEqualToString:@"decimal"]){
            
            
            DefaultCell *cell = (DefaultCell*) [tableView dequeueReusableCellWithIdentifier:defCellID];
            if (cell == nil) {
                cell = defaultCell;
                self.defaultCell = nil;
                
            }
            //cell Update
            //for calculator cells
            if ([item valueForKey:@"resItem"]) {
                [cell showFilterView:YES];
            }else{
                [cell showFilterView:NO];
            }
            [cell setNameLabelText:[converter itemNameForRow:indexPath.row]];
            [cell setDescLabelText:[item valueForKey:@"description"]];
            [cell setValueLabelText:[converter valueForRow:indexPath.row]];
            return cell;
        }
    }
    
    return nil;
}



- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Hide";
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  YES;
}


 // Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [converter moveItemFrom:fromIndexPath.row to:toIndexPath.row];
}



 // Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
 // Return NO if you do not want the item to be re-orderable.
 return YES;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!converterTable.editing)
    {
        if ([[converter itemForRow:indexPath.row] valueForKey:@"resItem"]){
            return nil;
        }
    }
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!converterTable.editing && indexPath!=nil) {
        [converterTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        if ([[tableView cellForRowAtIndexPath:ccellIndexPath] isKindOfClass:[CompoundCell class]]){
            [((CompoundCell*)[tableView cellForRowAtIndexPath:ccellIndexPath]) selectCompound:0];
        }
        ccellIndexPath = nil;
        ccellIndexPath = [indexPath retain];
        
        if (![keyboard isDescendantOfView:self.view]) {
            
            
            CGPoint newPoint = keyboard.center;
            newPoint.y += 200;
            keyboard.center = newPoint;
            [self.view addSubview:keyboard];
            CGRect frame = converterTable.frame;
            frame.size.height -= keyboard.frame.size.height;
            [UIView animateWithDuration:0.2f animations:^{
                CGPoint newPoint = keyboard.center;
                newPoint.y -= 200;
                
                keyboard.center= newPoint;
                
            } completion:^(BOOL finished) {
                converterTable.frame = frame;
                [converterTable scrollToRowAtIndexPath:ccellIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }];
            
        }
        
        
        [valueInputString setString:@""];
        
        
        if (!showsMenu && converterTable.contentOffset.y < 42) {
            [converterTable setContentOffset:CGPointMake(0, 42)];
        }
    }else if (tableView.editing){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [converter hideItemForRow:indexPath.row];
        [converterTable reloadData];
        
    }
}
#pragma mark - TableView Scrolling
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    
    if (scrollView.contentOffset.y<42.f) {
        [UIView animateWithDuration:0.2 animations:^{
            arrowbtn.transform = CGAffineTransformMakeRotation(M_PI);
        }];
    }
    
    if (showsMenu && scrollView.contentOffset.y < -23) {
        scrollView.scrollEnabled = NO;
        [scrollView setContentOffset:CGPointMake(0, 42) animated:YES];
        scrollView.scrollEnabled = YES;
        showsMenu = NO;
        return;
    }
    
    if (scrollView.contentOffset.y>=42) {
        
        [UIView animateWithDuration:0.2 animations:^{
            arrowbtn.transform = CGAffineTransformMakeRotation(0);
        }];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y < 42 && scrollView.contentOffset.y > 30) {
        [scrollView setContentOffset:CGPointMake(0, 42) animated:YES];
        
    }else if (scrollView.contentOffset.y < 30){
        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        showsMenu = YES;
    }else if(scrollView.contentOffset.y > 42){
        [UIView animateWithDuration:0.2 animations:^{
            arrowbtn.transform = CGAffineTransformMakeRotation(0);
        }];
    }
    
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(CGPoint *)targetContentOffset
{
    if (!showsMenu && targetContentOffset->y < 42 && scrollView.contentOffset.y >42){
        targetContentOffset->y = 42;
    }
}





#pragma mark - CompoundCellDelegate

- (void)selectedCompound:(NSUInteger)compNum rowLocation:(CGPoint)location
{
    //deselect previos compcell
    if ([[converterTable cellForRowAtIndexPath:ccellIndexPath] isKindOfClass:[CompoundCell class]]){
        [((CompoundCell*)[converterTable cellForRowAtIndexPath:ccellIndexPath]) selectCompound:0];
    }
    ccellIndexPath = nil;
    
    ccellIndexPath = [[converterTable indexPathForRowAtPoint:location] retain];
    if  ([[converter itemForRow:ccellIndexPath.row] valueForKey:@"resItem"]){
        return;
    }
    selectedCompound = compNum;
    
    [valueInputString setString:[((CompoundCell*)[converterTable cellForRowAtIndexPath:ccellIndexPath]) getCompoundValue:selectedCompound]];
    [converterTable reloadData];
    
    if (![keyboard isDescendantOfView:self.view]) {
        
        
        CGPoint newPoint = keyboard.center;
        newPoint.y += 200;
        keyboard.center = newPoint;
        [self.view addSubview:keyboard];
        CGRect frame = converterTable.frame;
        frame.size.height -= keyboard.frame.size.height;
        [UIView animateWithDuration:0.2f animations:^{
            CGPoint newPoint = keyboard.center;
            newPoint.y -= 200;
            
            keyboard.center= newPoint;
            
        } completion:^(BOOL finished) {
            converterTable.frame = frame;
            [converterTable scrollToRowAtIndexPath:ccellIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }];
        
    }
}


#pragma mark - Keyboard Actions

- (void)keyboardButtonPressed:(UIButton *)sender
{
    
    
    if ([sender.titleLabel.text isEqualToString:@"HideCalc"]) {
        CGPoint point = keyboard.center;
        point.y +=200;
        CGRect frame = converterTable.frame;
        frame.size.height += keyboard.frame.size.height;
        [UIView animateWithDuration:0.25f animations:^{
            converterTable.frame = frame;
            keyboard.center = point;
        } completion:^(BOOL finished) {
            [keyboard removeFromSuperview];
            CGPoint newPoint = point;
            newPoint.y -=200;
            keyboard.center = newPoint;
            converterTable.frame = frame;
            [converterTable scrollToRowAtIndexPath:ccellIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
            if (!showsMenu && converterTable.contentOffset.y < 42) {
                converterTable.contentOffset = CGPointMake(0, 42);
            }
        }];
        return;
    }
    if (sender.tag==11) {//del
        if ([valueInputString length]==1){
            [valueInputString setString:@"0"];
        }else if ([valueInputString characterAtIndex:([valueInputString length]-2)]=='.') {
            [valueInputString deleteCharactersInRange:NSMakeRange(([valueInputString length]-2),2)];
        }else if ([valueInputString isEqualToString:@""]){
            [valueInputString setString:@"0"];
        }else{
            [valueInputString deleteCharactersInRange:NSMakeRange(([valueInputString length]-1), 1)];
        }
    }
    
    if ([valueInputString length]>10) {
//        return;
    }
    
    if (sender.tag == 15) {// +
        prevVal = [valueInputString doubleValue];
        operation = YES;
        op_type = opType_plus;
        return ;
    }
    
    if (sender.tag == 14) {// -
        prevVal = [valueInputString doubleValue];
        operation = YES;
        op_type = opType_minus;
        return;
    }
    
    if (sender.tag == 13) {// *
        prevVal = [valueInputString doubleValue];
        operation = YES;
        op_type = opType_multipl;
        return;
    }
    
    if (sender.tag == 12) {// /
        prevVal = [valueInputString doubleValue];
        operation = YES;
        op_type = opType_divide;
        return;
    }
    
    if (sender.tag == 17) {// 1/x
        double newValue = 1 / [valueInputString doubleValue];
        [valueInputString setString:[NSString stringWithFormat:@"%.10f",newValue]]; 
        for (int i = ([valueInputString length] - 1); i>=0; i--) {
            if ([valueInputString characterAtIndex:i]=='.') {
                [valueInputString deleteCharactersInRange:NSMakeRange(i, 1)];
                break;
            }
            if ([valueInputString characterAtIndex:i]=='0') {
                [valueInputString deleteCharactersInRange:NSMakeRange(i, 1)];
                continue;
            }else{
                break;
            }
        }
    }
    
    if (sender.tag == 18) {// +/-
        double newValue = -[valueInputString doubleValue];
        [valueInputString setString:[NSString stringWithFormat:@"%.10f",newValue]]; 
        for (int i = ([valueInputString length] - 1); i>=0; i--) {
            if ([valueInputString characterAtIndex:i]=='.') {
                [valueInputString deleteCharactersInRange:NSMakeRange(i, 1)];
                break;
            }
            if ([valueInputString characterAtIndex:i]=='0') {
                [valueInputString deleteCharactersInRange:NSMakeRange(i, 1)];
                continue;
            }else{
                break;
            }
        }
    }
    
    if (sender.tag == 19) {// C
        [valueInputString setString:@"0"];
    }
    
    if (sender.tag == 20) {// MC
        memoredVal = INFINITY;
    }
    
    if (sender.tag == 21) {// MR
        if (memoredVal==INFINITY) 
            return;
        else 
            [valueInputString setString:[NSString stringWithFormat:@"%.10f",memoredVal]];
        
        for (int i = ([valueInputString length] - 1); i>=0; i--) {
            if ([valueInputString characterAtIndex:i]=='.') {
                [valueInputString deleteCharactersInRange:NSMakeRange(i, 1)];
                break;
            }
            if ([valueInputString characterAtIndex:i]=='0') {
                [valueInputString deleteCharactersInRange:NSMakeRange(i, 1)];
                continue;
            }else{
                break;
            }
        }
    }
    
    if (sender.tag == 22) {// M+
        memoredVal = [valueInputString doubleValue];
    }
    
    if (sender.tag == 16) {// =
        
        if (prevVal==INFINITY) {
            [converter setNewValue:valueInputString forKey:[converter itemNameForRow:ccellIndexPath.row]];
            [converterTable reloadData];
            return;
        }
        converter.removeZeros = YES; 
        switch (op_type) {
                double newValue;
            case opType_plus:
                newValue = prevVal + [valueInputString doubleValue];
                [valueInputString setString:[NSString stringWithFormat:@"%.10f",newValue]];
                break;
            case opType_minus:
                newValue = prevVal - [valueInputString doubleValue];
                [valueInputString setString:[NSString stringWithFormat:@"%.10f",newValue]];
                break;
            case opType_divide:
                newValue = prevVal / [valueInputString doubleValue];
                [valueInputString setString:[NSString stringWithFormat:@"%.10f",newValue]];
                break;
            case opType_multipl:
                newValue = prevVal * [valueInputString doubleValue];
                [valueInputString setString:[NSString stringWithFormat:@"%.10f",newValue]];
                break;
            default:
                break;
        }
        for (int i = ([valueInputString length] - 1); i>=0; i--) {
            if ([valueInputString characterAtIndex:i]=='.') {
                [valueInputString deleteCharactersInRange:NSMakeRange(i, 1)];
                break;
            }
            if ([valueInputString characterAtIndex:i]=='0') {
                [valueInputString deleteCharactersInRange:NSMakeRange(i, 1)];
                continue;
            }else{
                break;
            }
        }
        
    }

    
    //Number digits
    if (sender.tag<10) {
        
        if ([valueInputString isEqualToString:@"0"] || operation) {
            operation = NO;
            [valueInputString setString:[NSString stringWithFormat:@"%d",sender.tag]];
        }else{
            converter.removeZeros = NO;
            if (([valueInputString length] < 11) && (sender.tag < 11)) {
                [valueInputString appendString:[NSString stringWithFormat:@"%d",sender.tag]];
            }else{
                if (([valueInputString length] > 10) && (sender.tag > 10))
               {
                   [valueInputString appendString:[NSString stringWithFormat:@"%d",sender.tag]]; 
               }
            }
        }
        
        
    }
    if (sender.tag==10) {
        [valueInputString appendString:@"."];
        
        return;
    }
    
    //other buttons
    /*...*/
    //update converter
    if ([[converterTable cellForRowAtIndexPath:ccellIndexPath] isKindOfClass:[CompoundCell class]]) {
        [(CompoundCell*)([converterTable cellForRowAtIndexPath:ccellIndexPath]) setCompound:selectedCompound Value:valueInputString];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[converter itemForRow:ccellIndexPath.row]];
        NSString *compFormula = [dict valueForKey:@"compound"];
        int compNum = [[dict valueForKey:@"compNum"] intValue];
        switch (compNum) {
            case 1:
                
                //[dict setValue:[NSNumber numberWithDouble:[valueInputString doubleValue]] forKey:@"value"];
                [converter setNewValue:valueInputString forKey:[converter itemNameForRow:ccellIndexPath.row]];
                break;
            case 2:
            {    
                double firstCmpVal = [[(CompoundCell*)([converterTable cellForRowAtIndexPath:ccellIndexPath]) getCompoundValue:1] doubleValue];
                double secondCmpVal = [[(CompoundCell*)([converterTable cellForRowAtIndexPath:ccellIndexPath]) getCompoundValue:2] doubleValue];
                int loc = [compFormula rangeOfString:@" "].location+1;
                int len = [[compFormula substringFromIndex:loc] rangeOfString:@"#"].location;
                double compMultipl = [[compFormula substringWithRange:NSMakeRange(loc, len)] doubleValue];
                double val =0;
                if (selectedCompound==1) {
                    val = compMultipl * [valueInputString doubleValue] + secondCmpVal;
                }else{
                    val = firstCmpVal * compMultipl+ [valueInputString doubleValue]; 
                }
                [converter setNewValue:[NSString stringWithFormat:@"%.10f",val] forKey:[converter itemNameForRow:ccellIndexPath.row]];

            }   
                break;
            case 3:
            {
                double firstCmpVal = [[(CompoundCell*)([converterTable cellForRowAtIndexPath:ccellIndexPath]) getCompoundValue:1] doubleValue];
                double secondCmpVal = [[(CompoundCell*)([converterTable cellForRowAtIndexPath:ccellIndexPath]) getCompoundValue:2] doubleValue];
                double thirdCmpVal = [[(CompoundCell*)([converterTable cellForRowAtIndexPath:ccellIndexPath]) getCompoundValue:3] doubleValue]; 
                
                double thirdCompMult=1,secondCompMult=1;
                int loc = [compFormula rangeOfString:@" "].location+1;
                int len = [[compFormula substringFromIndex:loc] rangeOfString:@"#"].location;
                secondCompMult = [[compFormula substringWithRange:NSMakeRange(loc, len)] doubleValue];
                loc += [[compFormula substringFromIndex:loc] rangeOfString:@" "].location+1;
                len = [[compFormula substringFromIndex:loc] rangeOfString:@"#"].location;
                thirdCompMult = [[compFormula substringWithRange:NSMakeRange(loc, len)] doubleValue];
                double val = 0;
                if (selectedCompound==3) 
                {
                    val = firstCmpVal*secondCompMult*thirdCompMult + secondCmpVal*thirdCompMult + [valueInputString doubleValue];
                }
                if (selectedCompound==2)
                {
                    val = firstCmpVal*secondCompMult*thirdCompMult + [valueInputString doubleValue]*thirdCompMult + thirdCmpVal;
                }
                if (selectedCompound==1)
                {
                    val = [valueInputString doubleValue]*secondCompMult*thirdCompMult + secondCmpVal*thirdCompMult + thirdCmpVal;
                }
                    
                [converter setNewValue:[NSString stringWithFormat:@"%.10f",val] forKey:[converter itemNameForRow:ccellIndexPath.row]];    

            }
                break;
            case 4:
            {
                double firstCmpVal = [[(CompoundCell*)([converterTable cellForRowAtIndexPath:ccellIndexPath]) getCompoundValue:1] doubleValue];
                double secondCmpVal = [[(CompoundCell*)([converterTable cellForRowAtIndexPath:ccellIndexPath]) getCompoundValue:2] doubleValue];
                double thirdCmpVal = [[(CompoundCell*)([converterTable cellForRowAtIndexPath:ccellIndexPath]) getCompoundValue:3] doubleValue];
                double fourthCmpVal = [[(CompoundCell*)([converterTable cellForRowAtIndexPath:ccellIndexPath]) getCompoundValue:4] doubleValue];
                
                double secondCompMult=1,thirdCompMult=1,fourthCompMult=1;
                
                
                int loc = [compFormula rangeOfString:@" "].location+1;
                int len = [[compFormula substringFromIndex:loc] rangeOfString:@"#"].location;
                secondCompMult = [[compFormula substringWithRange:NSMakeRange(loc, len)] doubleValue];
                loc += [[compFormula substringFromIndex:loc] rangeOfString:@" "].location+1;
                len = [[compFormula substringFromIndex:loc] rangeOfString:@"#"].location;
                thirdCompMult = [[compFormula substringWithRange:NSMakeRange(loc, len)] doubleValue];
                loc += [[compFormula substringFromIndex:loc] rangeOfString:@" "].location+1;
                len = [[compFormula substringFromIndex:loc] rangeOfString:@"#"].location;
                fourthCompMult = [[compFormula substringWithRange:NSMakeRange(loc, len)] doubleValue];
                double val = 0;
                if (selectedCompound == 4){
                    val = firstCmpVal*secondCompMult*thirdCompMult*fourthCompMult + secondCmpVal*thirdCompMult*fourthCompMult + thirdCmpVal*fourthCompMult + [valueInputString doubleValue];
                }else if (selectedCompound == 3){
                    val = firstCmpVal*secondCompMult*thirdCompMult*fourthCompMult + secondCmpVal*thirdCompMult*fourthCompMult + [valueInputString doubleValue]*fourthCompMult + fourthCmpVal;
                }else if (selectedCompound == 2){
                    val = firstCmpVal*secondCompMult*thirdCompMult*fourthCompMult + [valueInputString doubleValue]*thirdCompMult*fourthCompMult + thirdCmpVal*fourthCompMult + fourthCmpVal;
                }else if (selectedCompound == 1){
                    val = [valueInputString doubleValue]*secondCompMult*thirdCompMult*fourthCompMult + secondCmpVal*thirdCompMult*fourthCompMult + thirdCmpVal*fourthCompMult + fourthCmpVal;
                }
                [converter setNewValue:[NSString stringWithFormat:@"%.10f",val] forKey:[converter itemNameForRow:ccellIndexPath.row]];


            }
                break;
                
                //[converter setValue:dict forKey:[converter itemNameForRow:ccellIndexPath.row]];
        }
    }else{
        [converter setNewValue:valueInputString forKey:[converter itemNameForRow:ccellIndexPath.row]];
    }
    if ([valueInputString length]==10) {
        [valueInputString deleteCharactersInRange:NSMakeRange(9, [valueInputString length]-10)];
        NSLog(@"ura");
    }
    if (([valueInputString length] > 10) && (sender.tag>11)) {
        [valueInputString deleteCharactersInRange:NSMakeRange(9, [valueInputString length]-10)];
        NSLog(@"lala");
    }
    [converterTable reloadData];
}


@end

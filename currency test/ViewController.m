//
//  ViewController.m
//  currency test
//
//  Created by Admin on 01.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "ConverterViewController.h"
#import "ConverterEditorVC.h"



@interface ViewController()
{
    
}
@end

@implementation ViewController




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self=[super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)arowBtnPressed
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    addTemp.font =  [UIFont fontWithName:@"Olney-Light" size:25];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [allConverters setImage:[UIImage imageNamed:@"ip_all_.png"] forState:UIControlStateNormal];
        searchTextField.font = [UIFont fontWithName:@"Olney-Light" size:20];
    }else {
         [allConverters setImage:[UIImage imageNamed:@"all_.png"] forState:UIControlStateNormal]; 
        searchTextField.font = [UIFont fontWithName:@"Olney-Light" size:14];
    }
   
    
    
    [searchView removeFromSuperview];
    sectionNumber = 1;
    searching = NO;
    favConverters.selected = YES;
    converterViewController = [[ConverterViewController alloc] initWithNibName:@"ConverterViewController" bundle:nil];
    [converterViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    convertersDict = [[NSMutableDictionary alloc] initWithContentsOfFile:[[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"Favorites.plist"]];
    currentFileFavorites = YES;
    tb.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tb.frame.size.width, 40)];
    view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
    footerLabel  = [[UILabel alloc] initWithFrame:CGRectMake(view.center.x-80, view.center.y -15, 160, 30)];
    footerLabel.textAlignment = UITextAlignmentCenter;
    footerLabel.textColor = [UIColor colorWithRed:0.32f green:0.32f blue:0.32f alpha:1.0f];
    footerLabel.shadowColor = [UIColor whiteColor];
    footerLabel.shadowOffset = CGSizeMake(-1, 1);
    footerLabel.backgroundColor = [UIColor clearColor];
    footerLabel.text = @"Footer View";
    [footerLabel setText:[NSString stringWithFormat:@"Showing %d items",[[convertersDict allKeys] count]]];
    [footerLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
    [view addSubview:footerLabel];
    [tb setTableFooterView:view];
    [view release];
    sectionOneHeader.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
    addTemp.textColor = [UIColor colorWithRed:0.32f green:0.32f blue:0.32f alpha:1.0f];
    previousWidth = 0;
    
    converterEditor.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    converterEditor = [[ConverterEditorVC alloc] initWithNibName:@"ConverterEditorVC" bundle:nil];
    currentFileFavorites = YES;
  ////////////////////////////////
//	SVSegmentedControl *navSC = [[SVSegmentedControl alloc] initWithSectionTitles:[NSArray arrayWithObjects:@"ALL", @"Favorites", nil]];
//    navSC.changeHandler = ^(NSUInteger newIndex) {
//        NSLog(@"segmentedControl did select index %i (via block handler)", newIndex);
//    };
//    navSC.font = [UIFont fontWithName:@"Olney-Light" size:25];
////    navSC.thumb.tintColor = [UIColor colorWithRed:255/255.0f green:89/255.0f blue:0.0 alpha:1.000];
//    navSC.thumb.tintColor = [UIColor orangeColor];
//    navSC.thumb.textColor = [UIColor whiteColor];
//    navSC.thumb.textShadowColor = [UIColor colorWithWhite:1 alpha:0.5];
//    navSC.thumb.textShadowOffset = CGSizeMake(0, 1);
//	[self.view addSubview:navSC];
//    navSC.frame = CGRectMake(23, 63, 204, 33);
//	
//	navSC.center = CGPointMake(92, 78);
//    navSC.tag = 1;
}


- (void)segmentedControlChangedValue:(SVSegmentedControl*)segmentedControl {
	NSLog(@"segmentedControl %i did select index %i (via UIControl method)", segmentedControl.tag, segmentedControl.selectedIndex);
}

- (void)viewDidUnload
{

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [footerLabel release],footerLabel = nil;
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //[self updateRates];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sectionNumber;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSUInteger number ;
    if (section == 0) {
        
        [footerLabel setText:[NSString stringWithFormat:@"Showing %d items",[[convertersDict allKeys] count]]];
        [footerLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
        number = [[convertersDict allKeys] count];
        
        if (searching) {
            number = [searchResults count];
        }
    }
    if (section == 1) {
        number = 2;
    }
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellID = @"_kCellID";
    static NSString *cellSOne = @"_kCellSONE";
    
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:cellSOne];
    }
    if (cell==nil) {
        
        
        
        
        
        
        if (indexPath.section == 1) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellSOne];
            UIImageView *view;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"table_cell_back.png"]];
            }else{
                view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"table_cell_back.png"]];
            }
            
            cell.backgroundView = view;
            [view release],view = nil;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
             cell.textLabel.font = [UIFont boldSystemFontOfSize:21];
            }else{
             cell.textLabel.font = [UIFont boldSystemFontOfSize:15];
            }
           
            cell.textLabel.backgroundColor = [UIColor clearColor];
            
            switch (indexPath.row) {
                case 0:
                    cell.textLabel.text = @"Design new converter";
                    [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:17]];
                    break;
                case 1:
                    cell.textLabel.text = @"Design new calculator";
                    [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:17]];
                    break;
            }
        }else if (indexPath.section == 0) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellID];
            UIImageView *view;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"table_cell_back.png"]];
            }else{
                view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"table_cell_back.png"]];
            }
            
            cell.backgroundView = view;
            [view release],view = nil;
            cell.textLabel.font = [UIFont fontWithName:@"Olney-Light" size:20];
            cell.textLabel.textColor = [UIColor whiteColor];
            
            cell.textLabel.backgroundColor = [UIColor clearColor];
            UIImageView *selView;
            UIImageView *accView;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
//                [allConverters setImage:[UIImage imageNamed:@"ip_all_.png"] forState:UIControlStateNormal];
                selView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"select_table_cel.png"]];
                 accView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]];
            }else{
                [allConverters setImage:[UIImage imageNamed:@"all_.png"] forState:UIControlStateNormal];
                selView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"select_table_cel.png"]];
                accView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]];
            }
            
            cell.selectedBackgroundView = selView;
            [selView release],selView = nil;
           
            CGRect frame = accView.frame;
            frame.size = CGSizeMake(40, 45);
            accView.frame = frame;
            cell.accessoryView = accView;
            [accView release],accView = nil;
            UIImageView *edaccView;
         
            edaccView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cor.png"]];
            


            frame = edaccView.frame;
            frame.size = CGSizeMake(53, 45);
            edaccView.frame = frame;
            cell.editingAccessoryView = edaccView;
            [edaccView release],edaccView = nil;
            
            
            cell.showsReorderControl = YES;
        }
        
        
    }
    if (indexPath.section == 1) {
        cell.accessoryView.hidden = YES;
        cell.editingAccessoryView.hidden = YES;
        cell.showsReorderControl = NO;
        switch (indexPath.row) {
            case 0:
                
                cell.textLabel.text = @"Design new converter";
                break;
            case 1:
                cell.textLabel.text = @"Design new calculator";
                break;
        }
        
        return cell;
    }
    
    
    
    if (searching) {
        cell.textLabel.text = [searchResults objectAtIndex:indexPath.row];
        return cell;
    }
    
    
    
    NSString *converterName = [[convertersDict valueForKey:[NSString stringWithFormat:@"Converter %d",indexPath.row]] valueForKey:@"Name"];
    
    cell.textLabel.text = converterName;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 0;
    if (section == 1) {
        height = 40;
    }
    return height;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
    if (section == 1) {
        return sectionOneHeader;
    }
    return view;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCellEditingStyle style = UITableViewCellEditingStyleDelete;
    
    if (indexPath.section == 1) {
        style = UITableViewCellEditingStyleInsert;
    }
    
    return style;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
#pragma mark - tableView: didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (keyboardOnScreen) {
        CGRect frame = tb.frame;
        frame.size.height += 216.0f;
        [UIView animateWithDuration:0.3f animations:^{
            tb.frame = frame;
            keyboardOnScreen = NO;
        }];
    }
    int index;
    if (searching)
    {
        for (int i = 0; i<[[convertersDict allKeys] count];i++ ) {
            if ([[[convertersDict valueForKey:[NSString stringWithFormat:@"Converter %d",i]]valueForKey:@"Name"] isEqualToString:[searchResults objectAtIndex:indexPath.row]]) {
                index = i;
                break;
            }
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView.editing)
    {
        if (indexPath.section == 1) {//add converter/calculator
            
            if (indexPath.row == 0) {//new converter
                ConverterEditorVC *convEdit = [[[ConverterEditorVC alloc] initWithNibName:@"ConverterEditorVC" bundle:nil] autorelease];
                [convEdit setConverterType:0];
                [self presentModalViewController:convEdit animated:YES];
            }else{//new calculator
                ConverterEditorVC *convEdit = [[[ConverterEditorVC alloc] initWithNibName:@"ConverterEditorVC" bundle:nil] autorelease];
                [convEdit setConverterType:1];
                [self presentModalViewController:converterEditor animated:YES];
            }
            
        }else{
            ConverterEditorVC *convEdit = [[[ConverterEditorVC alloc] initWithNibName:@"ConverterEditorVC" bundle:nil] autorelease];
            [convEdit setTemplateDictionary:[convertersDict valueForKey:[NSString stringWithFormat:@"Converter %d",indexPath.row]]];
            //[convEdit setDelegate:(id)self];
            if (currentFileFavorites)
                [convEdit setConvNum:indexPath.row favorites:YES];
            else
                [convEdit setConvNum:indexPath.row favorites:NO];
            
            [self presentModalViewController:convEdit animated:YES];
        }
    }else{
        ConverterViewController *cVC = [[[ConverterViewController alloc] initWithNibName:@"ConverterViewController" bundle:nil] autorelease];
        if (searching){
            [cVC setConverter:index favoritesFile:currentFileFavorites];
        }else{
            [cVC setConverter:indexPath.row favoritesFile:currentFileFavorites];
        }
        //[converterViewController setConverter:indexPath.row favoritesFile:currentFileFavorites];
        [self presentModalViewController:cVC animated:YES];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) return NO;
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSDictionary *dictToMove = [NSDictionary dictionaryWithDictionary:[convertersDict valueForKey:[NSString stringWithFormat:@"Converter %d",sourceIndexPath.row]]];
    if (sourceIndexPath.row<destinationIndexPath.row) {
        for (int i = sourceIndexPath.row; i <= destinationIndexPath.row; i++) {
            NSDictionary *dict = [NSDictionary dictionaryWithDictionary:[convertersDict valueForKey:[NSString stringWithFormat:@"Converter %d",i+1]]];
            [convertersDict setValue:dict forKey:[NSString stringWithFormat:@"Converter %d",i]];
        }
    } else {
        for (int i = sourceIndexPath.row; i > destinationIndexPath.row; i--) {
            NSDictionary *dict = [NSDictionary dictionaryWithDictionary:[convertersDict valueForKey:[NSString stringWithFormat:@"Converter %d",i-1]]];
            [convertersDict setValue:dict forKey:[NSString stringWithFormat:@"Converter %d",i]];
        }
    }
    [convertersDict setValue:dictToMove forKey:[NSString stringWithFormat:@"Converter %d",destinationIndexPath.row]];
    
    if (currentFileFavorites) {
        [convertersDict writeToFile:[[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"Favorites.plist"] atomically:YES];
    } else {
        [convertersDict writeToFile:[[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"All.plist"] atomically:YES];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
       
        [convertersDict removeObjectForKey:[NSString stringWithFormat:@"Converter %d",indexPath.row]];
        int count = [[convertersDict allKeys] count];
        for (int i = indexPath.row; i < count; i++) {
            
            NSDictionary *dict = [[NSDictionary alloc] initWithDictionary:[convertersDict valueForKey:[NSString stringWithFormat:@"Converter %d",i+1]]];
            [convertersDict removeObjectForKey:[NSString stringWithFormat:@"Converter %d",i+1]];
            NSString *str = [NSString stringWithFormat:@"Converter %d",i];
            
            [convertersDict setValue:dict forKey:str];
            [dict release];
        }
        
        
        if (currentFileFavorites) {
            [convertersDict writeToFile:[[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"Favorites.plist"] atomically:YES];
        } else {
            [convertersDict writeToFile:[[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"All.plist"] atomically:YES];
        }
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Button Actions

- (IBAction)buttonActions:(UIButton*)sender
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *pathALL = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"All.plist"];
    NSString *pathFAV = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"Favorites.plist"];
    
    switch (sender.tag) {
        case 0://search
            
            sectionNumber = 1;
            if (!searching) {
         
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    [search setFrame:CGRectMake(642, 12, 106, 104)];
                    [search setImage:[UIImage imageNamed:@"ip_search_.png"] forState:UIControlStateNormal];
                    
                }
                else
                {
                    [search setFrame:CGRectMake(259, 8, 51, 46)];
                    [search setImage:[UIImage imageNamed:@"search_high.png"] forState:UIControlStateNormal];
                }
                
              
                
                searching = YES;
                tb.editing = NO;
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){   (tb.editing==NO)?[edit setImage:[UIImage imageNamed:@"ip_edit.png"] forState:UIControlStateNormal]:[edit setImage:[UIImage imageNamed:@"ip_done.png"] forState:UIControlStateNormal];
                    [edit removeFromSuperview];
                    [tb reloadData];
                }else{ 
                        (tb.editing==NO)?[edit setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal]:[edit setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
                        [edit removeFromSuperview];
                        [tb reloadData];
                }
             
                if ([searchResults count]==0) {
                    [footerLabel setText:[NSString stringWithFormat:@"Found %d items",[searchResults count]]];
                }else{
                    [footerLabel setText:[NSString stringWithFormat:@"Found %d items",[searchResults count]]];
                }
                
                [allConverters removeFromSuperview];
                [favConverters removeFromSuperview];
                [topBar addSubview:searchView];
            }else{
                
         
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    [search setFrame:CGRectMake(642, 12, 106, 74)];
                    [search setImage:[UIImage imageNamed:@"ip_search.png"] forState:UIControlStateNormal];
                }
                else
                {
                    [search setFrame:CGRectMake(259, 8, 51, 36)];
                     [search setImage:[UIImage imageNamed:@"search_icon.png"] forState:UIControlStateNormal];
                }
      
               
                [topBar addSubview:edit];
                searching = NO;
                if (keyboardOnScreen) {
                    CGRect frame = tb.frame;
                    frame.size.height += 216.0f;
                    [UIView animateWithDuration:0.3f animations:^{
                        tb.frame = frame;
                        keyboardOnScreen = NO;
                    }];
                }
                [tb reloadData];
                
                [searchView removeFromSuperview];
                [topBar addSubview:allConverters];
                [topBar addSubview:favConverters];
            }
            break;
        case 1://all
            //sectionNumber = 1;
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                [allConverters setImage:[UIImage imageNamed:@"ip_all.png"] forState:UIControlStateNormal];   
                [favConverters setImage:[UIImage imageNamed:@"ip_favourites_.png"] forState:UIControlStateNormal];
            }else {
                [allConverters setImage:[UIImage imageNamed:@"all.png"] forState:UIControlStateNormal];   
                [favConverters setImage:[UIImage imageNamed:@"favourites_.png"] forState:UIControlStateNormal];
            }
           
            
            if (!allConverters.selected) {
                if (convertersDict!=nil) 
                    [convertersDict release],convertersDict = nil;
                currentFileFavorites = NO;
                favConverters.selected = NO;
                
                allConverters.selected = YES;
                
                convertersDict = [[NSMutableDictionary alloc] initWithContentsOfFile:pathALL];
                
                [tb reloadData];
                
            }
            
            break;
        case 2://favorites
            //sectionNumber = 1;
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                [allConverters setImage:[UIImage imageNamed:@"ip_all_.png"] forState:UIControlStateNormal];
                [favConverters setImage:[UIImage imageNamed:@"ip_favorites.png"] forState:UIControlStateNormal];
            }else{
                [allConverters setImage:[UIImage imageNamed:@"all_.png"] forState:UIControlStateNormal];
                [favConverters setImage:[UIImage imageNamed:@"favorites.png"] forState:UIControlStateNormal];
            }
            
            if (!favConverters.selected) {
                if (convertersDict!=nil) 
                    [convertersDict release],convertersDict = nil;
                currentFileFavorites = YES;
                favConverters.selected = YES;
                
                allConverters.selected = NO;
                
                convertersDict = [[NSMutableDictionary alloc] initWithContentsOfFile:pathFAV];
                [tb reloadData];
                
            }
            break;
        case 3://edit
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                sectionNumber = (tb.editing==NO)?2:1;//update section number
                (tb.editing==NO)?[tb setEditing:YES animated:YES]:[tb setEditing:NO animated:YES];
                (tb.editing==NO)?[edit setImage:[UIImage imageNamed:@"ip_edit.png"] forState:UIControlStateNormal]:[edit setImage:[UIImage imageNamed:@"ip_done.png"] forState:UIControlStateNormal];
                [tb reloadData];
            }else{
            sectionNumber = (tb.editing==NO)?2:1;//update section number
            (tb.editing==NO)?[tb setEditing:YES animated:YES]:[tb setEditing:NO animated:YES];
            (tb.editing==NO)?[edit setImage:[UIImage imageNamed:@"edit.png"] forState:UIControlStateNormal]:[edit setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
                [tb reloadData];}
            break;
        default:
            break;
    }
}

#pragma mark - TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect frame = tb.frame;
    frame.size.height -= 216.0f;
    [UIView animateWithDuration:0.3f animations:^{
        tb.frame = frame;
        keyboardOnScreen = YES;
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    CGRect frame = tb.frame;
    frame.size.height += 216.0f;
    [UIView animateWithDuration:0.3f animations:^{
        tb.frame = frame;
        keyboardOnScreen = NO;
    }];
//    if (searchResults!=nil) 
//        [searchResults release],searchResults=nil;
//    searchResults = [[NSMutableArray alloc] initWithCapacity:1];
//    for (int i = 0; i < [[convertersDict allKeys] count]; i++) {
//        NSDictionary *dict = [convertersDict valueForKey:[NSString stringWithFormat:@"Converter %d",i]];
//        if ([[[dict valueForKey:@"Name"] lowercaseString] rangeOfString:[searchTextField.text lowercaseString]].location!=NSNotFound) {
//            [searchResults addObject:[dict valueForKey:@"Name"]];
//        }
//    }
//    
//    [tb reloadData];
//    [footerLabel setText:[NSString stringWithFormat:@"Found %d items",[searchResults count]]];
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
        
    if (searchResults!=nil) 
        [searchResults release],searchResults=nil;
    searchResults = [[NSMutableArray alloc] initWithCapacity:1];
    for (int i = 0; i < [[convertersDict allKeys] count]; i++) {
        NSDictionary *dict = [convertersDict valueForKey:[NSString stringWithFormat:@"Converter %d",i]];
        if (range.length == 1) {
            
            if (searchTextField.text.length>2) {
                if ([[[dict valueForKey:@"Name"] lowercaseString] rangeOfString:[[searchTextField.text substringWithRange:NSMakeRange(0, searchTextField.text.length-1)] lowercaseString]].location!=NSNotFound) {
                    [searchResults addObject:[dict valueForKey:@"Name"]];
                }
            }else{
                if ([[[dict valueForKey:@"Name"] lowercaseString] rangeOfString:[[searchTextField.text substringWithRange:NSMakeRange(0, 1)] lowercaseString]].location!=NSNotFound) {
                    [searchResults addObject:[dict valueForKey:@"Name"]];
                }
            }
        }else{
            if ([[[dict valueForKey:@"Name"] lowercaseString] rangeOfString:[[searchTextField.text stringByAppendingString:string] lowercaseString]].location!=NSNotFound) {
                [searchResults addObject:[dict valueForKey:@"Name"]];
            }
        }
    }
    NSLog(@"%d",[searchResults count]);
    
    [tb reloadData];
    [footerLabel setText:[NSString stringWithFormat:@"Found %d items",[searchResults count]]];
    return YES;
}

- (void)dealloc {
    [super dealloc];
}
@end

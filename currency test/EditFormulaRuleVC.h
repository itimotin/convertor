//
//  EditFormulaRuleVC.h
//  currency test
//
//  Created by Admin on 14.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@protocol EditFormulaRuleVCDelegate

- (void)saveFormulaRule:(NSDictionary*)ruleDict;

@end

@class SelectItemVC;

@interface EditFormulaRuleVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>
{
    id<EditFormulaRuleVCDelegate>delegate;
    IBOutlet UITableView *editorTable;
    SelectItemVC *selectItemVC;
    IBOutlet UITableViewCell *formulaCell;
    NSString *itemName;
    IBOutlet UILabel *titleLabel;
    UITextView *_ptv;//textViewPointer
    NSMutableDictionary *ruleDict;
    NSArray *itemsArray;
}
@property (nonatomic, assign) id<EditFormulaRuleVCDelegate>delegate;
@property (nonatomic, retain) IBOutlet UITableViewCell *formulaCell;

- (void)setRuleDictionary:(NSDictionary*)dictionary;
- (void)setItemsArray:(NSArray*)array;
- (IBAction)goBack;
- (IBAction)save;

@end

//
//  Converter.h
//  currency test
//
//  Created by Admin on 27.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ConverterDelegate

- (void)convUpdated;

@end

@class Item;

@interface Converter : NSObject
{
    NSMutableDictionary *converter;
    NSMutableArray      *items;
    NSString            *editedKey;
    int formulaRuleCount;
    int linearRuleCount;
    NSMutableArray *itemsToShow;
    
    id<ConverterDelegate>delegate;
}
@property (nonatomic,assign) id<ConverterDelegate>delegate;
@property (nonatomic) BOOL removeZeros;
@property (nonatomic) NSUInteger convNum;
@property (nonatomic) BOOL isFav;

- (NSDictionary*)getConverterDictionary;
- (id)initWithDict:(NSDictionary*)dict;
- (void)setNewValue:(NSString*)value forKey:(NSString*)key;
- (void)hideItemForRow:(NSUInteger)row;
- (NSDictionary*)itemForRow:(NSUInteger)row;
- (NSString*)itemNameForRow:(NSUInteger)row;
- (NSUInteger)showingItems;
- (NSString*)valueForRow:(NSUInteger)row;
//for edit mode
- (NSDictionary*)allItems_itemForRow:(NSUInteger)row;
- (NSString*)allItems_itemNameForRow:(NSUInteger)row;
- (void)moveItemFrom:(NSUInteger)sourceRow to:(NSUInteger)destinationRow;

@end

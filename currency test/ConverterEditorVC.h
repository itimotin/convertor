//
//  ConverterEditorVC.h
//  currency test
//
//  Created by Admin on 10.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

enum convType {
    type_calc,
    type_conv
};

@protocol ConverterEditorVCDelegate

- (void)CEDidFinishEditing:(NSDictionary*)tempDict;

@end

@class EditLinearRuleVC,EditFormulaRuleVC;

@interface ConverterEditorVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
{
    id<ConverterEditorVCDelegate>delegate;
    
    NSMutableDictionary *templateDictionary;
    NSMutableArray *itemsArray;
    NSMutableArray *rulesArray;
    
    enum convType _convType;
    
    IBOutlet UITableView *editorTable;
    
    
    EditLinearRuleVC    *linearRuleEditor;
    EditFormulaRuleVC   *formulaRuleEditor;
    UITextField *_pTitleTF;
    NSString *templateTitle;
    
    IBOutlet UIImageView *backBtn;
    IBOutlet UIImageView *saveBtn;
    
    BOOL isEditing;
    
    NSUInteger _convNum;
    BOOL isFavorites;
    
    BOOL editItem;
    BOOL editRule;
    uint itemIndex;
    uint ruleIndex;

    IBOutlet UITextField *labelTemplateName;
}


@property (nonatomic, assign) id<ConverterEditorVCDelegate>delegate;
@property (nonatomic, retain) IBOutlet UITableViewCell *titleCell;

- (IBAction)goBack;
- (IBAction)save;
- (void)setConverterType:(int)type;
- (void)setTemplateDictionary:(NSDictionary*)dictionary;
- (void)setConvNum:(NSUInteger)convNum favorites:(BOOL)isFav;
@end

//
//  EditFormulaRuleVC.m
//  currency test
//
//  Created by Admin on 14.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EditFormulaRuleVC.h"
#import "SelectItemVC.h"

@implementation EditFormulaRuleVC

@synthesize formulaCell;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
//    EditFormulaRuleVC *controllerInstance;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
//        controllerInstance = [[EditFormulaRuleVC alloc] initWithNibName:@"IPadEditFormulaRuleVC" bundle:nil];
        self = [super initWithNibName:@"IPadEditFormulaRuleVC" bundle:nibBundleOrNil];
     
    }
    else
    {
//        controllerInstance = [[EditFormulaRuleVC alloc] initWithNibName:@"EditFormulaRuleVC" bundle:nil];
        self = [super initWithNibName:@"EditFormulaRuleVC" bundle:nibBundleOrNil];
    }
//    self.view = controllerInstance;
//    [controllerInstance release];
//    
  
    
    if (self) {
        // Custom initialization
//        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
        self.view.backgroundColor = [UIColor blackColor];
    }
    return self;
}

- (void)setItemsArray:(NSArray *)array
{
    if (itemsArray!=nil) 
        [itemsArray release],itemsArray = nil;
    //NSLog(@"iacount =%d",[array count]);
    itemsArray = [[NSArray alloc] initWithArray:array];
    //NSLog(@"iacount in fe =%d",[itemsArray count]);
}

- (void)setRuleDictionary:(NSDictionary *)dictionary
{
    if (ruleDict) {
        [ruleDict release],ruleDict = nil;
    }
    ruleDict = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
    if (itemName) {
        [itemName release],itemName = nil;
    }
    
    
    itemName = [[NSString alloc] initWithString:[ruleDict valueForKey:@"item"]];
    
}

- (void)selectedItem:(NSUInteger)item
{
    if (itemName!=nil) 
        [itemName release],itemName = nil;
    
    itemName = [[NSString alloc] initWithString:[itemsArray objectAtIndex:item]];
    [editorTable reloadData];
}

- (void)goBack
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)save
{
    
    char error = 0;
    if (_ptv==NULL) {
        error = 1;//textView pointer is NULL
    }else{
        if ([_ptv.text isEqualToString:@""]) {
            error = 2;//formula string is empty
        }
    }
    if (itemName==nil || [itemName isEqualToString:@""]) {
        error = 3;//item not selected or pointer is nil
    }
    
    if (error!=0) {
        UIView *errorAllertview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 150)];
        UILabel *errorAllertLabel = [[UILabel alloc] initWithFrame:errorAllertview.frame];
        errorAllertview.center= self.view.center;
        errorAllertLabel.textColor = [UIColor whiteColor];
        errorAllertLabel.textAlignment = UITextAlignmentCenter;
        errorAllertLabel.font = [UIFont systemFontOfSize:17];
        errorAllertLabel.numberOfLines = 5;
        errorAllertLabel.backgroundColor = [UIColor clearColor];
        [errorAllertLabel setFont:[UIFont fontWithName:@"Olney-Light" size:17]];
        [errorAllertview.layer setCornerRadius:15];
        errorAllertview.backgroundColor = [UIColor colorWithRed:1.0f green:0.3f blue:0.3f alpha:0.8f];
        errorAllertview.alpha = 0.0f;
        
        if ([_ptv isFirstResponder]) {
            CGRect frame = editorTable.frame;
            
            frame.size.height +=216.0f;
            
            [UIView animateWithDuration:0.1 animations:^{
                editorTable.frame = frame;
            }];
            [_ptv resignFirstResponder];
        }
        
        switch (error) {
            case 1:
                [errorAllertLabel setText:@"Error\ntextView pointer is NULL"];
                [errorAllertLabel setFont:[UIFont fontWithName:@"Olney-Light" size:17]];
                [errorAllertview addSubview:errorAllertLabel];
                [self.view addSubview:errorAllertview];
                
                [UIView animateWithDuration:0.2f animations:^{
                    errorAllertview.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:2.0f animations:^{
                        errorAllertview.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        [errorAllertview removeFromSuperview];
                        [errorAllertview release];
                        [errorAllertLabel release];
                    }];                
                }];
                break;
            case 2:
                [errorAllertLabel setText:@"Error\nformula string is empty"];
                [errorAllertLabel setFont:[UIFont fontWithName:@"Olney-Light" size:17]];
                [errorAllertview addSubview:errorAllertLabel];
                [self.view addSubview:errorAllertview];
                
                [UIView animateWithDuration:0.2f animations:^{
                    errorAllertview.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:2.0f animations:^{
                        errorAllertview.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        [errorAllertview removeFromSuperview];
                        [errorAllertview release];
                        [errorAllertLabel release];
                    }];                
                }];
                break;
            case 3:
                [errorAllertLabel setText:@"Error\nitem not selected or pointer is nil"];
                [errorAllertview addSubview:errorAllertLabel];
                [errorAllertLabel setFont:[UIFont fontWithName:@"Olney-Light" size:17]];
                [self.view addSubview:errorAllertview];
                
                [UIView animateWithDuration:0.2f animations:^{
                    errorAllertview.alpha = 1.0f;
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:2.0f animations:^{
                        errorAllertview.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        [errorAllertview removeFromSuperview];
                        [errorAllertview release];
                        [errorAllertLabel release];
                    }];                
                }];
                break;
            
        }
        return;
    }
    if (ruleDict!=nil) 
        [ruleDict release],ruleDict = nil;
    
    ruleDict = [[NSMutableDictionary alloc] initWithCapacity:1];
    
    [ruleDict setValue:@"formula" forKey:@"type"];
    
    [ruleDict setValue:[_ptv.text stringByAppendingString:@"\n"] forKey:@"formula"];
    [ruleDict setValue:itemName forKey:@"item"];
    
    [delegate saveFormulaRule:ruleDict];
    [self dismissModalViewControllerAnimated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    _ptv = textView;
    CGRect frame = editorTable.frame;
    
    frame.size.height -=216.0f;
    
    [UIView animateWithDuration:0.3 animations:^{
        editorTable.frame = frame;
    }];
    
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        CGRect frame = editorTable.frame;
        
        frame.size.height +=216.0f;
        
        [UIView animateWithDuration:0.1 animations:^{
            editorTable.frame = frame;
        }];
        [textView resignFirstResponder];
        [textView setFont:[UIFont fontWithName:@"Olney-Light" size:17]];
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    selectItemVC = [[SelectItemVC alloc] initWithStyle:UITableViewStylePlain];
    selectItemVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//    editorTable.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
    editorTable.backgroundColor = [UIColor blackColor];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    _ptv = NULL;
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    static NSString *BigCellID = @"_kBigCell";
    
    if (indexPath.section == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:BigCellID];
        if (cell == nil) {
            cell = formulaCell;
//            self.formulaCell = nil;
            for (UIView *subV in [cell.contentView subviews]) {
                if  ([subV isKindOfClass:[UITextView class]]){
                    _ptv = (UITextView*)subV;
                    [_ptv setFont:[UIFont fontWithName:@"Olney-Light" size:17]];
                }
            }
        }
        // Configure the cell...
        if ([ruleDict valueForKey:@"formula"]) {
            [_ptv setText:[ruleDict valueForKey:@"formula"]];
            [_ptv setFont:[UIFont fontWithName:@"Olney-Light" size:17]];
        }
        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        UIImageView *imgView = [[UIImageView alloc] initWithFrame:cell.frame];
//        [imgView setImage:[UIImage imageNamed:@"table_cell_back.png"]];
//        cell.backgroundView = imgView;
//        [imgView release];
    }  
        // Configure the cell...
    if (itemName) {
        cell.textLabel.text = itemName;
        [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:17]];
    }
    return cell;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section==1)
        return @"Formula";
    return @"Item";
    
}



//- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
//{
//    if (section==1) 
//        return @"Enter a math expression to calculate the item from others. Wrap other IDs in square brackets to reference their values";
//    return @"" ;
//    
//}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    NSString *sectionFooterTitle;
    if (section == 1) {
        sectionFooterTitle = @"Enter a math expression to calculate the item from others. Wrap other IDs in square brackets to reference their values";
    }
    else {
        sectionFooterTitle = @"";
    }
    UILabel *label = [[[UILabel alloc] init] autorelease];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label.frame = CGRectMake(0, -2, 768, 50);
    }
    else
    {
       label.frame = CGRectMake(0, -2, 320, 50);
    }

    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:@"Olney-Light" size:20];
    label.text = sectionFooterTitle;
    label.numberOfLines  = 3;
    label.textAlignment = UITextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    
    UIView *sectionFooterView;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
        sectionFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 30, 768, 100)];
    }
    else
    {
        sectionFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 30, 320, 100)];
    }
    [sectionFooterView autorelease];
    [sectionFooterView addSubview:label];
    
    return sectionFooterView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section) {
        return 88;
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){return 66;}else{return 44;}
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate



- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        return nil;
    }
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section==0) {
        NSLog(@"iacount in fe =%d",[itemsArray count]);
        if ([itemsArray count]>0) {
            [selectItemVC setItemsArray:itemsArray];
            selectItemVC.delegate = (id)self;
            
            CGRect frame = editorTable.frame;
            frame.size.height +=216.0f;
            editorTable.frame = frame;
            
            [self presentModalViewController:selectItemVC animated:YES];
        }
        
    }
}



@end

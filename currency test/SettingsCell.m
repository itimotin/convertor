//
//  SettingsCell.m
//  currency test
//
//  Created by Admin on 23.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingsCell.h"

@implementation SettingsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setNameLabelText:(NSString *)text
{
    [nameLabel setText:text];
    [nameLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19.0]];
    
    NSLog(@"settings cell Font : %@", nameLabel.font);

}
- (void)setDescripionLabelText:(NSString *)text
{
    [descriptionLabel setText:text];
}

- (void)setChecked:(BOOL)checked
{
    checkMark.hidden = !checked;
}

@end

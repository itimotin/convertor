//
//  ConverterEditorVC.m
//  currency test
//
//  Created by Admin on 10.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ConverterEditorVC.h"
#import "EditLinearRuleVC.h"
#import "EditFormulaRuleVC.h"
#import "AddItemVC.h"

@implementation ConverterEditorVC

@synthesize titleCell;
@synthesize delegate;

- (IBAction)goBack
{
    [self dismissModalViewControllerAnimated:YES];
}
- (IBAction)save
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"All.plist"];
    NSString *favPath = [[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"Favorites.plist"];
    if (!templateTitle) {
        if (_pTitleTF.text) {
            templateTitle = [NSString stringWithString:_pTitleTF.text];
            [templateDictionary setValue:templateTitle forKey:@"Name"];
        }else{
            UIView *errorAllertview = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 150)] autorelease];
            UILabel *errorAllertLabel = [[UILabel alloc] initWithFrame:errorAllertview.frame];
            errorAllertview.center= self.view.center;
            errorAllertLabel.textColor = [UIColor whiteColor];
            errorAllertLabel.textAlignment = UITextAlignmentCenter;
            errorAllertLabel.font = [UIFont systemFontOfSize:17];
            errorAllertLabel.numberOfLines = 5;
            errorAllertLabel.backgroundColor = [UIColor clearColor];
            [errorAllertview.layer setCornerRadius:15];
            errorAllertview.backgroundColor = [UIColor colorWithRed:1.0f green:0.3f blue:0.3f alpha:0.8f];
            errorAllertview.alpha = 0.0f;
            
            [errorAllertLabel setText:@"Error\nTemplate Name Not Set"];
            [errorAllertview addSubview:errorAllertLabel];
            [self.view addSubview:errorAllertview];
            
            [UIView animateWithDuration:0.2f animations:^{
                errorAllertview.alpha = 1.0f;
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:2.0f animations:^{
                    errorAllertview.alpha = 0.0f;
                } completion:^(BOOL finished) {
                    [errorAllertview removeFromSuperview];
                    [errorAllertview release];
                    [errorAllertLabel release];
                }];                
            }];
        }
    }
    for (NSString *s in itemsArray) {
//        NSLog(@"%@",s);
    }
    [templateDictionary setValue:itemsArray forKey:@"Items"];
    (_convType==type_conv) ? [templateDictionary setValue:@"converter" forKey:@"Type"] : [templateDictionary setValue:@"calculator" forKey:@"Type"];
    if (_convType == type_calc)
    {
        for (NSDictionary *rule in rulesArray) {
            if ([rule valueForKey:@"item"]) {
                NSMutableDictionary *item_dict = [NSMutableDictionary dictionaryWithDictionary:[templateDictionary valueForKey:[rule valueForKey:@"item"]]];
                [item_dict setValue:@"resItem" forKey:@"resItem"];
            }
        }
    }
    [templateDictionary setValue:rulesArray forKey:@"rules"];
    NSDictionary *convertersDictionary;
    if (isFavorites) {
        convertersDictionary = [NSDictionary dictionaryWithContentsOfFile:favPath];
    }else{
        convertersDictionary = [NSDictionary dictionaryWithContentsOfFile:path];
    }
    
    NSArray *array = [[convertersDictionary allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return  [obj1 compare:obj2 options:NSNumericSearch];
    }];
    
    int index = [[array lastObject] rangeOfString:@" "].location;
    int lastConvNum = [[[array lastObject] substringFromIndex:index] intValue];
    
    if (isEditing) {
        if (isFavorites) {
            [convertersDictionary setValue:templateDictionary forKey:[NSString stringWithFormat:@"Converter %d",_convNum]];
            [convertersDictionary writeToFile:favPath atomically:YES];
        }else{
            [convertersDictionary setValue:templateDictionary forKey:[NSString stringWithFormat:@"Converter %d",_convNum]];
            [convertersDictionary writeToFile:path atomically:YES];
        }
    }else{
        if (isFavorites){
            [convertersDictionary setValue:templateDictionary forKey:[NSString stringWithFormat:@"Converter %d",lastConvNum+1]];
            [convertersDictionary writeToFile:favPath atomically:YES];
        }else{
            [convertersDictionary setValue:templateDictionary forKey:[NSString stringWithFormat:@"Converter %d",lastConvNum+1]];
            [convertersDictionary writeToFile:path atomically:YES];
        }
    }
    //[convertersDictionary writeToFile:[[dirPaths objectAtIndex:0] stringByAppendingPathComponent:@"test.plist"] atomically:YES];
    [delegate CEDidFinishEditing:templateDictionary];
    [self dismissModalViewControllerAnimated:YES];
}

- (void)dealloc
{
    [templateDictionary release],templateDictionary = nil;
    [linearRuleEditor release],linearRuleEditor = nil;
    [formulaRuleEditor release],formulaRuleEditor = nil;
    [itemsArray release],itemsArray = nil;
    [rulesArray release],rulesArray = nil;
    [labelTemplateName release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self = [super initWithNibName:@"IPadConverterEditorVC" bundle:nibBundleOrNil];
        
    }
    else
    {
        self = [super initWithNibName:@"ConverterEditorVC" bundle:nibBundleOrNil];
    }
    
    if (self) {
        // Custom initialization
        templateDictionary = [[NSMutableDictionary alloc] initWithCapacity:0];
        itemsArray = [[NSMutableArray alloc] initWithCapacity:0];
        rulesArray = [[NSMutableArray alloc] initWithCapacity:0];
        isEditing = NO;
        
    }
    return self;
}

- (void)setConverterType:(int)type
{
    if (type==0) 
        _convType = type_conv;
    else
        _convType = type_calc;
}

- (void)setConvNum:(NSUInteger)convNum favorites:(BOOL)isFav
{
    isFavorites = isFav;
    _convNum = convNum;
}
- (void)setTemplateDictionary:(NSDictionary *)dictionary
{
    if (templateDictionary)
        [templateDictionary release],templateDictionary = nil;
    
    templateDictionary = [[NSMutableDictionary alloc] initWithDictionary:dictionary];
    if (itemsArray)
        [itemsArray release],itemsArray = nil;
    itemsArray = [[NSMutableArray alloc] initWithArray:[[templateDictionary valueForKey:@"Items"] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2 options:NSLiteralSearch];
    }]];
    if (rulesArray)
        [rulesArray release],rulesArray = nil;
    rulesArray = [[NSMutableArray alloc] initWithArray:[templateDictionary valueForKey:@"rules"]];
    if (templateTitle)
        [templateTitle release],templateTitle = nil;
    templateTitle = [[NSString alloc] initWithString:[templateDictionary valueForKey:@"Name"]];
    
    isEditing = YES;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
    
    [editorTable setEditing:YES];
    editorTable.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back.png"]];
    if (templateTitle) {
        [labelTemplateName setText:templateTitle];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                [labelTemplateName setFont:[UIFont fontWithName:@"Olney-Light" size:24]];
        }else{
                [labelTemplateName setFont:[UIFont fontWithName:@"Olney-Light" size:18]];
        }

    }

    
}

- (void)viewDidUnload
{
    [labelTemplateName release];
    labelTemplateName = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}

- (void)viewDidAppear:(BOOL)animated
{
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    if (section==0) {
        return ([itemsArray count]+1);
    }
    if (section==1) {
        return ([rulesArray count]+2);
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
//    static NSString *TitleCellID = @"TitleCell";
    static NSString *addCell = @"addCell";
    
    UITableViewCell *cell;
    
    // Configure the cell...
//    if (indexPath.section==0) {
//         
//        cell = [tableView dequeueReusableCellWithIdentifier:TitleCellID];
//        if (cell == nil) {
//            cell = titleCell;
//            cell.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"table_cell_back.png"]];
//            for (UIView *subV in [cell.contentView subviews]) {
//                
//                if  ([subV isKindOfClass:[UITextField class]])
//                {
//                    _pTitleTF = (UITextField*)subV;
//                }
//            }
//        }
//        //cell config
//        if (templateTitle) {
//            [_pTitleTF setText:templateTitle];
//        }
//    }
    if (indexPath.section==0) {
        
        if ([itemsArray count]>0){
            if (indexPath.row == ([itemsArray count])) {
                cell = [tableView dequeueReusableCellWithIdentifier:addCell];
                if (cell == nil) {
                    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addCell] autorelease];
                    UIImageView *back = [[UIImageView alloc] initWithFrame:cell.frame];
                    back.image = [UIImage imageNamed:@"table_cell_back.png"];
                    cell.backgroundView = back;
                    [back release];
                    cell.contentView.backgroundColor = [UIColor clearColor];
                    cell.textLabel.backgroundColor = [UIColor clearColor];
                    cell.textLabel.textColor = [UIColor whiteColor];
                    [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
                }
                cell.textLabel.text = @"Add Item";
                cell.detailTextLabel.text = @"";
                [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
            }else{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
                    UIImageView *back = [[UIImageView alloc] initWithFrame:cell.frame];
                    back.image = [UIImage imageNamed:@"table_cell_back.png"];
                    cell.backgroundView = back;
                    [back release];
                    cell.contentView.backgroundColor = [UIColor clearColor];
                    cell.textLabel.backgroundColor = [UIColor clearColor];
                    cell.textLabel.textColor = [UIColor whiteColor];
                    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
                    [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                }
                cell.textLabel.text = [itemsArray objectAtIndex:indexPath.row];
                cell.detailTextLabel.text = [[templateDictionary valueForKey:cell.textLabel.text] valueForKey:@"description"];
                [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                [cell.detailTextLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                cell.textLabel.textColor = [UIColor whiteColor];
//                cell.detailTextLabel.textColor = [UIColor whiteColor];
            }
        }else{
            cell = [tableView dequeueReusableCellWithIdentifier:addCell];
            if (cell == nil) {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addCell] autorelease];
                UIImageView *back = [[UIImageView alloc] initWithFrame:cell.frame];
                back.image = [UIImage imageNamed:@"table_cell_back.png"];
                cell.backgroundView = back;
                [back release];
                cell.contentView.backgroundColor = [UIColor clearColor];
                cell.textLabel.backgroundColor = [UIColor clearColor];
                cell.textLabel.textColor = [UIColor whiteColor];
                cell.detailTextLabel.backgroundColor = [UIColor clearColor];
                [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
            }
            cell.textLabel.text = @"Add Item";
            [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
            cell.textLabel.textColor = [UIColor whiteColor];
        }
        //data from items array
        
    }
    if (indexPath.section==1) {
        
        if ([rulesArray count]>0){
            if (indexPath.row == ([rulesArray count])){
                cell = [tableView dequeueReusableCellWithIdentifier:addCell];
                if (cell == nil) {
                    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addCell] autorelease];
                    UIImageView *back = [[UIImageView alloc] initWithFrame:cell.frame];
                    back.image = [UIImage imageNamed:@"table_cell_back.png"];
                    cell.backgroundView = back;
                    [back release];
                    cell.contentView.backgroundColor = [UIColor clearColor];
                    cell.textLabel.backgroundColor = [UIColor clearColor];
                    [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                    cell.textLabel.textColor = [UIColor whiteColor];
                    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
                    [cell.detailTextLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                }
                
                cell.textLabel.text = @"Add Linear Rule";
                [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                cell.textLabel.textColor = [UIColor whiteColor];
                
            }
            else if (indexPath.row == ([rulesArray count]+1)){
                cell = [tableView dequeueReusableCellWithIdentifier:addCell];
                if (cell == nil) {
                    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addCell] autorelease];
                    UIImageView *back = [[UIImageView alloc] initWithFrame:cell.frame];
                    back.image = [UIImage imageNamed:@"table_cell_back.png"];
                    cell.backgroundView = back;
                    [back release];
                    cell.contentView.backgroundColor = [UIColor clearColor];
                    cell.textLabel.backgroundColor = [UIColor clearColor];
                    
                    cell.textLabel.textColor = [UIColor whiteColor];
                    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
                    [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                    [cell.detailTextLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                }
                cell.textLabel.text = @"Add Formula Rule";
                [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                cell.textLabel.textColor = [UIColor whiteColor];
                
            }
            else{
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
                    UIImageView *back = [[UIImageView alloc] initWithFrame:cell.frame];
                    back.image = [UIImage imageNamed:@"table_cell_back.png"];
                    cell.backgroundView = back;
                    [back release];
                    cell.contentView.backgroundColor = [UIColor clearColor];
                    cell.textLabel.backgroundColor = [UIColor clearColor];
                    [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                    cell.textLabel.textColor = [UIColor whiteColor];
                    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
                    [cell.detailTextLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                }
                cell.textLabel.text = [NSString stringWithFormat:@"Rule %d",indexPath.row];
                [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                cell.textLabel.textColor = [UIColor whiteColor];
                NSString *type = [[rulesArray objectAtIndex:indexPath.row] valueForKey:@"type"];
                if ([type isEqualToString:@"formula"]){
                    cell.detailTextLabel.text = [type stringByAppendingFormat:@":%@",[[rulesArray objectAtIndex:indexPath.row] valueForKey:type]];
                    [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                }else{
                    NSString *refItem = [[rulesArray objectAtIndex:indexPath.row] valueForKey:@"refItem"];
                    NSString *item = [[rulesArray objectAtIndex:indexPath.row] valueForKey:@"item"];
                    double slope = [[[rulesArray objectAtIndex:indexPath.row] valueForKey:@"slope"] doubleValue];
                    double intercept = [[[rulesArray objectAtIndex:indexPath.row] valueForKey:@"intercept"] doubleValue];
                    cell.detailTextLabel.text = [type stringByAppendingFormat:@":[%@]=%.3f*[%@]+%.3f",item,slope,refItem,intercept];
                    [cell.detailTextLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                }
            }
        }else{
            if (indexPath.row == 0){
                cell = [tableView dequeueReusableCellWithIdentifier:addCell];
                if (cell == nil) {
                    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addCell] autorelease];
                    UIImageView *back = [[UIImageView alloc] initWithFrame:cell.frame];
                    back.image = [UIImage imageNamed:@"table_cell_back.png"];
                    cell.backgroundView = back;
                    [back release];
                    cell.contentView.backgroundColor = [UIColor clearColor];
                    cell.textLabel.backgroundColor = [UIColor clearColor];
                    [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                    cell.textLabel.textColor = [UIColor whiteColor];
                    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
                    [cell.detailTextLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                }
                cell.textLabel.text = @"Add Linear Rule";
                [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                cell.textLabel.textColor = [UIColor whiteColor];
            }
            else if (indexPath.row == 1){
                cell = [tableView dequeueReusableCellWithIdentifier:addCell];
                if (cell == nil) {
                    cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addCell] autorelease];
                    UIImageView *back = [[UIImageView alloc] initWithFrame:cell.frame];
                    back.image = [UIImage imageNamed:@"table_cell_back.png"];
                    cell.backgroundView = back;
                    [back release];
                    cell.contentView.backgroundColor = [UIColor clearColor];
                    cell.textLabel.backgroundColor = [UIColor clearColor];
                    cell.textLabel.textColor = [UIColor whiteColor];
                    [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
                    [cell.detailTextLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                }
                cell.textLabel.text = @"Add Formula Rule";
                [cell.textLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19]];
                cell.textLabel.textColor = [UIColor whiteColor];
            }
        }
        
        //data from rules array
        
    }
    return cell;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    switch (section) {
////        case 0:
////            return @"Converter Template";
////            break;
//        case 0:
//            return @"Items";
//            break;
//        case 1:
//            return @"Rules";
//            break;
//         
//    }
//    return @"";
// 
//}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionTitle ;
    switch (section) {
        case 0:
        {
           sectionTitle= @"Items";
        }
            break;
        case 1:
        {
            sectionTitle = @"Rules";
        }
            break;
    }

            // Create label with section title
            UILabel *label = [[[UILabel alloc] init] autorelease];
            UIView *view;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {

                    label.frame = CGRectMake(0, 0, 768, 20);
         view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, 100)];
    }
    else
    {
            label.frame = CGRectMake(0, 0, 320, 20);
         view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    }

            label.textColor = [UIColor whiteColor];
            label.font = [UIFont fontWithName:@"Olney-Light" size:20];
            label.text = sectionTitle;
            label.backgroundColor = [UIColor blackColor];
    
            // Create header view and add label as a subview
          
            [view autorelease];
            [view addSubview:label];
        return view;

    

}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        if (indexPath.row==[itemsArray count]) {
            return UITableViewCellEditingStyleInsert;
        }
        return UITableViewCellEditingStyleDelete;
    }
    if (indexPath.section==1) {
        if (indexPath.row==[rulesArray count])
            return UITableViewCellEditingStyleInsert;
        if (indexPath.row==([rulesArray count]+1)) {
            return UITableViewCellEditingStyleInsert;
        }
        return UITableViewCellEditingStyleDelete;
    }
    
    return UITableViewCellEditingStyleNone;
    
}

 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }


 // Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        if (indexPath.section == 0) {
            [templateDictionary removeObjectForKey:[itemsArray objectAtIndex:indexPath.row]];
            [itemsArray removeObjectAtIndex:indexPath.row];
            
        }else if (indexPath.section == 1){
            [rulesArray removeObjectAtIndex:indexPath.row];
            
        }
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        
    }   

}
 


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.section==0) {
        if (indexPath.row==[itemsArray count]) {
            AddItemVC *addvc = [[[AddItemVC alloc] initWithNibName:@"AddItemVC" bundle:nil] autorelease];
            [addvc setDelegate:(id)self];
            addvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentModalViewController:addvc animated:YES];
            editItem = NO;
            
        }else{
            editItem = YES;
            AddItemVC *addvc = [[[AddItemVC alloc] initWithNibName:@"AddItemVC" bundle:nil] autorelease];
            [addvc setDelegate:(id)self];
            [addvc setItem:[itemsArray objectAtIndex:indexPath.row] Dictionary:[templateDictionary valueForKey:[itemsArray objectAtIndex:indexPath.row]]];
            addvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentModalViewController:addvc animated:YES];
        }
    }
    
    if (indexPath.section==1){
        if (indexPath.row==[rulesArray count]){
            //if ([itemsArray count]>0) 
            editRule = NO;
            EditLinearRuleVC *lrEditor = [[[EditLinearRuleVC alloc] initWithNibName:@"EditLinearRuleVC" bundle:nil] autorelease];
            lrEditor.delegate = (id)self;
            [lrEditor setItemsArray:itemsArray];
            [self presentModalViewController:lrEditor animated:YES];
            
        }
            
        else if (indexPath.row==[rulesArray count]+1){
            editRule = NO;
            //if ([itemsArray count]>0)
            EditFormulaRuleVC *frEditor = [[[EditFormulaRuleVC alloc] initWithNibName:@"EditFormulaRuleVC" bundle:nil] autorelease];
            frEditor.delegate = (id)self;
            [frEditor setItemsArray:itemsArray];
            [self presentModalViewController:frEditor animated:YES];
            
            
        }
        
        else{
            NSDictionary *dict = [rulesArray objectAtIndex:indexPath.row];
            if ([[dict valueForKey:@"type"] isEqualToString:@"linear"]){
                editRule = YES;
                ruleIndex = indexPath.row;
                EditLinearRuleVC *lrEditor = [[[EditLinearRuleVC alloc] initWithNibName:@"EditLinearRuleVC" bundle:nil] autorelease];
                lrEditor.delegate = (id)self;
                [lrEditor setItemsArray:itemsArray];
                [lrEditor setRuleDictionary:dict];
                [self presentModalViewController:lrEditor animated:YES];
            }else{
                editRule = YES;
                ruleIndex = indexPath.row;
                EditFormulaRuleVC *frEditor = [[[EditFormulaRuleVC alloc] initWithNibName:@"EditFormulaRuleVC" bundle:nil] autorelease];
                frEditor.delegate = (id)self;
                [frEditor setItemsArray:itemsArray];
                [frEditor setRuleDictionary:dict];
                [self presentModalViewController:frEditor animated:YES];
            }
        }
    }
}


#pragma mark - AddItemVC delegate
- (void)saveItem:(NSDictionary*)item WithKeyName:(NSString*)name
{
    
    [templateDictionary setValue:item forKey:name];
    if (!editItem) {
        [itemsArray addObject:name];
    }
    [editorTable reloadData];
}

#pragma mark - EditFormulaRuleVC delegate
- (void)saveFormulaRule:(NSDictionary*)ruleDict
{
    if (editRule){
        [rulesArray replaceObjectAtIndex:ruleIndex withObject:ruleDict];
    }else{
        [rulesArray addObject:ruleDict];
        [editorTable reloadData];
    }
}

#pragma mark - EditLinearRuleVC delegate
- (void)saveLinearRule:(NSDictionary*)ruleDict
{
    [rulesArray addObject:ruleDict];
    [editorTable reloadData];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end

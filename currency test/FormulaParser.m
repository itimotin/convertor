//
//  FormulaParser.m
//  NewEmpty
//
//  Created by Admin on 28.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FormulaParser.h"

enum actions
{
    a_ERR,      //Error
    a_EVAL,      //evaluate
    a_PUSH,     //push to operator stack
    a_RET       //return expression result
};

enum tokenType 
{
    /* operators */
    tUno_prefix,    //Prefix Unary operator ,at this time its just unary minus
    tBin1,          //Binary operator weight 1  ('+' '-')
    tBin2,          //Binary operator weight 2  ('*' '/')
    tBin3,          //Binary operator weight 3  ('^' '\')
    tFun,           //Funcion
    tLPR,           //Left parenthesis
    tRPR,           //Right parenthesis
    tSep,           //Function arguments separator
    tEof,           //End of string
    tMaxOp,         // maximum number of operators
    tUno_suffix,    //Suffix Unary operator, not used in map because it can be placed only before a value otherwise is error
    /* non-operators */
    tVal            //Value
};

enum tokenName
{
    t_VAL,          //  value
    t_END,          //  '\n'
    t_LeftPr,       //  '('
    t_RightPr,      //  ')'
    t_comma,        //  ','
    t_SemiCol,      //  ';'
    t_UMinus,       //  unary minus
    t_Add,          //  '+'  
    t_Sub,          //  '-'
    t_Mul,          //  '*'
    t_Div,          //  '/'
    t_Pow,          //  '^'
    t_Mod,          //  '\'
    t_Fct,          //  '!'
    t_PowTwo,       //  '\u00B2' superScript 2
    t_PowThree,     //  '\u00B3' superScript 3
    t_AND,          //  '&' binary AND
    t_OR,           //  '|' binary OR
    t_XOR,          //  '#' bynary XOR
    t_NOT,          //  '~' bynary NOT
    t_Fewer,        //  '<'
    t_Greater,      //  '>'
    t_Equal,        //  '='
    t_iif,          //  iff(c, x1, x2) if c!=0 return x1 else x2
    t_err,          //  return and display error
    t_abs,          //  abs(x)  |x| modulo 
    t_round,        //  round(x)
    t_rand,         //  rand return random number between 0 and 1
    t_sqrt,         //  sqrt(x) square root of x
    t_e,            //  return Euler's constant M_E
    t_lg,           //  lg(x)   
    t_pi,           //  return π constant       M_PI
    t_sin,          //  sin(x)      sin
    t_cos,          //  cos(x)      cos
    t_tan,          //  tan(x)      tan
    t_asin,         //  asin(x)     arc sin
    t_acos,         //  acos(x)     arc cos
    t_atan,         //  atan(x)     arc tan
    t_sinh,         //  sinh(x)     hyperbolic sin
    t_cosh,         //  cosh(x)     hyperbolic cos
    t_tanh          //  tanh(x)     hyperbolic tan
};

typedef struct
{
    enum tokenType tType;   
    enum tokenName tName;        
    
}token;

//карта выбора действия / choose action map
char actionsMap[tMaxOp][tMaxOp] =
{
    /* OPERATIONS       -------------------------------- INPUT --------------------------------- */
    /*  STACK            tUnoP    tBin1   tBin2   tBin3   tFun    tLPR    tRPR    tSep    tEof   */
    /*                  ------------------------------------------------------------------------ */  
    /*  tUnoP */       {a_PUSH,  a_EVAL, a_EVAL, a_EVAL, a_PUSH, a_PUSH, a_EVAL, a_EVAL, a_EVAL  },
    /*  tBin1 */       {a_PUSH,  a_EVAL, a_PUSH, a_PUSH, a_PUSH, a_PUSH, a_EVAL, a_EVAL, a_EVAL  }, 
    /*  tBin2 */       {a_PUSH,  a_EVAL, a_EVAL, a_PUSH, a_PUSH, a_PUSH, a_EVAL, a_EVAL, a_EVAL  },  
    /*  tBin3 */       {a_PUSH,  a_EVAL, a_EVAL, a_EVAL, a_PUSH, a_PUSH, a_EVAL, a_EVAL, a_EVAL  },  
    /*  tFun  */       {a_EVAL,  a_EVAL, a_EVAL, a_EVAL, a_EVAL, a_PUSH, a_EVAL, a_EVAL, a_EVAL  },  
    /*  tLPR  */       {a_PUSH,  a_PUSH, a_PUSH, a_PUSH, a_PUSH, a_PUSH, a_PUSH, a_PUSH, a_ERR   },
    /*  tRPR  */       {a_EVAL,  a_EVAL, a_EVAL, a_EVAL, a_ERR,  a_ERR,  a_EVAL, a_EVAL, a_EVAL  },
    /*  tSep  */       {a_EVAL,  a_EVAL, a_EVAL, a_EVAL, a_EVAL, a_EVAL, a_EVAL, a_EVAL, a_ERR   },
    /*  tEof  */       {a_PUSH,  a_PUSH, a_PUSH, a_PUSH, a_PUSH, a_PUSH, a_ERR,  a_ERR,  a_RET   }
};

#define MAX_OPR         50000
#define MAX_VAL         50000
token oprStack[MAX_OPR];      /* operator stack */
double valStack[MAX_VAL];    /* value stack */
int oprTop, valTop;     /* top of operator, value stack */

token tok;
token prevToken; //previos token

static inline token
TokenMake(enum tokenType type, enum tokenName name)
{
    token t; t.tType = type; t.tName = name; return t;
}

@interface FormulaParser (Private)

- (token)operatorToken:(char)op;
- (token)functionToken:(NSString*)func;
- (double)getKeyVal:(NSString*)key;
- (BOOL)push;
- (BOOL)getToken;
- (BOOL)evaluate;
@end

@implementation FormulaParser

@synthesize converter;

- (void)dealloc
{
    converter = nil;
    [inputString release];
    [super dealloc];
}

- (double)fact:(double)n 
{
    double i, t;
    for (t = 1, i = 1; i <= n; i++)
        t *= i;
    return t;
}

- (double)error
{
    return INFINITY;
}

- (double)parse:(NSString*)string
{
    charIndex = 0;
    
    tok = TokenMake(tEof, t_END);
    
    inputString = [[NSString alloc] initWithString:string];
    
    oprTop = 0; valTop = -1;
    oprStack[oprTop] = tok;
    
    [self getToken];
    
    while (YES) {
        
        if (tok.tType==tVal) {
            if ([self push])
                return NAN;
            
            continue;
        }
        
        switch (actionsMap[oprStack[oprTop].tType][tok.tType]) {
            case a_PUSH:
                if ([self push]) 
                    return NAN;
                
                break;
            case a_EVAL:
                if ([self evaluate]) 
                    return NAN;
                
                break;
            case a_ERR:
                return NAN;
                break;
            case a_RET:
                if (valTop!=0) 
                    return NAN;
                NSLog(@"%f",valStack[valTop]);
                NSMutableString *str = [NSMutableString stringWithFormat:@"%.20f",valStack[valTop]];
                for (int i = [str length]-1; i>=0; i--) {
                    if ([str characterAtIndex:i]=='0')
                    {
                        if ([str characterAtIndex:i-2]=='.') {
                            break;
                        }
                        [str deleteCharactersInRange:NSMakeRange(i, 1)];
                    }else{
                        break;
                    }
                }
                
                return [str doubleValue];
                break;
                
        }
        
    }
    
    
    return NAN;
}

@end

@implementation FormulaParser (Private)


- (token)functionToken:(NSString *)func
{
    if ([func isEqualToString:@"iif"]) return TokenMake(tFun, t_iif);
    if ([func isEqualToString:@"err"]) return TokenMake(tFun, t_err);
    if ([func isEqualToString:@"abs"]) return TokenMake(tFun, t_abs);
    if ([func isEqualToString:@"round"]) return TokenMake(tFun, t_round);
    if ([func isEqualToString:@"rand"]) return TokenMake(tFun, t_rand);
    if ([func isEqualToString:@"sqrt"]) return TokenMake(tFun, t_sqrt);
    if ([func isEqualToString:@"e"]) return TokenMake(tFun, t_e);
    if ([func isEqualToString:@"lg"]) return TokenMake(tFun, t_lg);
    if ([func isEqualToString:@"pi"]) return TokenMake(tFun, t_pi);
    if ([func isEqualToString:@"sin"]) return TokenMake(tFun, t_sin);
    if ([func isEqualToString:@"cos"]) return TokenMake(tFun, t_cos);
    if ([func isEqualToString:@"tan"]) return TokenMake(tFun, t_tan);
    if ([func isEqualToString:@"asin"]) return TokenMake(tFun, t_asin);
    if ([func isEqualToString:@"acos"]) return TokenMake(tFun, t_acos);
    if ([func isEqualToString:@"atan"]) return TokenMake(tFun, t_atan);
    if ([func isEqualToString:@"sinh"]) return TokenMake(tFun, t_sinh);
    if ([func isEqualToString:@"cosh"]) return TokenMake(tFun, t_cosh);
    if ([func isEqualToString:@"tanh"]) return TokenMake(tFun, t_tanh);
    prevToken = tok;
    return TokenMake(tFun, t_err);
}

- (token)operatorToken:(char)op
{
    token _aTok = TokenMake(tFun, t_err);
    switch (op) {
        case '+':       _aTok = TokenMake(tBin1, t_Add);            break;
        case '-':       _aTok = TokenMake(tBin1, t_Sub);            break;
        case '*':       _aTok = TokenMake(tBin2, t_Mul);            break;
        case '/':       _aTok = TokenMake(tBin2, t_Div);            break;
        case '\\':      _aTok = TokenMake(tBin3, t_Mod);            break;
        case '^':       _aTok = TokenMake(tBin3, t_Pow);            break;
            //unary suffix надо вычислить
        case '!':       _aTok = TokenMake(tUno_suffix, t_Fct);      break;
        case '\u00B2':  _aTok = TokenMake(tUno_suffix, t_PowTwo);   break;
        case '\u00B3':  _aTok = TokenMake(tUno_suffix, t_PowThree); break;
            //binary logic operators priority 3
        case '&':      _aTok = TokenMake(tBin3, t_AND);             break;
        case '|':      _aTok = TokenMake(tBin3, t_OR);              break;
        case '#':      _aTok = TokenMake(tBin3, t_XOR);             break;
        case '~':      _aTok = TokenMake(tUno_prefix, t_NOT);       break;
        case '>':      _aTok = TokenMake(tBin3, t_Greater);         break;
        case '<':      _aTok = TokenMake(tBin3, t_Fewer);           break;
        case '=':      _aTok = TokenMake(tBin3, t_Equal);           break;
        case '(':      _aTok = TokenMake(tLPR, t_LeftPr);           break;
        case ')':      _aTok = TokenMake(tRPR, t_RightPr);          break;
        case ',':      _aTok = TokenMake(tSep, t_comma);            break;
        case ';':      _aTok = TokenMake(tSep, t_SemiCol);          break;
        case '\n':     _aTok = TokenMake(tEof, t_END);              break;    
    }
    
    //check for unary minus
    
    if (_aTok.tName==t_Sub && prevToken.tType!=tVal && prevToken.tType!=tRPR)
    {
        
        _aTok = TokenMake(tUno_prefix, t_UMinus);
        
        
    }
    //save previos token if unary suffix
    if (_aTok.tType!=tUno_suffix) 
        prevToken = _aTok;
    return _aTok;
}

- (double)getKeyVal:(NSString*)key
{
    return [[[self.converter valueForKey:key] valueForKey:@"value"] doubleValue];
}

- (BOOL)evaluate
{
    //NSLog(@"EVAL");
    switch (oprStack[oprTop].tName) {
        case t_Add:
            // eval X=X+X
            if (valTop<1) return YES;
            valStack[valTop-1]=valStack[valTop-1] + valStack[valTop];
            valTop--;
            break;
            // eval X=X-X
        case t_Sub:
            if (valTop<1) return YES;
            valStack[valTop-1]=valStack[valTop-1] - valStack[valTop];
            valTop--;
            break;
            // eval X=X*X
        case t_Mul:
            if (valTop<1) return YES;
            valStack[valTop-1]=valStack[valTop-1] * valStack[valTop];
            valTop--;
            break;
            // eval X=X/X
        case t_Div:
            if (valTop<1) return YES;
            valStack[valTop-1]=valStack[valTop-1] / valStack[valTop];
            valTop--;
            break;
            // eval X=X^X
        case t_Pow:
            if (valTop<1) return YES;
            valStack[valTop-1]= pow(valStack[valTop-1], valStack[valTop]);
            valTop--;
            break;
            // eval X=XmodulationX
        case t_Mod:
            if (valTop<1) return YES;
            valStack[valTop-1]= (int)valStack[valTop-1] % (int)valStack[valTop];
            valTop--;
            break;
            // eval X=-X
        case t_UMinus:
            if (valTop<0) return YES;
            valStack[valTop] = -valStack[valTop];
            break;
            /////////////////////////
            // eval X=X&X
        case t_AND:
            if (valTop<1) return YES;
            valStack[valTop-1] = (int)valStack[valTop-1] & (int)valStack[valTop];
            valTop--;
            break;
            // eval X=X|X
        case t_OR:
            if (valTop<1) return YES;
            valStack[valTop-1] = (int)valStack[valTop-1] | (int)valStack[valTop];
            valTop--;
            break;
            // eval X=X XOR X
        case t_XOR:
            if (valTop<1) return YES;
            valStack[valTop-1] = (int)valStack[valTop-1] ^ (int)valStack[valTop];
            valTop--;
            break;
            // eval X=!X 
        case t_NOT:
            if (valTop<0) return YES;
            valStack[valTop] = ~(int)valStack[valTop];
            break;
            //////////////////////////
            // eval X=sin(X)
        case t_sin:
            if (valTop<0) 
                return YES;
            valStack[valTop] = sin(valStack[valTop]);
            break;
            // eval X=cos(X)
        case t_cos:
            if (valTop<0) 
                return YES;
            valStack[valTop] = cos(valStack[valTop]);
            break;
            // eval X=tan(X)
        case t_tan:
            if (valTop<0) return YES;
            valStack[valTop] = tan(valStack[valTop]);
            break;
            // eval X=asin(X)
        case t_asin:
            if (valTop<0) return YES;
            valStack[valTop] = asin(valStack[valTop]);
            break;
            // eval X=acos(X)
        case t_acos:
            if (valTop<0) return YES;
            valStack[valTop] = acos(valStack[valTop]);
            break;
            // eval X=atan(X)
        case t_atan:
            if (valTop<0) return YES;
            valStack[valTop] = atan(valStack[valTop]);
            break;
            // eval X=sinh(X)
        case t_sinh:
            if (valTop<0) return YES;
            valStack[valTop] = sinh(valStack[valTop]);
            break;
            // eval X=cosh(X)
        case t_cosh:
            if (valTop<0) return YES;
            valStack[valTop] = cosh(valStack[valTop]);
            break;
            // eval X=tanh(X)
        case t_tanh:
            if (valTop<0) return YES;
            valStack[valTop] = tanh(valStack[valTop]);
            break;
            // eval X=lg(X)
        case t_lg:
            if (valTop<0) return YES;
            valStack[valTop] = log10(valStack[valTop]);
            break;
            // eval X=M_PI
        case t_pi:
            
            valTop++;
            valStack[valTop] = M_PI;
            break;
            // eval X=M_E
        case t_e:
            
            valTop++;
            valStack[valTop] = M_E;
            break;
            // eval X=sqrt(X)
        case t_sqrt:
            if (valTop<0) return YES;
            
            valStack[valTop] = sqrt(valStack[valTop]);
            break;
            // eval X=rand  random in [0..1]
        case t_rand:
            valTop++;
            int j,k,l;
            j=arc4random()%10;
            k=arc4random()%10;
            l=arc4random()%1000;
            valStack[valTop]  = (double)j/10+(double)k/1000+(double)l/100000;
            
            break;
            // eval X=round(X)
        case t_round:
            if (valTop<0) return YES;
            valStack[valTop] = round(valStack[valTop]);
            
            break;
            // eval X=iif(c,x1,x2) if c!=0 return x1 else x2
        case t_iif:
            if (valTop<2) return YES;
            valStack[valTop-2] = (valStack[valTop-2]!=0)?valStack[valTop-1]:valStack[valTop];
            valTop-=2;
            break;
            // eval X=abs(X)
        case t_abs:
            if (valTop<0) return YES;
            valStack[valTop] = (valStack[valTop]>0)?valStack[valTop]:-valStack[valTop];
            break;
            // eval X=X>X   return 0 if NO -1 if YES
        case t_Greater:
            if (valTop<1) return YES;
            valStack[valTop-1] = (valStack[valTop-1]>valStack[valTop])?-1:0;
            valTop--;
            break;
            // eval X=X<X   return 0 if NO -1 if YES
        case t_Fewer:
            if (valTop<1) return YES;
            valStack[valTop-1] = (valStack[valTop-1]<valStack[valTop])?-1:0;
            valTop--;
            break;
            // eval X=X==X   return 0 if NO -1 if YES
        case t_Equal:
            if (valTop<1) return YES;
            valStack[valTop-1] = (valStack[valTop-1]==valStack[valTop])?-1:0;
            valTop--;
            break;
            // return ERROR
        case t_err:
            return YES;
            break;
            // pop () off stack
        case t_RightPr:
            oprTop--;
            break;
            // get rid of warning
        default:
            break;
    }
    oprTop--;
    return NO;
}

- (BOOL)push
{
    if (tok.tType==tVal) 
    {
        if (++valTop >= MAX_VAL)            return YES;
        valStack[valTop] = tokenValue;
        //NSLog(@"push value %f",tokenValue);
    }else{
        if (++oprTop >= MAX_OPR)            return YES;
        oprStack[oprTop] = tok;
        //NSLog(@"push opr %d",tok.tName);
    }
    if ([self getToken]) 
        return YES;
    return NO;
}

- (BOOL)getToken
{
    
    NSMutableString *key = [[NSMutableString alloc] initWithCapacity:1];
    NSMutableString *number = [[NSMutableString alloc] initWithCapacity:1];
    NSMutableString *function = [[NSMutableString alloc] initWithCapacity:1];
    
    BOOL isKey = NO;
    BOOL isNum = NO;
    BOOL isFun = NO; 
    
    
    for (int i = charIndex; i<[inputString length]; i++) {
        
        char ch = [inputString characterAtIndex:i];
        
        //skip if function or key is parsed, at the moment ,for supporting functions and keys with numbers 
        if (((ch >='0' && ch<='9' )|| ch == '.') && !isKey && !isFun){
            if(!isNum)isNum = YES;
            [number appendString:[inputString substringWithRange:NSMakeRange(i, 1)]];
            continue;
        }   
        //not a number check for key starter '['
        if (ch=='[') {
            isKey = YES;
            continue;
        }
        //check for key end ']'
        if (ch==']') {
            isKey = NO;
            tokenValue = [self getKeyVal:key];
            
            tok = TokenMake(tVal, t_VAL);
            prevToken = tok;
            charIndex = i+1;
            break;
            
        }
        //check for parsing key
        if (isKey) {
            [key appendString:[inputString substringWithRange:NSMakeRange(i, 1)]];
            continue;
        }
        //if isNum==YES we got a value
        if (isNum) {
            isNum = NO;
            tokenValue = [number doubleValue];
            
            tok = TokenMake(tVal, t_VAL);
            prevToken = tok;
            charIndex = i;
            
            break;
        }
        //not a number and not a key start/end -> check for letters 
        if ((ch>='a' && ch<='z')||
            (ch>='A' && ch<='Z'))
        {
            if (!isFun) isFun = YES;
            [function appendString:[inputString substringWithRange:NSMakeRange(i, 1)]];
            continue;
            
        }
        //not a number,key start/end,or letter 
        if (isFun) {
            isFun = NO;
            tok = [self functionToken:function];
            charIndex = i;
            break;
        }
        
        //at this point we know that the [inputString characterAtIndex:i] is not a number,an '[' or ']' and not a letter
        //so we assume that we got an operator character
        
        //check if operator is unary postfix, if it is evaluate it else return token
        
        token _ATOK = [self operatorToken:ch];
        if (_ATOK.tType==tUno_suffix) {
            switch (_ATOK.tName) {
                case t_Fct:
                    if (prevToken.tType!=tVal && prevToken.tType!=tRPR) 
                        return YES;
                    if (prevToken.tType==tRPR) {
                        //pop right parenthesis set prevtoken and token {tVal,t_VAL}
                        
                        [self evaluate];
                        prevToken = tok = TokenMake(tVal, t_VAL);
                        
                    }
                    valStack[valTop]=[self fact:valStack[valTop]];
                    break;
                case t_PowTwo:
                    if (prevToken.tType!=tVal && prevToken.tType!=tRPR) 
                        return YES;
                    if (prevToken.tType==tRPR) {
                        
                        
                        [self evaluate];
                        prevToken = tok = TokenMake(tVal, t_VAL);
                        
                    }
                    valStack[valTop]*=valStack[valTop];
                    break;
                case t_PowThree:
                    if (prevToken.tType!=tVal && prevToken.tType!=tRPR) 
                        return YES;
                    if (prevToken.tType==tRPR) {
                        
                        
                        [self evaluate];
                        prevToken = tok = TokenMake(tVal, t_VAL);
                        
                    }
                    valStack[valTop]*=valStack[valTop]*valStack[valTop];
                    break;
                default:
                    break;
            }
            continue;
        }else{
            charIndex = i+1;
            tok = _ATOK;
            break;
        }
        
        
        //if got here something got wrong and it's sad :(
        return YES;
        
    }
    
    [function release],function=nil;
    [key release],key = nil;
    [number release],number = nil;
    return NO;
}


@end

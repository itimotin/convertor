//
//  DefaultCell.m
//  currency test
//
//  Created by Admin on 23.02.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DefaultCell.h"

@implementation DefaultCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        prevSize = CGSizeZero;
        CGRect frame = self.frame;
        frame.origin = CGPointZero;
        filterView.frame = frame;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)showFilterView:(BOOL)show
{
    [filterView removeFromSuperview];
    if (show) {
        [self addSubview:filterView];
    }
}

- (void)setNameLabelText:(NSString*)text
{
    [nameLabel setText:text];
    
    [nameLabel setFont:[UIFont fontWithName:@"Olney-Light" size:19.0]];
    
//    NSLog(@"default cell Font : %@", nameLabel.font);

    
}
- (void)setDescLabelText:(NSString*)text
{
    [descriptionLabel setText:text];
}

- (NSString *)valLabelText
{
    return valLabel.text;
}

- (void)setValueLabelText:(NSString*)text
{   
    CGSize  size = [text sizeWithFont:valLabel.font];
    CGRect frame = valLabel.frame;
    frame.size = size;
    frame.origin.y = 25;
//    if (frame.size.width > self.accessoryView.frame.size.width) {
//        CGRect accFrame = self.accessoryView.frame;
//        accFrame.size.width = size.width + 20;
//        self.accessoryView.frame = accFrame;
//        accFrame.origin = CGPointZero;
//        accBack.frame = accFrame;
//        accBack.image = [[UIImage imageNamed:@"mony_back.png"] stretchableImageWithLeftCapWidth:35 topCapHeight:0];
//    }
//    if (prevSize.width > size.width) {
//        CGRect accFrame = self.accessoryView.frame;
//        accFrame.size.width = size.width + 20;
//        self.accessoryView.frame = accFrame;
//        accFrame.origin = CGPointZero;
//        accBack.frame = accFrame;
//        accBack.image = [[UIImage imageNamed:@"mony_back.png"] stretchableImageWithLeftCapWidth:35 topCapHeight:0];
//    }
    frame.origin.x = self.accessoryView.frame.size.width - frame.size.width;
    
    valLabel.frame = frame;
    [valLabel setFont:[UIFont fontWithName:@"Olney-Light" size:17]];
    
    [valLabel setText:text];
    prevSize = size;
  
}
@end
